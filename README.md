Reforma GKH 2.0: Migration data with MySql to PostgreSQL
=======================

Installation
------------

Manually invoke `composer` using the shipped `composer.phar`:

    cd my/project/dir
    php composer.phar self-update
    php composer.phar install
