<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'console' => array(
        'router' => array(
            'routes' => array(
                'importFias' => array(
                    'options' => array(
                        'route'    => 'migration importFias <fiasXmlFilename> [-force]',
                        'defaults' => array(
                            'controller' => 'Migration\Controller\ImportFias',
                            'action'     => 'doimport',
                        ),
                    ),
                ),
                'syncFias' => array(
                    'options' => array(
                        'route'    => 'migration syncFias [-force]',
                        'defaults' => array(
                            'controller' => 'Migration\Controller\ImportFias',
                            'action'     => 'dosync',
                        ),
                    ),
                ),
                'all' => array(
                    'options' => array(
                        'route'    => 'migration all [-debug] [-force]',
                        'defaults' => array(
                            'controller' => 'Migration\Controller\Index',
                            'action'     => 'domigration',
                        ),
                    ),
                ),
                'users' => array(
                    'options' => array(
                        'route'    => 'migration users [-debug] [-force]',
                        'defaults' => array(
                            'controller' => 'Migration\Controller\Users',
                            'action'     => 'domigration',
                        ),
                    ),
                ),
                'geotegs' => array(
                    'options' => array(
                        'route'    => 'migration geotags [-debug] [-force]',
                        'defaults' => array(
                            'controller' => 'Migration\Controller\Geotags',
                            'action'     => 'domigration',
                        ),
                    ),
                ),
                'managementOrgs' => array(
                    'options' => array(
                        'route'    => 'migration orgs [-debug] [-force]',
                        'defaults' => array(
                            'controller' => 'Migration\Controller\ManagementOrgs',
                            'action'     => 'domigration',
                        ),
                    ),
                ),
                'mkd' => array(
                    'options' => array(
                        'route'    => 'migration mkd [-debug] [-force]',
                        'defaults' => array(
                            'controller' => 'Migration\Controller\Mkd',
                            'action'     => 'domigration',
                        ),
                    ),
                ),
                'rating_uo' => array(
                    'options' => array(
                        'route'    => 'migration rating [-debug] [-force]',
                        'defaults' => array(
                            'controller' => 'Migration\Controller\RatingUO',
                            'action'     => 'domigration',
                        ),
                    ),
                ),
                'files' => array(
                    'options' => array(
                        'route'    => 'migration files [-debug] [-force]',
                        'defaults' => array(
                            'controller' => 'Migration\Controller\Files',
                            'action'     => 'domigration',
                        ),
                    ),
                ),
                'files_archive' => array(
                    'options' => array(
                        'route'    => 'migration files_archive [-debug] [-force]',
                        'defaults' => array(
                            'controller' => 'Migration\Controller\Files',
                            'action'     => 'doMigrationArchive',
                        ),
                    ),
                ),
                'files_refactor' => array(
                    'options' => array(
                        'route'    => 'migration files_refactor [-debug] [-force]',
                        'defaults' => array(
                            'controller' => 'Migration\Controller\Files',
                            'action'     => 'domigrationRefactorAction',
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Migration\Controller\Index' => 'Migration\Controller\IndexController',
            'Migration\Controller\ImportFias' => 'Migration\Controller\ImportFiasController',
            'Migration\Controller\Geotags' => 'Migration\Controller\GeotagsController',
            'Migration\Controller\ManagementOrgs' => 'Migration\Controller\ManagementOrgsController',
            'Migration\Controller\Mkd' => 'Migration\Controller\MkdController',
            'Migration\Controller\Users' => 'Migration\Controller\UsersController',
            'Migration\Controller\RatingUO' => 'Migration\Controller\RatingUOController',
            'Migration\Controller\Files' => 'Migration\Controller\FilesController',
        ),
    ),
);
