<?php

namespace Migration\Model;

use Migration\Mapper\MkdMapper;

class MkdModel extends AbstractModel
{
    public function getMapper()
    {
        if ($this->_mapper === null) {
            $this->_mapper = new MkdMapper($this->getMySqlEntityManager(),$this->getPgSqlEntityManager(),$this->getMySqlEntityManagerSecond());
        }
        return $this->_mapper;
    }

    public function doMigration($debugMode = false, $forceMode = false)
    {
        ini_set('memory_limit','14000M');

        $this->getMapper()->setDebugMode($debugMode);
        $this->getMapper()->setForceMode($forceMode);

        echo "[".date('Y-m-d, H:i')."] Start migration MKD section".($debugMode?" [in DebugMode]":"").".\n";
        echo "Fix dublicate houses by addresses...\n";
        $this->getMapper()->dublicates();

        echo "Clearing data from rgkh2.0 Houses tables.\n";
        $this->getMapper()->clearDataFromTblHouses_rgkh2();
    //   $this->getMapper()->clearDataFromTblAddresses_rgkh2();

        $this->getMapper()->insertDataToTblSysEvents_rgkh2();
        $periods = $this->getMapper()->selectReportingPeriods_rgkh1();
        $this->getMapper()->insertDataToTblPeriods_rgkh2($periods);

        echo "Selecting FIAS links...\n";
        $this->getMapper()->selectFiasData_rgkh2();
       $this->getMapper()->selectLastIds();

        echo "Count ".count($periods)." periods\n";

        // ревизии нового отчетного периода
        foreach ($periods as $kp => $period) {
            if ($period['status']=='fictitious')    continue;
            $kp++;
            echo "Start migration NEW REVISIONS IN $kp period [{$period['date_start']} - {$period['date_end']}]\n";
            echo"Select count partion...\n";
            $count = $this->getMapper()->selectCountFromTblsMkd_rgkh1(strtotime($period['date_start']), strtotime($period['date_end']));
            $count_src = $count[0]['count'];
            $max_offset_ = $this->getMapper()->selectOffsetFromTblsMkd_rgkh1(strtotime($period['date_start']), strtotime($period['date_end']), false);
            $max_offset = $max_offset_[0]['nid'];

            $offset_ = $this->getMapper()->selectOffsetFromTblsMkd_rgkh1(strtotime($period['date_start']), strtotime($period['date_end']),true);
            $offset = $offset_[0]['nid'];

            $count_dst = 0;
            while ($offset < $max_offset)
            {
                $limit = $offset+5000;
                $data = $this->getMapper()->selectDataFromTblsMkd_rgkh1(strtotime($period['date_start']), strtotime($period['date_end']), $offset,$limit );
                $offset = $limit+1;
                $result = $this->getMapper()->insertDataToTblHouses_rgkh2($data, strtotime($period['date_start']), $period['id']);

                $count_dst += count($data);
                unset($data);
                $p = 100*$count_dst/$count_src;
                echo sprintf("\r%.2f%% MKD $kp period migration completed...",$p);
            }


            echo "\nCount {$this->getMapper()->getCountIdents()} equivalent addresses\n";

            $lost = $count_src - $count_dst;
            echo "Losted rows = ".$lost."\n";

       //     $this->getMapper()->unsetFiasData();

            unset($data);

            /////////////////////////////////////////////////////////////////////////////////////////////////
        }
        // ревизии по смене уо
        foreach ($periods as $kp => $period) {
            if ($period['status']=='fictitious')    continue;
            $kp++;
            if ($kp<4) continue;
            echo "Start migration UO LINKS REVISIONS IN $kp period [{$period['date_start']} - {$period['date_end']}]\n";

            $count = $this->getMapper()->selectCountFromTblsMkdUO_rgkh1(strtotime($period['date_start']), strtotime($period['date_end']));
            $count_src = $count[0]['count'];

            $max_offset_ = $this->getMapper()->selectOffsetFromTblsMkdUO_rgkh1(strtotime($period['date_start']), strtotime($period['date_end']), false);
            $max_offset = $max_offset_[0]['nid'];
            $offset_ = $this->getMapper()->selectOffsetFromTblsMkdUO_rgkh1(strtotime($period['date_start']), strtotime($period['date_end']),true);
            $offset = $offset_[0]['nid'];

            $count_dst = 0;
            while($offset < $max_offset)
            {
                $limit = $offset+2000;
                $data = $this->getMapper()->selectDataFromTblsMkdUO_rgkh1(strtotime($period['date_start']), strtotime($period['date_end']), $offset,$limit );
                $offset = $limit+1;
                $result = $this->getMapper()->insertDataToTblHouses_rgkh2($data, strtotime($period['date_start']), $period['id']);
                $count_dst += count($data);
                unset($data);
                $p = 100*$count_dst/$count_src;
                echo sprintf("\r%.2f%% MKD $kp period migration completed...",$p);
            }


            echo "\nCount {$this->getMapper()->getCountIdents()} equivalent addresses\n";

            $lost = $count_src - $count_dst;
            echo "Losted rows = ".$lost."\n";

            //     $this->getMapper()->unsetFiasData();

            unset($data);

        }

        $this->getMapper()->unsetFiasData();
        // ревизии по смене уо, то что не логировалось в 1.0 (до осени 2013)
        foreach ($periods as $kp => $period) {
            if ($period['status']!='fictitious')    continue;
            echo "Start migration UO LINKS REVISIONS IN $kp period < 2013\n";

            $count = $this->getMapper()->selectCountFromTblsMkdUOold_rgkh1(strtotime($period['date_end']));
            $count_src = $count[0]['count'];

            $max_offset_ = $this->getMapper()->selectOffsetFromTblsMkdUOold_rgkh1(strtotime($period['date_start']), strtotime($period['date_end']), false);
            $max_offset = $max_offset_[0]['nid'];
            $offset_ = $this->getMapper()->selectOffsetFromTblsMkdUOold_rgkh1(strtotime($period['date_start']), strtotime($period['date_end']),true);
            $offset = $offset_[0]['nid'];

            $count_dst = 0;
            while($offset < $max_offset)
            {
                $limit = $offset+2000;
                $data = $this->getMapper()->selectDataFromTblsMkdUOold_rgkh1(strtotime($period['date_start']), strtotime($period['date_end']), $offset,$limit );
                $offset = $limit+1;
                $result = $this->getMapper()->insertDataToTblHouses_rgkh2($data, strtotime($period['date_start']), $period['id']);
                $count_dst += count($data);
                unset($data);
                $p = 100*$count_dst/$count_src;
                echo sprintf("\r%.2f%% MKD $kp period migration completed...",$p);
            }


            echo "\nCount {$this->getMapper()->getCountIdents()} equivalent addresses\n";

            $lost = $count_src - $count_dst;
            echo "Losted rows = ".$lost."\n";

            //     $this->getMapper()->unsetFiasData();

            unset($data);
        }


        $this->getMapper()->executeQueryWithLog($this->getPgSqlEntityManager(),
            "update house_profile_revision set is_current = 0",true
        );
        $this->getMapper()->executeQueryWithLog($this->getPgSqlEntityManager(),
            "update house_profile_revision set is_current = 1 where id in (select hr.id from house_profile_revision hr left join houses h  on h.id = hr.houses_id  where hr.reporting_period_id = 8 and h.id is not null and hr.id = (select max(hr2.id) from house_profile_revision hr2 where hr2.reporting_period_id = 8 and hr2.houses_id = hr.houses_id))",true
        );

        echo "Migration MKD Fonds:\n";
        $this->getMapper()->clearDataFromTblsFond_rgkh2();

        $count_src = $this->getMapper()->selectCountTblFond_rgkh1('crashed_mkd');
        $count_dst = 0;
        $id = 1;
        for ($i=0;$i<$count_src;$i+=10000)
        {
            $data = $this->getMapper()->selectDataFromTblCrashedMkd_rgkh1($i,10000);
            if ($data == null) break;
            $result = $this->getMapper()->insertDataToTblFond_rgkh2($data, 'crashed_mkd', $id);
            unset($data);
            $count_dst += $result;
            $p = 100*$count_dst/$count_src;
            echo sprintf("\r%.2f%% MKD Fonds - crashed_mkd completed...",$p);
        }

        $lost = $count_src - $count_dst;
        echo "\nLosted rows = ".$lost."\n";

        $count_src = $this->getMapper()->selectCountTblFond_rgkh1('stat_migration');
        $count_dst = 0;
        $id = 1;
        for ($i=0;$i<$count_src;$i+=10000)
        {
            $data = $this->getMapper()->selectDataFromTblStatMigration_rgkh1($i,10000);
            if ($data == null) break;
            $result = $this->getMapper()->insertDataToTblFond_rgkh2($data, 'stat_migration', $id);
            unset($data);
            $count_dst += $result;
            $p = 100*$count_dst/$count_src;
            echo sprintf("\r%.2f%% MKD Fonds - stat_migration completed...",$p);
        }

        $lost = $count_src - $count_dst;
        echo "\nLosted rows = ".$lost."\n";

        $count_src = $this->getMapper()->selectCountTblFond_rgkh1('stat_overhaul');
        $count_dst = 0;
        $id = 1;
        for ($i=0;$i<$count_src;$i+=10000)
        {
            $data = $this->getMapper()->selectDataFromTblStatOverhaul_rgkh1($i,10000);
            if ($data == null) break;
            $result = $this->getMapper()->insertDataToTblFond_rgkh2($data, 'stat_overhaul', $id);
            unset($data);
            $count_dst += $result;
            $p = 100*$count_dst/$count_src;
            echo sprintf("\r%.2f%% MKD Fonds - stat_overhaul completed...",$p);
        }

        $lost = $count_src - $count_dst;
        echo "\nLosted rows = ".$lost."\n";

        $count_src = $this->getMapper()->selectCountTblFond_rgkh1('crashed_mkd_ext');
        $count_dst = 0;
        $id = 1;
        for ($i=0;$i<$count_src;$i+=10000)
        {
            $data = $this->getMapper()->selectDataFromTblCrashedMkd_rgkh1($i,10000, true);
            if ($data == null) break;
            $result = $this->getMapper()->insertDataToTblFond_rgkh2($data, 'crashed_mkd_ext', $id);
            unset($data);
            $count_dst += $result;
            $p = 100*$count_dst/$count_src;
            echo sprintf("\r%.2f%% MKD Fonds - crashed_mkd_ext completed...",$p);
        }

        $lost = $count_src - $count_dst;
        echo "\nLosted rows = ".$lost."\n";

        $count_src = $this->getMapper()->selectCountTblFond_rgkh1('stat_migration_ext');
        $count_dst = 0;
        $id = 1;
        for ($i=0;$i<$count_src;$i+=10000)
        {
            $data = $this->getMapper()->selectDataFromTblStatMigration_rgkh1($i,10000, true);
            if ($data == null) break;
            $result = $this->getMapper()->insertDataToTblFond_rgkh2($data, 'stat_migration_ext', $id);
            unset($data);
            $count_dst += $result;
            $p = 100*$count_dst/$count_src;
            echo sprintf("\r%.2f%% MKD Fonds - stat_migration_ext completed...",$p);
        }

        $lost = $count_src - $count_dst;
        echo "\nLosted rows = ".$lost."\n";

        $count_src = $this->getMapper()->selectCountTblFond_rgkh1('stat_overhaul_ext');
        $count_dst = 0;
        $id = 1;
        for ($i=0;$i<$count_src;$i+=10000)
        {
            $data = $this->getMapper()->selectDataFromTblStatOverhaul_rgkh1($i,10000, true);
            if ($data == null) break;

            $result = $this->getMapper()->insertDataToTblFond_rgkh2($data, 'stat_overhaul_ext', $id);
            unset($data);
            $count_dst += $result;
            $p = 100*$count_dst/$count_src;
            echo sprintf("\r%.2f%% MKD Fonds - stat_overhaul_ext completed...",$p);
        }

        $lost = $count_src - $count_dst;
        echo "\nLosted rows = ".$lost."\n";

        $this->getMapper()->updateHousesStateAndStage_rgkh2();

        $this->getMapper()->updateImportTable('crashed_mkd');
        $this->getMapper()->updateImportTable('crashed_mkd_ext');
        $this->getMapper()->updateImportTable('stat_overhaul');
        $this->getMapper()->updateImportTable('stat_overhaul_ext');
        $this->getMapper()->updateImportTable('stat_migration');
        $this->getMapper()->updateImportTable('stat_migration_ext');

        echo "Migration MKD lifts:\n";
        $this->getMapper()->clearDataFromTblLifts_rgkh2();
        $periods = $this->getMapper()->selectReportingPeriods_rgkh1();
        foreach ($periods as $kp => $period) {
            $kp++;
            if ($period['status']=='fictitious')    continue;

            echo "Select houses vids...\n";
            $this->getMapper()->selectHousesVids_rgkh2($period['id']);

            echo "Start migration $kp period [{$period['date_start']} - {$period['date_end']}]\n";

            $count_src = $this->getMapper()->selectCountTblLifts_rgkh1(strtotime($period['date_start']), strtotime($period['date_end']));
            $count_dst = 0;

            for ($i=0;$i<$count_src;$i+=20000)
            {
                $data = $this->getMapper()->selectDataFromTblLifts_rgkh1(strtotime($period['date_start']), strtotime($period['date_end']), $i,20000);
                if ($data)
                    $result = $this->getMapper()->insertDataToTblLifts_rgkh2($data);
                unset($data);
                $count_dst += $result;
                $p = 100*$count_dst/$count_src;
                echo sprintf("\r%.2f%% MKD lifts completed...",$p);
            }

            $lost = $count_src - $count_dst;
            echo "\nLosted rows = ".$lost."\n";
        }

        echo "[".date('Y-m-d, H:i')."] Finish migration MKD section.\n";
    }
}
