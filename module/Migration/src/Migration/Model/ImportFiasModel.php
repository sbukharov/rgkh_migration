<?php

namespace Migration\Model;

use Doctrine\DBAL\Types\VarDateTimeType;
use Migration\Mapper\ImportFiasMapper;
use Migration\Utils\ExtXMLReader;
use Migration\Utils\SqlLogger;
use SimpleXMLElement;
use Zend\Filter\StringToLower;

class ImportFiasModel extends AbstractModel
{
    protected $_tempStack = array();

    public function getMapper()
    {
        if ($this->_mapper === null) {
            $this->_mapper = new ImportFiasMapper($this->getMySqlEntityManager(),$this->getPgSqlEntityManager());
        }
        return $this->_mapper;
    }

    /**
     * @param ExtXMLReader $reader
     */
    public function callbackFiasRow($reader)
    {
        /**
         * @var SimpleXMLElement $objXml
         */
        $objXml = $reader->expandSimpleXml();

        /**
         * insert by 10M parts and clear tempStack
         */
        if (count($this->_tempStack) == 10000)
        {
            echo "Inserting data part 10M rows into rgkh2.0\n";
            $this->getMapper()->insertDataIntoFiasTbl($this->_tempStack, false);
            unset($this->_tempStack);
        //    exit;
        }

        $this->_tempStack[] = $objXml->attributes();
        unset($objXml);

        return true;
    }

    public function doImport($filename, $forceMode = false)
    {
        $this->getMapper()->setForceMode($forceMode);

        echo "[".date('Y-m-d, H:i')."] Start import FIAS xml into rgkh2.0\n";
        echo "Clearing FIAS table...\n";
        $this->getMapper()->clearDataFromTblFias_rgkh2();
        echo "Dropping FIAS indexes in rgkh2.0 db\n";
        echo "Selecting KLADR ais_ids...";
        $this->getMapper()->selectCodesFromKladr_rgkh1();
        $this->getMapper()->dropFiasIndexes();
        $reader = new ExtXmlReader();
        $reader->registerCallback("/AddressObjects/Object", array($this, "callbackFiasRow"));
        $reader->open("./public/".$filename);
        $reader->parse($reader);
        $reader->close();

        echo "[".date('Y-m-d, H:i')."] Finished import FIAS xml into rgkh2.0\n";
    }

    public function doSyncFiasPart($part)
    {
        echo "\n\nStart to compare $part\n";

        $selected_count = 0;
        $updates_name_count = 0;
        $updates_currstat_count = 0;
        $updates_ais_count = 0;
        $inserts_count = 0;
        $parents_count = 0;


        echo "Selecting {$part} actual data from KLADR...\n";
        $ao_list = $this->getMapper()->selectKladrData_rgkh1($part);    // берем все актуальные записи
        $selected_count += count($ao_list);

        echo "Selecting {$part} actual data from FIAS...\n";
        $fiasRows = $this->getMapper()->selectRowsFromTblFias_rgkh2($part);

        echo "Comparying FIAS vs KLADR:\n";

        $newRevisions = array();

        foreach ($ao_list as $i => &$ao_item)    // перебераем записи
        {
            $ao_item["shortname"] = $this->getMapper()->getShortName($ao_item["offname"],$ao_item["shortname"]);    // извлекаем тип набудущее

            if (!empty($ao_item['code']))
            {
                $plaincode = substr($ao_item['code'],0,strlen($ao_item['code'])-2);
                $fiasRow = $fiasRows[$plaincode]; // ищем совпадение по коду КЛАДР (без учета актуальности %00)
            } else {
                $fiasRow = null;
            }


               if (!empty($fiasRow))   // если нашли совпадение
               {
                   $kladr_name = iconv('windows-1251','utf-8',$ao_item['shortname'].$ao_item['offname']);
                   $fias_name = $fiasRow['shortname'].$fiasRow['offname'];
                   if (trim($kladr_name) != trim($fias_name))   // не сошлись названия или типы
                   {
                       $this->getMapper()->insertRevisionIntoTblFias_rgkh2($part, $fiasRow, $ao_item, 'name');   // создаем новую ревизию
                       $updates_name_count += 1;

           //            $logger = new SqlLogger();
           //            $logger->loggingActions("[REVNAME]Новая ревизия(изм. название)[ais_id={$ao_item['aisid']}]:\n Старое название АО | Новое название АО:".$fias_name." | ".$kladr_name);
                   }
                   else if ($fiasRow['code'] != $ao_item['code'])  // не сошлись коды актуальности в КЛАДРе
                   {
                       $this->getMapper()->insertRevisionIntoTblFias_rgkh2($part, $fiasRow, $ao_item, 'currcode');   // создаем новую ревизию
                       $updates_currstat_count += 1;

            //           $logger = new SqlLogger();
            //           $logger->loggingActions("[REVcode]Новая ревизия(изм. код КЛАДР)[ais_id={$ao_item['aisid']}]:\n Старый код АО | Новый код АО:".$fiasRow['code']." | ".$ao_item['code']);
                   }
                   else if (empty($fiasRow['aisid']))  // всё сошлось, но не указан ais_id
                   {
                       $updates_ais_count += $this->getMapper()->updateAisIdInTblFias_rgkh2($part, $ao_item, $fiasRow);   // обновляем ревизию
                   }
               }                            // исключаем aisid "<Улица отсутствует>"
               else  if ($ao_item['aisid'] != 54940047) {  // если не нашли совпадение
                   $ao_item = $this->getMapper()->selectKladrRow_rgkh1($part, $ao_item['aisid']);
                   $ao_item["shortname"] = $this->getMapper()->getShortName($ao_item["offname"],$ao_item["shortname"]);
                   $inserts = $this->getMapper()->insertNewRowIntoTblFias_rgkh2($part, $ao_item);
                   $inserts_count +=  $inserts[0];
                   $parents_count +=  $inserts[1];
           //        $kladr_name = iconv('windows-1251','utf-8',$ao_item['offname']);
           //        $logger = new SqlLogger();
            //       $logger->loggingActions("[NEW]Новый АО(не найден в ФИАС по коду КЛАДР)[ais_id={$ao_item['aisid']}]:\n Код нового АО | Название нового АО:".$ao_item['code']." | ".$kladr_name);
               }

            unset($fiasRow);
            unset($ao_item);
            $p = 100*($i+1)/$selected_count;
            echo sprintf("\r%.2f%% {$part} completed...",$p);
        }
        echo "$part - save new revisions \n";
        //$this->getMapper()->insertDataIntoFiasTbl($newRevisions, true);
        $this->getMapper()->executeQueryWithLog($this->getPgSqlEntityManager(),
            "UPDATE fias_addrobj SET \"nextid\" = u.aoidnew, \"actstatus\" = '0', \"operstatus\" = u.operstat FROM(VALUES".implode(',',$this->getMapper()->updateRevisions).") as u(aoidnew,operstat,aoidold) WHERE \"aoid\" = u.aoidold",
            true
        );
        unset($ao_list);
        unset($fiasRows);

        echo "\n";
        echo "$part - count in KLADR: ".$selected_count."\n";
        echo "$part - updated (update ais_id): ".$updates_ais_count."\n";
        echo "$part - added new revisons (not converge kladr actual status): ".$updates_currstat_count."\n";
        echo "$part - added new revisons (not converge name): ".$updates_name_count."\n";
        echo "$part - added new AO (not found in FIAS) / parents count: ".$inserts_count." / ".$parents_count."\n";
    }

    public function doSyncFiasAndKladr($forceMode = false)
    {
        $this->getMapper()->setForceMode($forceMode);

        echo "[".date('Y-m-d, H:i')."] Start sync FIAS and KLADR in rgkh2.0 db\n";
        echo "Prepare archive ao's...\n";
        $this->getMapper()->prepareArchiveAOs_rgkh1();
        echo "Add fias indexes in rgkh2.0 db\n";
        $this->getMapper()->addFiasIndexes();
        echo "Selecting KLADR ais_ids...\n";
        $this->getMapper()->selectCodesFromKladr_rgkh1();

        $this->doSyncFiasPart('Regions');
        $this->doSyncFiasPart('Areas');
        $this->doSyncFiasPart('Cities');
        $this->doSyncFiasPart('Streets');

        echo "[".date('Y-m-d, H:i')."] Finish sync FIAS and KLADR in rgkh2.0 db\n";
    }
}
