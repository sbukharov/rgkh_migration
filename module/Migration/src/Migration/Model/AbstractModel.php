<?php

namespace Migration\Model;

use Doctrine\ORM\EntityManager;

abstract class AbstractModel
{
    /**
     * @var model mapper
     */
    protected $_mapper = null;

    /**
     * mySQL entity manager
     * @var \Doctrine\ORM\EntityManager
     */
    protected $_mysqlEntityManager;

    /**
     * mySQL entity manager
     * @var \Doctrine\ORM\EntityManager
     */
    protected $_mysqlEntityManagerSecond;

    /**
     * pgSQL entity manager
     * @var \Doctrine\ORM\EntityManager
     */
    protected $_pgsqlEntityManager;

    public function __construct(EntityManager $mysql_em, EntityManager $pgsql_em, EntityManager $mysql_em_s = null)
    {
        $this->_mysqlEntityManager = $mysql_em;
        $this->_pgsqlEntityManager = $pgsql_em;
        $this->_mysqlEntityManagerSecond = $mysql_em_s;
    }

    public function getMapper()
    {
        return $this->_mapper;
    }

    public function getMySqlEntityManager()
    {
        return $this->_mysqlEntityManager;
    }

    public function getPgSqlEntityManager()
    {
        return $this->_pgsqlEntityManager;
    }

    public function getMySqlEntityManagerSecond()
    {
        return $this->_mysqlEntityManagerSecond;
    }

    /**
     * General migration method
     * Calling in controller
     * @param bool $debugMode
     * @param bool $forceMode
     */
    public function doMigration($debugMode = false, $forceMode = false)
    {

    }

    /**
     * import methods (FIAS and etc)
     * @param string $filename
     * @param bool $forceMode
     */
    public function doImport($filename, $forceMode = false)
    {

    }
}
