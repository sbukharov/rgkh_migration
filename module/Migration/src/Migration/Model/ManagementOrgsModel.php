<?php

namespace Migration\Model;

use Migration\Mapper\ManagementOrgsMapper;

class ManagementOrgsModel extends AbstractModel
{
    public function getMapper()
    {
        if ($this->_mapper === null) {
            $this->_mapper = new ManagementOrgsMapper($this->getMySqlEntityManager(),$this->getPgSqlEntityManager());
        }
        return $this->_mapper;
    }

    public function doMigration($debugMode = false, $forceMode = false)
    {
        ini_set('memory_limit','14000M');
        $this->getMapper()->setDebugMode($debugMode);
        $this->getMapper()->setForceMode($forceMode);

        echo "Start migration ManagementOrgs section".($debugMode?" [in DebugMode]":"").".\n";

        echo "Clearing data from rgkh2.0 ManagementOrganizations table.\n";
        $this->getMapper()->clearDataFromTblManagementOrgs_rgkh2();
        $this->getMapper()->clearDataFromTblAddresses_rgkh2();

        echo "Selecting FIAS links...\n";
        $this->getMapper()->generatePeriodicItems_rgkh2();
        $periods = $this->getMapper()->selectReportingPeriods_rgkh1();
        $this->getMapper()->selectFiasData_rgkh2();
        $this->getMapper()->selectLastIdAddresses();
        $this->getMapper()->insertDataToTblPeriods_rgkh2($periods);

        echo "Selecting providers-uo...\n";
        $this->getMapper()->selectProviderUO();

        echo "Count ".count($periods)." periods\n";

        foreach ($periods as $kp => $period) {
            $kp++;

            echo "Start migration NEW REVISIONS IN $kp period [{$period['date_start']} - {$period['date_end']}]\n";

            $count_src = $this->getMapper()->selectCountFromTblNode_rgkh1($period);


            $count_dst = 0;

            for ($i=0;$i<$count_src;$i+=1000)
            {
                $data = $this->getMapper()->selectDataFromTblNode_rgkh1($period,$i, 1000);
                if ($data == null) break;
                $result = $this->getMapper()->insertDataToTblManagementOrgs_rgkh2($data, $period['id']);
                unset($data);
                /*        if (!$debugMode && $result == null) {
                            $this->getMapper()->clearDataFromTblManagementOrgs_rgkh2();
                            break;
                        }*/
                $count_dst += $result;
                $p = 100*$count_dst/$count_src;
                echo sprintf("\r%.2f%% UO migration completed...",$p);
            }


            echo "\nCount {$this->getMapper()->getCountIdents()} equivalent addresses\n";

            $lost = $count_src - $count_dst;
            echo "Losted rows = ".$lost."\n";

            //     $this->getMapper()->unsetFiasData();

            unset($data);
        }

        $this->getMapper()->updateCurrentRevisions_rgkh2();

        $this->getMapper()->unsetFiasData();

        echo "Clearing data from rgkh2.0 ManagementOrganizationsUsers table.\n";
        $this->getMapper()->clearDataFromTblManagementOrgsUsers_rgkh2();

        echo "Selecting data from rgkh1.0 AdminUO table\n";
        $data = $this->getMapper()->selectDataFromTblAdminUO_rgkh1();

        echo "Inserting data into rgkh2.0 ManagementOrganizationsUsers table\n";
        $count_dst = $this->getMapper()->insertDataToTblManagementOrgsUsers_rgkh2($data);

        $lost = count($data) - $count_dst;
        echo "Losted rows = ".$lost."\n";

        unset($data);

        $this->getMapper()->updateOrgNotMOStatus();

        echo "Finish migration ManagementOrgs section.\n";
    }
}
