<?php

namespace Migration\Model;

use Migration\Mapper\RatingUOMapper;

class RatingUOModel extends AbstractModel
{
    public function getMapper()
    {
        if ($this->_mapper === null) {
            $this->_mapper = new RatingUOMapper($this->getMySqlEntityManager(),$this->getPgSqlEntityManager());
        }
        return $this->_mapper;
    }

    public function doMigration($debugMode = false, $forceMode = false)
    {
        $this->getMapper()->setDebugMode($debugMode);
        $this->getMapper()->setForceMode($forceMode);

        echo "Start migration Ratings UO section".($debugMode?" [in DebugMode]":"").".\n";

        echo "Clearing rgkh2.0 section tables.\n";
        $this->getMapper()->clearDataFromTblsRating_rgkh2();

        echo "Start migration table rating_uo\n";
        $data = $this->getMapper()->selectDataFromTblRating_rgkh1('rating_uo');
        $result = $this->getMapper()->insertDataToTblRating_rgkh2('rating_uo', $data);
        unset($data);
        echo "Migrated $result rows\n";

        echo "Start migration table rating_uo_index\n";
        $data = $this->getMapper()->selectDataFromTblRating_rgkh1('rating_uo_index');
        $result = $this->getMapper()->insertDataToTblRating_rgkh2('rating_uo_index', $data);
        unset($data);
        echo "Migrated $result rows\n";

        echo "Start migration table rating_uo_disclosure\n";
        $data = $this->getMapper()->selectDataFromTblRating_rgkh1('rating_uo_disclosure');
        $result = $this->getMapper()->insertDataToTblRating_rgkh2('rating_uo_disclosure', $data);
        unset($data);
        echo "Migrated $result rows\n";

        echo "Start migration table rating_uo_result\n";
        $data = $this->getMapper()->selectDataFromTblRating_rgkh1('rating_uo_result');
        $result = $this->getMapper()->insertDataToTblRating_rgkh2('rating_uo_result', $data);
        unset($data);
        echo "Migrated $result rows\n";


        echo "Finish migration Ratings UO section.\n";
    }
}
