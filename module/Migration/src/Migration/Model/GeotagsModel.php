<?php

namespace Migration\Model;

use Migration\Mapper\GeotagsMapper;

class GeotagsModel extends AbstractModel
{
    public function getMapper()
    {
        if ($this->_mapper === null) {
            $this->_mapper = new GeotagsMapper($this->getMySqlEntityManager(),$this->getPgSqlEntityManager());
        }
        return $this->_mapper;
    }

    public function doMigration($debugMode = false, $forceMode = false)
    {
        $this->getMapper()->setDebugMode($debugMode);
        $this->getMapper()->setForceMode($forceMode);

        echo "Start migration Geotags section".($debugMode?" [in DebugMode]":"").".\n";

        echo "Clearing rgkh2.0 section tables.\n";
        $this->getMapper()->clearDataFromTblGeo_rgkh2();

        echo "Selecting data from  rgkh1.0 section tables.\n";
        $count = $this->getMapper()->selectCountFromTblGeo_rgkh1();
        $count_src = $count[0]['count'];

        echo "Inserting data into rgkh2.0 section tables.\n";

        $count_dst = 0;

        /**
         * work by 10000 item parts
         */
        for ($i=0;$i<$count_src;$i+=10000)
        {
            $data = $this->getMapper()->selectDataFromTblGeo_rgkh1($i, 10000);
            if ($data == null) break;
            $result = $this->getMapper()->insertDataToTblGeo_rgkh2($data);
            unset($data);
            if (!$debugMode && $result == null) {
                $this->getMapper()->clearDataFromTblGeo_rgkh2();
                break;
            }
            $count_dst += $result;
        }

        $lost = $count_src - $count_dst;
        echo "Losted rows = ".$lost."\n";

        echo "Finish migration Geotags section.\n";
    }
}
