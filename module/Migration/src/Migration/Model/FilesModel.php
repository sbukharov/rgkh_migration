<?php

namespace Migration\Model;

use Migration\Mapper\FilesMapper;

class FilesModel extends AbstractModel
{
    public function getMapper()
    {
        if ($this->_mapper === null) {
            $this->_mapper = new FilesMapper($this->getMySqlEntityManager(),$this->getPgSqlEntityManager());
        }
        return $this->_mapper;
    }

    public function doMigration($debugMode = false, $forceMode = false)
    {
        $this->getMapper()->setDebugMode($debugMode);
        $this->getMapper()->setForceMode($forceMode);

        echo "Start migration Files section".($debugMode?" [in DebugMode]":"").".\n";

        echo "Clearing rgkh2.0 section tables.\n";
        $this->getMapper()->clearDataFromTblFiles_rgkh2();


        $count_src = $this->getMapper()->selectCountFromTblFiles_rgkh1();

        $count_dst = 0;

        for ($i=0;$i<$count_src;$i+=50000)
        {
            $data = $this->getMapper()->selectDataFromTblFiles_rgkh1($i,50000);
            if ($data == null) break;
            $result = $this->getMapper()->insertDataToTblFiles_rgkh2($data);
            unset($data);
            $count_dst += $result;
            $p = 100*$count_dst/$count_src;
            echo sprintf("\r%.2f%% Files migration completed...",$p);
        }

        $lost = $count_src - $count_dst;
        echo "Losted rows = ".$lost."\n";

        unset($data);

        echo "Set file types\n";
        $this->getMapper()->insertDataToTblFileTypes_rgkh2();

        $this->getMapper()->migrateUsersAvatars();
        $this->getMapper()->migrateFileField('field_answer_reports', 'audit_commision_report', 'profile_uo_periodic');
        $this->getMapper()->migrateFileField('field_audit_reports', 'audit_reports', 'profile_uo_periodic');
        $this->getMapper()->migrateFileField('field_member_protocos', 'general_meetings_protocol', 'profile_uo_periodic');
        $this->getMapper()->migrateFileField('field_org_god_bug_otch', 'annual_financial_statements', 'profile_uo_periodic');
        $this->getMapper()->migrateFileField('field_org_proj_dog_upr', 'management_contract', 'profile_uo_periodic');
        $this->getMapper()->migrateFileField('field_org_stoimost_uslug', 'services_cost', 'profile_uo_periodic');
        $this->getMapper()->migrateFileField('field_org_tarifs', 'tariffs', 'profile_uo_periodic');
        $this->getMapper()->migrateFileField('field_reports_finish_gsk', 'performance_report', 'profile_uo_periodic');
        $this->getMapper()->migrateFileField('field_reports_gsk', 'revenues_expenditures_estimates', 'profile_uo_periodic');
        $this->getMapper()->migrateFileField('field_org_additional', 'additional_files', 'profile_uo_static');
        $this->getMapper()->migrateFileField('field_org_copy_doc_admin', 'prosecute_documents_copies', 'profile_uo_static');
        $this->getMapper()->migrateFileField('field_dgvr_prices', 'cost_service', 'contract');
        $this->getMapper()->migrateFileField('field_dgvr_res_tsg', 'resources_tsz_zsk', 'contract');
        $this->getMapper()->migrateFileField('field_dgvr_services_terms', 'terms_service_tsz_zsk', 'contract');
        $this->getMapper()->migrateFileField('field_dgvr_jobs', 'jobs', 'contract');
        $this->getMapper()->migrateFileField('field_dgvr_obyaz', 'responsibility', 'contract');

        echo "\nFinish migration Files section.\n";
    }

    public function doMigrationArchive($debugMode = false, $forceMode = false)
    {
        $this->getMapper()->setDebugMode($debugMode);
        $this->getMapper()->setForceMode($forceMode);

        echo "Start scan-migration Archive Files section".($debugMode?" [in DebugMode]":"").".\n";
        $this->getMapper()->migrateArchiveFiles();
        echo "Finished\n";
    }

    public function doRefactorFiles($debugMode = false, $forceMode = false)
    {
        $this->getMapper()->setDebugMode($debugMode);
        $this->getMapper()->setForceMode($forceMode);

        echo "Start refactoring-migration Files section".($debugMode?" [in DebugMode]":"").".\n";
        $this->getMapper()->refactorFiles();
        echo "Finished\n";
    }
}
