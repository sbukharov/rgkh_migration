<?php

namespace Migration\Model;

use Migration\Mapper\UsersMapper;

class UsersModel extends AbstractModel
{
    public function getMapper()
    {
        if ($this->_mapper === null) {
            $this->_mapper = new UsersMapper($this->getMySqlEntityManager(),$this->getPgSqlEntityManager());
        }
        return $this->_mapper;
    }

    public function doMigration($debugMode = false, $forceMode = false)
    {
        $this->getMapper()->setDebugMode($debugMode);
        $this->getMapper()->setForceMode($forceMode);

        echo "Start migration Users section".($debugMode?" [in DebugMode]":"").".\n";

  //      echo "Clearing data in rgkh2.0  UsersTermRoles table\n";
  //      $this->getMapper()->clearDataFromTblUsersTermRoles_rgkh2();

        echo "Clearing data in rgkh2.0  UsersRoles table\n";
        $this->getMapper()->clearDataFromTblUsersRoles_rgkh2();

        echo "Clearing data in rgkh2.0  Users table\n";
        $this->getMapper()->clearDataFromTblUsers_rgkh2();

        echo "Selecting data from rgkh1.0 Users table\n";
        $data = $this->getMapper()->selectDataFromTblUsers_rgkh1();

        if ($data != null) {
            echo "Inserting data into rgkh2.0 Users table\n";
            $count_dst = $this->getMapper()->insertDataToTblUsers_rgkh2($data);

            $lost = count($data) - $count_dst;
            echo "Losted rows = ".$lost."\n";

            unset($data);
        }

        echo "Clearing data in rgkh2.0  Roles table\n";
        $this->getMapper()->clearDataFromTblRoles_rgkh2();

        echo "Selecting data from rgkh1.0 Roles table\n";
        $data = $this->getMapper()->selectDataFromTblRoles_rgkh1();

        if ($data != null) {
            echo "Inserting data into rgkh2.0 Roles table\n";
            $count_dst = $this->getMapper()->insertDataToTblRoles_rgkh2($data);

            $lost = count($data) - $count_dst;
            echo "Losted rows = ".$lost."\n";

            unset($data);
        }
/*
        echo "Selecting data from rgkh1.0 UsersTermRoles table\n";
        $data = $this->getMapper()->selectDataFromTblUsersTermRoles_rgkh1();

        if ($data != null) {
            echo "Inserting data into rgkh2.0 UsersTermRoles table\n";
            $count_dst = $this->getMapper()->insertDataToTblUsersTermRoles_rgkh2($data);

            $lost = count($data) - $count_dst;
            echo "Losted rows = ".$lost."\n";

            unset($data);
        }
*/
        echo "Selecting data from rgkh1.0 UsersRoles table\n";
        $data = $this->getMapper()->selectDataFromTblUsersRoles_rgkh1();

        if ($data != null) {
            echo "Inserting data into rgkh2.0 UsersRoles table\n";
            $count_dst = $this->getMapper()->insertDataToTblUsersRoles_rgkh2($data);

            $lost = count($data) - $count_dst;
            echo "Losted rows = ".$lost."\n";


            unset($data);
        }

        echo "Finish migration Users section.\n";
    }
}
