<?php

namespace Migration\Utils;

class SqlLogger
{
    protected $_queryLogFilepath = './public/logs/SQLqueries.log';
    protected $_errorLogFilepath = './public/logs/SQLerrors.log';
    protected $_actionLogFilepath = './public/logs/actions.log';

    /**
     * logging sql query
     * @param string $query
     * @param int $countRows
     */
    public function loggingQuery($query, $countRows)
    {
    /*       $fhandle = fopen($this->_queryLogFilepath, 'a');
           if ( $fhandle ) {
               fwrite($fhandle, "[".date('d.m.Y H:i:s', time())."]\n");
               fwrite($fhandle, "QUERY({$countRows} - rows count): ".iconv("windows-1251","utf-8",$query)."\n");
               fwrite($fhandle, "------------*************************************************************-------------\n");
               fclose($fhandle);
           }*/
    }

    /**
     * logging sql query error
     * @param string $error
     */
    public function loggingQueryError($error)
    {
            $fhandle = fopen($this->_errorLogFilepath, 'a');
            if ( $fhandle ) {
                fwrite($fhandle, "[".date('d.m.Y H:i:s', time())."]\n");
                fwrite($fhandle, $error."\n");
                fwrite($fhandle, "------------*************************************************************-------------\n");
                fclose($fhandle);
            }
    }

    /**
     * logging actions
     * @param string $action
     */
    public function loggingActions($action)
    {
        $fhandle = fopen($this->_actionLogFilepath, 'a');
        if ( $fhandle ) {
            fwrite($fhandle, "[".date('d.m.Y H:i:s', time())."]\n");
            fwrite($fhandle, $action."\n");
            fwrite($fhandle, "------------*************************************************************-------------\n");
            fclose($fhandle);
        }
    }
}
