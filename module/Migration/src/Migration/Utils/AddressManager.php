<?php

namespace Migration\Utils;

class AddressManager
{
    /**
 * generate full string addresss
 * @param array $address
 * @return string full string addresss
 */
    public function generateStringAddress($address)
    {
        $stringAddress = '';
        if (!empty($address['region_type']) && !empty($address['region_name'])) {  // регион
            $stringAddress .= $address['region_type']." ".$address['region_name']." ";
        }
        if (!empty($address['area_type']) && !empty($address['area_name'])) {  // район
            $stringAddress .= $address['area_type']." ".$address['area_name']." ";
        }
        if (!empty($address['city_type']) && !empty($address['city_name'])) {  // нп
            $stringAddress .= $address['city_type']." ".$address['city_name']." ";
        }
        if (!empty($address['street_type']) && !empty($address['street_name'])) {  // улица
            $stringAddress .= $address['street_type']." ".$address['street_name']." ";
        }
        if (!empty($address['house_number'])) {  // номер дома
            $stringAddress .= "д.".trim($address['house_number'])." ";
        }
        if (!empty($address['housing'])) {  // корпус дома
            $stringAddress .= "кор.".trim($address['housing'])." ";
        }
        if (!empty($address['building'])) {  // строение дома
            $stringAddress .= "строение ".trim($address['building'])." ";
        }
        if (!empty($address['room'])) {  // офис
            $stringAddress .= "офис ".trim($address['room'])." ";
        }
        $stringAddress = substr($stringAddress, 0, strlen($stringAddress)-1);    // удаляем последний пробел

        return $stringAddress;
    }

    /**
     * generate full serialized addresss
     * @param array $address
     * @return string full serialized addresss
     */
    public function generateSerializedAddress($arrayToSerialize)
    {
        unset($arrayToSerialize['region_guid']);
        unset($arrayToSerialize['area_guid']);
        unset($arrayToSerialize['city_guid']);
        unset($arrayToSerialize['street_guid']);
        unset($arrayToSerialize['flag_invalid']);
        unset($arrayToSerialize['comment']);

        $arrayToSerialize['house_number'] = trim($arrayToSerialize['house_number']);
        $arrayToSerialize['block'] = trim($arrayToSerialize['housing']);
        unset($arrayToSerialize['housing']);
        $arrayToSerialize['building'] = trim($arrayToSerialize['building']);
        $arrayToSerialize['room'] = trim($arrayToSerialize['room']);

        return serialize($arrayToSerialize);
    }
}
