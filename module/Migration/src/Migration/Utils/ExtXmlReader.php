<?php

namespace Migration\Utils;

use XMLReader;
use DomDocument;
use Zend\Config\Exception;

/**
 * Class ExtXMLReader
 * Обертка для XMLReader с возможностью callback-функций
 * @package Migration\Utils
 */
class ExtXmlReader extends XMLReader
{

    /**
     * Callbacks
     *
     * @var array
     */
    protected $callback = array();


    /**
     * Depth
     *
     * @var int
     */
    protected $currentDepth = 0;


    /**
     * Previos depth
     *
     * @var int
     */
    protected $prevDepth = 0;


    /**
     * Stack of the parsed nodes
     *
     * @var array
     */
    protected $nodesParsed = array();


    /**
     * Stack of the node types
     *
     * @var array
     */
    protected $nodesType = array();


    /**
     * Stack of node position
     *
     * @var array
     */
    protected $nodesCounter = array();


    /**
     * Add node callback
     *
     * @param  string   $xpath
     * @param  callback $callback
     * @param  integer  $nodeType
     * @return ExtXMLReader
     */
    public function registerCallback($xpath, $callback, $nodeType = XMLReader::ELEMENT)
    {
        if (isset($this->callback[$nodeType][$xpath])) {
            throw new Exception\RuntimeException(sprintf(
                "Already exists callback '%s':%s.",
                $xpath, $nodeType
            ));
        }
        if (!is_callable($callback)) {
            throw new Exception\RuntimeException(sprintf(
                "Not callable callback '%s':%s.",
                $xpath, $nodeType
            ));
        }
        $this->callback[$nodeType][$xpath] = $callback;
        return $this;
    }


    /**
     * Remove node callback
     *
     * @param  string  $xpath
     * @param  integer $nodeType
     * @return ExtXMLReader
     */
    public function unRegisterCallback($xpath, $nodeType = XMLReader::ELEMENT)
    {
        if (!isset($this->callback[$nodeType][$xpath])) {
            throw new Exception\RuntimeException(sprintf(
                "Unknow parser callback '%s':%s.",
                $xpath, $nodeType
            ));
        }
        unset($this->callback[$nodeType][$xpath]);
        return $this;
    }

    /**
     * Moves cursor to the next node in the document.
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function read()
    {
        $read = parent::read();
        if ($this->depth < $this->prevDepth) {
            if (!isset($this->nodesParsed[$this->depth])) {
                throw new Exception\RuntimeException(sprintf(
                    "Invalid xml: missing items in %s",
                    $this->nodesParsed
                ));
            }
            if (!isset($this->nodesCounter[$this->depth])) {
                throw new Exception\RuntimeException(sprintf(
                    "Invalid xml: missing items in %s",
                    $this->nodesCounter
                ));
            }
            if (!isset($this->nodesType[$this->depth])) {
                throw new Exception\RuntimeException(sprintf(
                    "Invalid xml: missing items in %s",
                    $this->nodesType
                ));
            }
            $this->nodesParsed = array_slice($this->nodesParsed, 0, $this->depth + 1, true);
            $this->nodesCounter = array_slice($this->nodesCounter, 0, $this->depth + 1, true);
            $this->nodesType = array_slice($this->nodesType, 0, $this->depth + 1, true);
        }
        if (isset($this->nodesParsed[$this->depth]) && $this->localName == $this->nodesParsed[$this->depth] && $this->nodeType == $this->nodesType[$this->depth]) {
            $this->nodesCounter[$this->depth] = $this->nodesCounter[$this->depth] + 1;
        } else {
            $this->nodesParsed[$this->depth] = $this->localName;
            $this->nodesType[$this->depth] = $this->nodeType;
            $this->nodesCounter[$this->depth] = 1;
        }
        $this->prevDepth = $this->depth;
        return $read;
    }

    /**
     * Return current xpath node
     *
     * @param boolean $nodesCounter
     * @return string
     */
    public function currentXpath($nodesCounter = false)
    {
        if (count($this->nodesCounter) != count($this->nodesParsed) && count($this->nodesCounter) != count($this->nodesType)) {
            throw new Exception\RuntimeException("Empty reader");
        }
        $result = "";
        foreach ($this->nodesParsed as $depth => $name) {
            switch ($this->nodesType[$depth]) {
                case self::ELEMENT:
                    $result .= "/" . $name;
                    if ($nodesCounter) {
                        $result .= "[" . $this->nodesCounter[$depth] . "]";
                    }
                    break;

                case self::TEXT:
                case self::CDATA:
                    $result .= "/text()";
                    break;

                case self::COMMENT:
                    $result .= "/comment()";
                    break;

                case self::ATTRIBUTE:
                    $result .= "[@{$name}]";
                    break;
            }
        }
        return $result;
    }


    /**
     * Run parser
     * @param \Migration\Model\AbstractModel $callbacksClass
     * @return void
     */
    public function parse($callbacksClass)
    {
        if (empty($this->callback)) {
            throw new Exception\RuntimeException("Empty parser callback.");
        }
        $continue = true;
        while ($continue && $this->read()) {
            if (!isset($this->callback[$this->nodeType])) {
                continue;
            }
            if (isset($this->callback[$this->nodeType][$this->name])) {
                $continue = call_user_func($this->callback[$this->nodeType][$this->name], $callbacksClass);
            } else {
                $xpath = $this->currentXpath(false); // without node counter
                if (isset($this->callback[$this->nodeType][$xpath])) {
                    $continue = call_user_func($this->callback[$this->nodeType][$xpath], $callbacksClass);
                } else {
                    $xpath = $this->currentXpath(true); // with node counter
                    if (isset($this->callback[$this->nodeType][$xpath])) {
                        $continue = call_user_func($this->callback[$this->nodeType][$xpath], $callbacksClass);
                    }
                }
            }
        }
    }

    /**
     * Run XPath query on current node
     *
     * @param  string $path
     * @param  string $version
     * @param  string $encoding
     * @return array(SimpleXMLElement)
     */
    public function expandXpath($path, $version = "1.0", $encoding = "UTF-8")
    {
        return $this->expandSimpleXml($version, $encoding)->xpath($path);
    }

    /**
     * Expand current node to string
     *
     * @param  string $version
     * @param  string $encoding
     * @return SimpleXMLElement
     */
    public function expandString($version = "1.0", $encoding = "UTF-8")
    {
        return $this->expandSimpleXml($version, $encoding)->asXML();
    }

    /**
     * Expand current node to SimpleXMLElement
     *
     * @param  string $version
     * @param  string $encoding
     * @param  string $className
     * @return SimpleXMLElement
     */
    public function expandSimpleXml($version = "1.0", $encoding = "UTF-8", $className = null)
    {
        $element = $this->expand();
        $document = new DomDocument($version, $encoding);
        $node = $document->importNode($element, true);
        $document->appendChild($node);
        return simplexml_import_dom($node, $className);
    }
}