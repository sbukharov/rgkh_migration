<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Migration\Controller;


class RatingUOController extends AbstractMigrationController
{

    public function indexAction()
    {
        return false;
    }

    public function domigrationAction()
    {
        $this->prepareParams();

        $ManagementOrgsModel = $this->getServiceLocator()->get('ratingUOModel');
        $ManagementOrgsModel->doMigration($this->_debugMode, $this->_forceMode);
    }
}
