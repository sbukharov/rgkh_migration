<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Migration\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Console\Request as ConsoleRequest;

abstract class AbstractMigrationController extends AbstractActionController
{
    protected $_debugMode;
    protected $_forceMode;
    protected $_fiasXmlFilename;

    public function prepareParams()
    {
        $request = $this->getRequest();
        // Make sure that we are running in a console and the user has not tricked our
        // application into running this action from a public web server.
        if (!$request instanceof ConsoleRequest){
            throw new \RuntimeException('You can only use this action from a console!');
        }

        $this->_debugMode = $request->getParam('debug');
        $this->_forceMode = $request->getParam('force');
        $this->_fiasXmlFilename = $request->getParam('fiasXmlFilename');
    }
}
