<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Migration\Controller;


class ImportFiasController extends AbstractMigrationController
{

    public function indexAction()
    {
        return false;
    }

    public function doimportAction()
    {
        $this->prepareParams();

        $importFiasModel = $this->getServiceLocator()->get('importFiasModel');
        $importFiasModel->doImport($this->_fiasXmlFilename, $this->_forceMode);
    }

    public function dosyncAction()
    {
        $this->prepareParams();

        $importFiasModel = $this->getServiceLocator()->get('importFiasModel');
        $importFiasModel->doSyncFiasAndKladr($this->_forceMode);
    }
}
