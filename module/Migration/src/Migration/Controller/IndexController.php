<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Migration\Controller;


class IndexController extends AbstractMigrationController
{
    public function indexAction()
    {
        return false;
    }

    public function domigrationAction()
    {
    //    error_reporting(0);
        ini_set('memory_limit','14000M');
        $this->prepareParams();


       $geotagsModel = $this->getServiceLocator()->get('geotagsModel');
       $geotagsModel->doMigration($this->_debugMode, true);
       $usersModel = $this->getServiceLocator()->get('usersModel');
       $usersModel->doMigration($this->_debugMode, $this->_forceMode);
        $importFiasModel = $this->getServiceLocator()->get('importFiasModel');
        $importFiasModel->doImport('fias.xml', false);
        $importFiasModel = $this->getServiceLocator()->get('importFiasModel');
        $importFiasModel->doSyncFiasAndKladr($this->_forceMode);

       $ManagementOrgsModel = $this->getServiceLocator()->get('managementOrgsModel');
       $ManagementOrgsModel->doMigration($this->_debugMode, $this->_forceMode);
       $mkdModel = $this->getServiceLocator()->get('mkdModel');
       $mkdModel->doMigration($this->_debugMode, true);
       $ratingUOModel = $this->getServiceLocator()->get('ratingUOModel');
       $ratingUOModel->doMigration($this->_debugMode, true);

       $filesModel = $this->getServiceLocator()->get('filesModel');
       $filesModel->doMigration($this->_debugMode, true);
    }
}
