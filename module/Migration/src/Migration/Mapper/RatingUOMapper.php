<?php

namespace Migration\Mapper;

class RatingUOMapper extends AbstractMapper
{
    public function clearDataFromTblsRating_rgkh2()
    {
        $this->togglePgTblAllTriggers('rating_uo_disclosure', 'disable');
        $this->togglePgTblAllTriggers('rating_uo_index', 'disable');
        $this->togglePgTblAllTriggers('rating_uo_result', 'disable');
        $this->togglePgTblAllTriggers('rating_uo', 'disable');

        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM rating_uo_disclosure"
        );

        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM rating_uo_index"
        );

        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM rating_uo_result"
        );

        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM rating_uo"
        );


        $this->togglePgTblAllTriggers('rating_uo_disclosure', 'enable');
        $this->togglePgTblAllTriggers('rating_uo_index', 'enable');
        $this->togglePgTblAllTriggers('rating_uo_result', 'enable');
        $this->togglePgTblAllTriggers('rating_uo', 'enable');

        return $resultData;
    }

    public function selectDataFromTblRating_rgkh1($table)
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT * FROM $table"
        );

       return $resultData;
    }

    public function insertDataToTblRating_rgkh2($table, $data)
    {
        if ($this->_forceMode)
            $this->togglePgTblAllTriggers($table, 'disable');

        $sql = 'INSERT INTO '.$table.' VALUES';
        $sql_values = array();


        foreach ($data as $row) {
            if (!empty($row['date_start'])) { $row['date_start']=strtotime($row['date_start']); $this->prepareValue($row['date_start'], 'date');}
            if (!empty($row['date_end'])) { $row['date_end']=strtotime($row['date_end']); $this->prepareValue($row['date_end'], 'date');}
            if (!empty($row['date_result'])) { $row['date_result']=strtotime($row['date_result']); $this->prepareValue($row['date_result'], 'date');}
            if (!empty($row['code'])) $this->prepareValue($row['code'], 'string');
            if (!empty($row['description'])) $this->prepareValue($row['description'], 'string');
            if (!empty($row['name'])) $this->prepareValue($row['name'], 'string');
            if (!is_null($row['status'])) $this->prepareValue($row['status'], 'boolean');
            if (!empty($row['P1_7'])) $this->prepareValue($row['P1_7'], 'string');

            foreach ($row as &$field) {
                $this->prepareValue($field,'');
            }

            $sql_values[] = "(".implode(',',$row).")";
        }

        $result = $this->insertByParts($this->getPgSqlEntityManager(), $sql, $sql_values);

        if ($this->_forceMode)
            $this->togglePgTblAllTriggers($table, 'enable');

        return $result;
    }
}
