<?php

namespace Migration\Mapper;

use Doctrine\DBAL\Event\SchemaAlterTableRenameColumnEventArgs;

class FilesMapper extends AbstractMapper
{
    protected $_archiveFiles = array();
    protected $_lastFid = null;

    public function clearDataFromTblFiles_rgkh2()
    {
        $this->togglePgTblAllTriggers('files', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM files"
        );
        $this->togglePgTblAllTriggers('files', 'enable');
        $this->togglePgTblAllTriggers('file_types', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM file_types"
        );
        $this->togglePgTblAllTriggers('file_types', 'enable');
        $this->togglePgTblAllTriggers('file_relation_object', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM file_relation_object"
        );
        $this->togglePgTblAllTriggers('file_relation_object', 'enable');
    }

    public function selectCountFromTblFiles_rgkh1()
    {

        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT count(*) as cnt FROM files"
        );

        return $resultData[0]['cnt'];
    }

    public function selectDataFromTblFiles_rgkh1($offset, $limit)
    {

        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT u.uid as uu, f.* FROM files f left join users u on u.uid = f.uid limit {$offset},{$limit}"
        );

        return $resultData;
    }


    public function insertDataToTblFiles_rgkh2($data)
    {
        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('files', 'disable');

        $sql = 'INSERT INTO files ("id", "name", "extension", "file_hash", "original_name", "path", "date_created", "date_deleted", "user_id") VALUES';
        $sql_values = array();


        foreach ($data as $row) {
            $sql_values_row = array();
            $sql_values_row[] = $row['fid'];

            $filename = explode('.',$row['filename']);
            $original_name = $filename[0];
            $ext = $filename[count($filename)-1];

            $path = 'files/'.date('Y',$row['timestamp']).'/'.date('m',$row['timestamp']).'/'.date('d',$row['timestamp']).'/';

            $solt = '';
       //     $name = sha1( $solt . $original_name . $row['timestamp']);
            $name = basename($row['filepath']);
            $hash = 'todo';//md5_file($path); @TODO

            $sql_values_row[] = $this->prepareValue($name,'string');
            $sql_values_row[] = $this->prepareValue($ext,'string');
            $sql_values_row[] = $this->prepareValue($hash,'string');
            $sql_values_row[] = $this->prepareValue($original_name,'string');
            $sql_values_row[] = $this->prepareValue($path,'string');
            $sql_values_row[] = $this->prepareValue($row['timestamp'],'date');
            $sql_values_row[] = 'null';
            if (empty($row['uu'])) $row['uid'] = 1;
            $sql_values_row[] = $this->prepareValue($row['uid'],'integer');

            $sql_values[] = "(".implode(',',$sql_values_row)." )";
        }

        $result = $this->insertByParts($this->getPgSqlEntityManager(), $sql, $sql_values);

        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('files', 'enable');

        return $result;
    }

    public function insertDataToTblFileTypes_rgkh2()
    {
        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('file_types', 'disable');

        $this->executeQueryWithLog($this->getPgSqlEntityManager(),"INSERT INTO file_types (\"group_name\", \"field_name\") VALUES
                        ('overview', 'prosecute_documents_copies'),
                        ('overview', 'additional_files'),
                        ('financial_indicators', 'annual_financial_statements'),
                        ('financial_indicators', 'revenues_expenditures_estimates'),
                        ('financial_indicators', 'performance_report'),
                        ('financial_indicators', 'general_meetings_protocol'),
                        ('financial_indicators', 'audit_commision_report'),
                        ('financial_indicators', 'audit_reports'),
                        ('management_activities', 'management_contract'),
                        ('management_activities', 'services_cost'),
                        ('management_activities', 'tariffs'),
                        ('contract_periodic_data', 'jobs'),
                        ('contract_periodic_data', 'responsibility'),
                        ('contract_periodic_data', 'cost_service'),
                        ('contract_periodic_data', 'resources_tsz_zsk'),
                        ('contract_periodic_data', 'terms_service_tsz_zsk'),
                        ('users', 'avatar')");


        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('file_types', 'enable');
    }

    public function selectReportingPeriods_rgkh1()
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT id, date_start, date_end, status FROM rating_uo"
        );

        foreach ($resultData as &$row) {
            if ($row['id'] == 4) $row['date_end'] = '2013-12-31';   // @TODO
            if ($row['id'] == 8) $row['date_start'] = '2014-01-01';   // @TODO
        }
        //   $resultData[] = array('id'=>5, 'date_start'=>'2014-01-01', 'date_end'=>'2015-01-01','status'=>'0');

        return $resultData;
    }

    public function migrateUsersAvatars() {

        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('file_relation_object', 'disable');

        echo "Migration user avatars...\n";
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(), "SELECT u.picture, u.created, u.uid FROM users u");

        $ftid = $this->executeQueryWithLog($this->getPgSqlEntityManager(), "select \"id\" from file_types where \"field_name\" = 'avatar'");
        $type_id = $ftid[0]['id'];

        $last_row = $this->executeQueryWithLog($this->getPgSqlEntityManager(), "SELECT id FROM files ORDER BY id DESC LIMIT 1");
        $fid = $last_row[0]['id'];
        $insert_file_rel = array();
        $update_values = array();
        $count_src = count($resultData); $count_dst = 0;
        foreach ($resultData as $row) {
            if (empty($row['picture'])) continue;

            $arr = explode('/',$row['picture']);
            $row['filename'] = $arr[(count($arr) - 1)];

            $filename = explode('.',$row['filename']);
            $original_name = $filename[0];
            $ext = $filename[count($filename)-1];

            $path = 'files/'.date('Y',$row['created']).'/'.date('m',$row['created']).'/'.date('d',$row['created']).'/';

            $solt = '';
          //  $name = sha1( $solt . $original_name . $row['created']);
            $name = $row['filename'];
            $hash = 'not_found';//md5_file($path);

            $sql_values_row = array();
            $fid += 1;
            $sql_values_row[] = $fid;
            $sql_values_row[] = $this->prepareValue($name,'string');
            $sql_values_row[] = $this->prepareValue($ext,'string');
            $sql_values_row[] = $this->prepareValue($hash,'string');
            $sql_values_row[] = $this->prepareValue($original_name,'string');
            $sql_values_row[] = $this->prepareValue($path,'string');
            $sql_values_row[] = $this->prepareValue($row['created'],'date');
            $sql_values_row[] = 'null';
            $sql_values_row[] = $row['uid'];

           $this->executeQueryWithLog($this->getPgSqlEntityManager(), 'INSERT INTO files ("id","name", "extension", "file_hash", "original_name", "path", "date_created", "date_deleted", "user_id") VALUES ('
           .implode(',',$sql_values_row).')');



            $insert_file_rel[] = "($fid, $type_id, {$row['uid']})";

            $count_dst++;
            $p = 100*$count_dst/$count_src;
            echo sprintf("\r%.2f%% Avatars migrated...",$p);

        }

        $this->insertByParts($this->getPgSqlEntityManager(), 'INSERT INTO file_relation_object ("file_id","file_type_id","object_id")  VALUES', $insert_file_rel);unset($insert_file_rel);

        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('file_relation_object', 'enable');
    }

    public function migrateFileField($field_name_1_0, $field_name_2_0, $item) {

        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('file_relation_object', 'disable');

        echo "\nMigration field - $field_name_1_0...\n";

        $ftid = $this->executeQueryWithLog($this->getPgSqlEntityManager(), "select \"id\" from file_types where \"field_name\" = '".$field_name_2_0."'");
        $type_id = $ftid[0]['id'];

        $periods = $this->selectReportingPeriods_rgkh1();

        foreach ($periods as $period)
        {
            if ($period['id'] < 8 && ($item == 'profile_uo_static' || $item == 'profile_uo_periodic')) continue;
            $start_period = strtotime($period['date_start']);
            $end_period = strtotime($period['date_end']);
            echo "\nMigration field - $field_name_1_0 PERIOD [{$period['date_start']} - {$period['date_end']}]...\n";

            $item_ids = array();
            $resultData = array();

            $select = '';
            switch ($item) {
                case 'profile_uo_static':
                    $select = "SELECT f.{$field_name_1_0}_file_fid as fid, o.field_org_company_nid as company_id, nr.timestamp as revision_time FROM content_{$field_name_1_0}_file f join content_type_organization o on o.vid = f.vid join node_revisions nr on nr.vid = o.vid where nr.vid = (select max(nr2.vid) from node_revisions nr2 where nr2.nid = nr.nid and nr2.timestamp >= {$start_period} and nr2.timestamp <= {$end_period}) and o.field_org_company_nid is not null and f.{$field_name_1_0}_file_fid is not null order by f.vid ASC";

                    $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(), $select);

                    if (count($resultData) > 0) {
                        $result = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
                            "SELECT c.id, c.company_id  FROM company_profiles c WHERE  c.reporting_period_id = {$period['id']}");
                        $count_src = count($result); $count_dst = 0;
                        foreach ($result as $result_row) {
                            $item_ids[$result_row['company_id']] = $result_row['id'];

                            $count_dst++;
                            $p = 100*$count_dst/$count_src;
                            echo sprintf("\r%.2f%% Field buffered...",$p);
                        }
                    }
                    break;
                case 'profile_uo_periodic':
                    $select = "SELECT f.{$field_name_1_0}_file_fid as fid, o.field_org_company_nid as company_id, nr.timestamp as revision_time FROM content_{$field_name_1_0}_file f join content_type_organization o on o.vid = f.vid join node_revisions nr on nr.vid = o.vid where nr.vid = (select max(nr2.vid) from node_revisions nr2 where nr2.nid = nr.nid and nr2.timestamp >= {$start_period} and nr2.timestamp <= {$end_period}) and o.field_org_company_nid is not null and f.{$field_name_1_0}_file_fid is not null order by f.vid ASC";

                    $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(), $select);

                    if (count($resultData) > 0) {
                        $result = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
                            "SELECT c.id, c.company_id  FROM company_profiles c WHERE  c.reporting_period_id = {$period['id']}");
                        $count_src = count($result); $count_dst = 0;
                        foreach ($result as $result_row) {
                            $item_ids[$result_row['company_id']] = $result_row['id'];

                            $count_dst++;
                            $p = 100*$count_dst/$count_src;
                            echo sprintf("\r%.2f%% Field buffered...",$p);
                        }
                    }
                    break;
                case 'contract':
                    $select = "SELECT f.{$field_name_1_0}_file_fid as fid, d.field_dgvr_mkd_nid as house_id, nr.timestamp as revision_time FROM content_{$field_name_1_0}_file f join content_type_dogovor d on d.vid = f.vid join node_revisions nr on nr.vid = d.vid WHERE nr.vid = (select max(nr2.vid) from node_revisions nr2 where nr2.nid = nr.nid and nr2.timestamp >= {$start_period} and nr2.timestamp <= {$end_period}) and f.{$field_name_1_0}_file_fid is not null order by f.vid ASC";

                    $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(), $select);

                    if (count($resultData) > 0) {
                        $result = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
                            "SELECT cp.id, c.house_id FROM contract_periodic_data cp JOIN contracts c ON c.id = cp.contracts_id  WHERE reporting_periods_id = {$period['id']}");

                        $count_src = count($result); $count_dst = 0;
                        foreach ($result as $result_row) {
                            $item_ids[$result_row['house_id']] = $result_row['id'];

                            $count_dst++;
                            $p = 100*$count_dst/$count_src;
                            echo sprintf("\r%.2f%% Field buffered...",$p);
                        }
                    }
                    break;
            }

            $insert_file_rel = array();
            $count_dst = 0;
            $count_src = count($resultData);

            if ($count_src == 0) continue;



            echo $count_src."\n";
            foreach ($resultData as $row) {

                $count_dst++;
                $p = 100*$count_dst/$count_src;
                echo sprintf("\r%.2f%% Field migration completed...",$p);


                $item_id = null;
                switch ($item) {
                    case 'profile_uo_static':
                        $item_id = $item_ids[$row['company_id']];
                        break;
                    case 'profile_uo_periodic':
                        $item_id = $item_ids[$row['company_id']];
                        break;
                    case 'contract':
                        $item_id = $item_ids[$row['house_id']];
                        break;
                }

                if ($item_id) {
                    $insert_file_rel[] = "({$row['fid']}, $type_id, $item_id)";
                }

            }
            unset($item_ids);

            if ($insert_file_rel) {
                echo "\rSaving files_rel_objects...";
                $this->insertByParts($this->getPgSqlEntityManager(), 'INSERT INTO file_relation_object ("file_id","file_type_id", "object_id")  VALUES', $insert_file_rel);
                unset($insert_file_rel);
            }
        }

        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('file_relation_object', 'enable');
    }

    public function refactorFiles() {
        $files = $this->executeQueryWithLog($this->getPgSqlEntityManager(), "SELECT * FROM files ORDER BY \"id\" ASC");
        $update_values = array();
        $count_dst = 0;
        $count_src = count($files);
        foreach ($files as $file) {
            $full_path = ''.$file['path'].$file['original_name'].'.'.$file['extension'];
            $new_full_path = ''.$file['path'].$file['name'].'.'.$file['extension'];
          //  $full_path = str_replace('/','\\',$full_path);var_dump($full_path );exit;

              if (file_exists($full_path)) {
                  rename($full_path, $new_full_path);
                  $hash = md5_file($new_full_path);
                  $update_values[] = "({$file['id']},'$hash',true)";
              } else {
                  $update_values[] = "({$file['id']},'not_found',false)";
              }
            $count_dst++;
            $p = 100*$count_dst/$count_src;
            echo sprintf("\r%.2f%% files refactored...",$p);
        }

        echo sprintf("\rSaving to changes to DB...",$p);
        $this->executeQueryWithLog($this->getPgSqlEntityManager(), "UPDATE files SET \"is_actual\" = v.actual_val, \"file_hash\" = v.hash_val FROM (VALUES ".implode(',',$update_values).") as v(fid, hash_val, actual_val) WHERE id = v.fid");
    }

    public function migrateArchiveFiles()
    {

        $filepath = 'ank_archive/period_';

        $last_row = $this->executeQueryWithLog($this->getPgSqlEntityManager(), "SELECT id FROM files ORDER BY id DESC LIMIT 1");
        $this->_lastFid = $last_row[0]['id'];

        $periods = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT r.id, r.status, r.date_result FROM rating_uo r"
        );

        foreach ($periods as $period){
            if ($period['status']=='1') continue;

            $item_ids = array();
            $result = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
                "SELECT c.id, c.company_id  FROM company_profiles c WHERE  c.reporting_period_id = {$period['id']}");
            $count_src = count($result); $count_dst = 0;
            foreach ($result as $result_row) {
                $item_ids[$result_row['company_id']] = $result_row['id'];

                $count_dst++;
                $p = 100*$count_dst/$count_src;
                echo sprintf("\r%.2f%% Field buffered...",$p);
            }

            echo "Migration archive files by ".$period['id']." period\n";

            $uoArray = $this->executeQueryWithLog($this->getPgSqlEntityManager(), "SELECT o.id, urr.user_id FROM management_organizations o JOIN company_profiles c ON c.company_id = o.id JOIN user_registration_requests urr ON urr.id = o.id WHERE c.reporting_period_id = {$period['id']} ASC");

            $count_src = count($uoArray);
            foreach ($uoArray as $i=>$uo){
                $uo_filepath = $filepath.$period['id'].'/'.$uo['id'];
                if (!is_dir($uo_filepath)) continue;
                $this->scanAndSaveFiles($item_ids,$uo['id'],$uo['user_id'],$uo_filepath,'tarifs','tariffs',strtotime($period['date_result']));
                $this->scanAndSaveFiles($item_ids,$uo['id'],$uo['user_id'],$uo_filepath,'uslugi','services_cost',strtotime($period['date_result']));
                $this->scanAndSaveFiles($item_ids,$uo['id'],$uo['user_id'],$uo_filepath,'dogovors','management_contract',strtotime($period['date_result']));
                $this->scanAndSaveFiles($item_ids,$uo['id'],$uo['user_id'],$uo_filepath,'buh','annual_financial_statements',strtotime($period['date_result']));
                $this->scanAndSaveFiles($item_ids,$uo['id'],$uo['user_id'],$uo_filepath,'extras','additional_files',strtotime($period['date_result']));
                $this->scanAndSaveFiles($item_ids,$uo['id'],$uo['user_id'],$uo_filepath,'copy_docs','prosecute_documents_copies',strtotime($period['date_result']));
                $this->scanAndSaveFiles($item_ids,$uo['id'],$uo['user_id'],$uo_filepath,'reports_gsk','revenues_expenditures_estimates',strtotime($period['date_result']));
                $this->scanAndSaveFiles($item_ids,$uo['id'],$uo['user_id'],$uo_filepath,'reports_finish_gsk','performance_report',strtotime($period['date_result']));
                $this->scanAndSaveFiles($item_ids,$uo['id'],$uo['user_id'],$uo_filepath,'member_protocols','general_meetings_protocol',strtotime($period['date_result']));
                $this->scanAndSaveFiles($item_ids,$uo['id'],$uo['user_id'],$uo_filepath,'answer_reports','audit_commision_report',strtotime($period['date_result']));
                $this->scanAndSaveFiles($item_ids,$uo['id'],$uo['user_id'],$uo_filepath,'audit_reports','audit_reports',strtotime($period['date_result']));


                $p = 100*($i+1)/$count_src;
                echo sprintf("\r%.2f%% files archive scaned...",$p);
            }
        }
    }

    private function scanAndSaveFiles($item_ids,$uo_id, $user_id, $uo_filepath, $fileType, $field_name_2_0, $dateArchive)
    {

        $ftid = $this->executeQueryWithLog($this->getPgSqlEntityManager(), "select \"id\" from file_types where \"field_name\" = '".$field_name_2_0."'");
        $type_id = $ftid[0]['id'];

        $sql_files = 'INSERT INTO files ("id", "name", "extension", "file_hash", "original_name", "path", "date_created", "date_deleted","user_id") VALUES';
        $sql_files_rel = 'INSERT INTO file_relation_object ("file_id","file_type_id", "object_id")  VALUES';
        $archiveFiles = array();
        $archiveFilesRel = array();

        if (is_dir($uo_filepath.'/'.$fileType)) {
            $cdir = scandir($uo_filepath.'/'.$fileType);
            foreach ($cdir as $key => $value)
            {
                if (!in_array($value,array(".","..")))
                {
                    $filename = explode('.',$value);
                    $original_name = $filename[0];
                    $ext = $filename[count($filename)-1];
                    $path = $uo_filepath.'/'.$fileType.'/'.$value;
                    $solt = '';
                    $name = sha1( $solt . $original_name . $dateArchive);
                    $hash = 'todo';//md5_file($path); @TODO

                    $sql_values_row = array();
                    $this->_lastFid += 1;
                    $sql_values_row[] = $this->_lastFid;
                    $sql_values_row[] = $this->prepareValue($name,'string');
                    $sql_values_row[] = $this->prepareValue($ext,'string');
                    $sql_values_row[] = $this->prepareValue($hash,'string');
                    $sql_values_row[] = $this->prepareValue($original_name,'string');
                    $sql_values_row[] = $this->prepareValue($path,'string');
                    $sql_values_row[] = $this->prepareValue($dateArchive,'date');
                    $sql_values_row[] = 'null';
                    $sql_values_row[] = $user_id;

                    $archiveFiles[] = "(".implode(',',$sql_values_row)." )";

                    $sql_values_row = array();
                    $sql_values_row[] = $this->_lastFid;
                    $sql_values_row[] = $type_id;
                    $sql_values_row[] = $item_ids[$uo_id];
                    $archiveFilesRel[] = "(".implode(',',$sql_values_row)." )";
                }
            }

            $this->insertByParts($this->getPgSqlEntityManager(), $sql_files, $archiveFiles);
            $this->insertByParts($this->getPgSqlEntityManager(), $sql_files_rel, $archiveFilesRel);
        }
    }


}
