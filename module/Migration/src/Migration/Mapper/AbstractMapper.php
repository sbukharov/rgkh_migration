<?php

namespace Migration\Mapper;

use Doctrine\ORM\EntityManager;
use Migration\Utils\SqlLogger;
use Migration\Utils\AddressManager;
use Zend\Db\Sql\Ddl\Column\Integer;

class AbstractMapper implements  InterfaceMapper
{
    /**
     * debug mode (inserting by 1 row and not rollback transaction then SQLState error)
     * @var bool
     */
    protected $_debugMode = false;

    /**
     * force mode (inserting without FK checks)
     * @var bool
     */
    protected $_forceMode = false;

    /**
     * enable/disable logging sql-queries
     * @var bool
     */
    protected $_logging = true;

    /**
     * mySQL entity manager
     * @var \Doctrine\ORM\EntityManager
     */
    protected $_mysqlEntityManager;

    /**
     * mySQL entity manager
     * @var \Doctrine\ORM\EntityManager
     */
    protected $_mysqlEntityManagerSecond;

    /**
     * pgSQL entity manager
     * @var \Doctrine\ORM\EntityManager
     */
    protected $_pgsqlEntityManager;

    protected $_errorShowed = false;

    protected $_addressManager = null;

    /**
     * @param boolean $debugMode
     */
    public function setDebugMode($debugMode)
    {
        $this->_debugMode = $debugMode;
    }

    /**
     * @return boolean
     */
    public function getDebugMode()
    {
        return $this->_debugMode;
    }

    /**
     * @param boolean $forceMode
     */
    public function setForceMode($forceMode)
    {
        $this->_forceMode = $forceMode;
    }

    /**
     * @return boolean
     */
    public function getForceMode()
    {
        return $this->_forceMode;
    }

    /**
     * @param boolean $logging
     */
    public function setLogging($logging)
    {
        $this->_logging = $logging;
    }

    /**
     * @return boolean
     */
    public function getLogging()
    {
        return $this->_logging;
    }

    /**
     * @return AddressManager
     */
    public function getAddressManager()
    {
        return $this->_addressManager;
    }

    /**
     * @param \Doctrine\ORM\EntityManager $mysqlEntityManager
     */
    public function setMysqlEntityManager($mysqlEntityManager)
    {
        $this->_mysqlEntityManager = $mysqlEntityManager;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getMysqlEntityManager()
    {
        return $this->_mysqlEntityManager;
    }

    /**
     * @param \Doctrine\ORM\EntityManager $mysqlEntityManager
     */
    public function setMysqlEntityManagerSecond($mysqlEntityManager)
    {
        $this->_mysqlEntityManagerSecond = $mysqlEntityManager;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getMysqlEntityManagerSecond()
    {
        return $this->_mysqlEntityManagerSecond;
    }


    /**
     * @param \Doctrine\ORM\EntityManager $pgsqlEntityManager
     */
    public function setPgsqlEntityManager($pgsqlEntityManager)
    {
        $this->_pgsqlEntityManager = $pgsqlEntityManager;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getPgsqlEntityManager()
    {
        return $this->_pgsqlEntityManager;
    }

    public function __construct(EntityManager $mysql_em, EntityManager $pgsql_em, EntityManager $mysql_em_s = null)
    {
        $this->_mysqlEntityManager = $mysql_em;
        $this->_pgsqlEntityManager = $pgsql_em;
        $this->_mysqlEntityManagerSecond = $mysql_em_s;

        $this->_addressManager = new AddressManager();
    }

    protected function prepareQuery(&$query)
    {
        $query = str_replace('?','/?',$query);
    }

    /**
     * executing native sql with logging
     * @param EntityManager $em
     * @param string $query
     */
    public function executeQueryWithLog($em, $query, $returnCount = false, $convFrom = 'windows-1251')
    {
        $logger = new SqlLogger();

        try {
            $em->getConnection()->beginTransaction();
            /**
             * var $exResult \Doctrine\DBAL\Driver\Statement
             */
            if (!empty($convFrom))
                $query = iconv($convFrom,"utf-8", $query);

        //    $this->prepareQuery($query);
            $exResult = $this->tryExecuteQuerySeveralTimes($em, $query);
            $em->getConnection()->commit();
        } catch (\Exception $e) {


            if (!$this->_errorShowed)
            {
                echo "Error executing sql. See SQLerrors.log!\n";
                $this->_errorShowed = true;
            }
            if ($this->_logging){
                $logger->loggingQueryError($e->getMessage());
            }

            if(!$this->_debugMode) {
                $em->getConnection()->rollBack();
          //      $em->getConnection()->close();
                return null;
            }


        }

        $em->clear();

        if ($this->_logging)
            $logger->loggingQuery($query, $exResult->rowCount());

        if ($returnCount)  return $exResult->rowCount();

        return $exResult->fetchAll();
    }

    /**
     * executing native sql with logging
     * @param EntityManager $em
     * @param string $str_query
     */
    protected function executeTransactionWithLog($em, array $queries, $returnCount = false, $convFrom = 'windows-1251')
    {
        $logger = new SqlLogger();

        $countRows = 0;
        $resultArray = array();

        if (!$this->_debugMode) {
            $this->tryBeginTransaction($em);
        }

            foreach ($queries as $sql_query)
            {
                try {
                    /**
                     * var $exResult \Doctrine\DBAL\Driver\Statement
                     */
                    if (!empty($convFrom))
                        $sql_query = iconv($convFrom,"utf-8", $sql_query);

                //    $this->prepareQuery($sql_query);

                    $exResult = $this->tryExecuteQuerySeveralTimes($em, $sql_query);

                    if ($returnCount)
                        $countRows += $exResult->rowCount();
                    else
                        $resultArray = array_merge($resultArray,$exResult->fetchAll());

                    if ($this->_logging)
                        $logger->loggingQuery($sql_query, $exResult->rowCount());

                } catch (\Exception $e) {
                    if (!$this->_errorShowed)
                    {
                        echo "Error executing sql. See SQLerrors.log!\n";
                        $this->_errorShowed = true;
                    }
                    if ($this->_logging){
                        $logger->loggingQueryError($e->getMessage());
                    }
                    if (!$this->_debugMode) {
                        $em->getConnection()->rollBack();
                        $em->getConnection()->close();
                        return null;
                    }
                }
            }
        if (!$this->_debugMode) {
            $this->tryCommitTransaction($em);
        }

        $em->clear();

        unset($queries);
        if ($returnCount) return $countRows;

        return $resultArray;
    }

    /**
     * insert data by parts
     * @param EntityManager $em
     * @param string $query
     * @param array $values
     * @return int rowscount
     */
    protected function insertByParts($em, $query, array $values, $partSize = 1000, $convFrom = 'windows-1251')
    {
        $SQLs = array();
        $i = 0;

        if ($this->_debugMode) {
            foreach($values as $value)
            {
                $SQLs[] = $query.$value;
            }
        } else {
            while ($i < count($values))
            {
                $valuesPart = array_slice($values, $i, $partSize);
                $SQLs[] = $query.implode(",",$valuesPart);

                $i+=$partSize;
            }
        }
        unset($values);

        return $this->executeTransactionWithLog($em, $SQLs, true, $convFrom);
    }

    /**
     * toggle pgsql's table all triggers
     * @param string $table
     * @param string $state    enabled|disabled
     */
    protected function togglePgTblAllTriggers($table, $state)
    {
        $this->getPgSqlEntityManager()->getConnection()->executeQuery("alter table {$table} {$state} trigger all;");
    }

    protected function escape_string($data)
    {
        // return pg_escape_string($data);
        return str_replace("'","''",$data);
    }

    public function getFiasDataByAisid($aisId)
    {
        if (isset($aisId)){
            $data = $this->executeQueryWithLog($this->getPgSqlEntityManager(), "SELECT \"aoguid\", \"shortname\", \"formalname\" FROM fias_addrobj WHERE \"aisid\" = {$aisId}");
            return isset($data)?$data[0]:null;
        }
        return null;
    }

    public function compareAisWithFias($region_ais_id, $area_ais_id, $city_ais_id, $street_ais_id, $h_number, $h_korp, $h_part, $h_room, $comment)
    {

        $regionFiasData = $this->_fiasArray[$region_ais_id];
        $areaFiasData = $this->_fiasArray[$area_ais_id];
        if (!in_array($city_ais_id,array(11177399,11184976))) $cityFiasData = $this->_fiasArray[$city_ais_id];
        $streetFiasData = $this->_fiasArray[$street_ais_id];
/*
        $regionFiasData = $this->getFiasDataByAisid($region_ais_id);
        $areaFiasData = $this->getFiasDataByAisid($area_ais_id);
        $cityFiasData = $this->getFiasDataByAisid($city_ais_id);
        $streetFiasData = $this->getFiasDataByAisid($street_ais_id);
*/
        $row = array();
        if (!empty($region_ais_id) && isset($regionFiasData)) {
            $row['region_guid'] = $regionFiasData['aoguid'];
            $row['region_type'] = $regionFiasData['shortname'];
            $row['region_name'] = $regionFiasData['formalname'];
        }
        if (!empty($area_ais_id) && isset($areaFiasData)) {
            $row['area_guid'] = $areaFiasData['aoguid'];
            $row['area_type'] = $areaFiasData['shortname'];
            $row['area_name'] = $areaFiasData['formalname'];
        }
        if (!empty($city_ais_id) && isset($cityFiasData)) {
            $row['city_guid'] = $cityFiasData['aoguid'];
            $row['city_type'] = $cityFiasData['shortname'];
            $row['city_name'] = $cityFiasData['formalname'];
        }
        if (!empty($street_ais_id) && isset($streetFiasData)) {
            $row['street_guid'] = $streetFiasData['aoguid'];
            $row['street_type'] = $streetFiasData['shortname'];
            $row['street_name'] = $streetFiasData['formalname'];
        }
        $row['house_number'] = !empty($h_number) ? $this->escape_string(iconv('windows-1251','utf-8',$h_number)) : null;
        $row['housing'] = !empty($h_korp) ? $this->escape_string(iconv('windows-1251','utf-8',$h_korp)) : null;
        $row['building'] = !empty($h_part) ? $this->escape_string(iconv('windows-1251','utf-8',$h_part)) : null;
        $row['room'] = !empty($h_room) ? $this->escape_string(iconv('windows-1251','utf-8',$h_room)) : null;

        $row['flag_invalid'] = '';
        $row['address_comment'] = !empty($comment) ? $this->escape_string(iconv('windows-1251','utf-8',$comment)) : null;

        if (empty($region_ais_id))
        {
            $row['flag_invalid'] .= (!empty($row['flag_invalid'])?',':'').'21';
            $row['comment'] = 'has null required AO';
        }
        if (empty($area_ais_id))
        {
            $row['flag_invalid'] .= (!empty($row['flag_invalid'])?',':'').'22';
            $row['comment'] = 'has null required AO';
        }
        if (empty($naspunkt_ais_id))
        {
            $row['flag_invalid'] .= (!empty($row['flag_invalid'])?',':'').'23';
            $row['comment'] = 'has null required AO';
        }
        if (empty($street_ais_id))
        {
            $row['flag_invalid'] .= (!empty($row['flag_invalid'])?',':'').'14';
            $row['comment'] = 'has null street';
        }
        if (empty($h_number))
        {
            $row['flag_invalid'] .= (!empty($row['flag_invalid'])?',':'').'25';
            $row['comment'] = 'has null required AO';
        }

        return $row;
    }

    protected function prepareValue(&$value, $type = 'string'){
        if ($value == null) {
            $value = "null";
            return $value;
        }

        switch ($type) {
            case "string": $value = "'".$this->escape_string($value)."'";break;
            case "integer": $value = intval($value);break;
            case "boolean": $value = !empty($value) ? "true": "false";break;
            case "date": $value = "'".date(self::DATE_FORMAT, intval($value))."'";break;
            case "": break;
            default: $value = "'$value'";
        }

        return $value;
    }

    protected function tryExecuteQuerySeveralTimes($em, $query, $tryTimes = 0)
    {
        try {
            $exResult = $em->getConnection()->executeQuery($query);
        } catch(\Exception $e) {
            if ($tryTimes < 5) {
                sleep(3); //пусть постгрес отдохнет несколько секунд
                $exResult = $this->tryExecuteQuerySeveralTimes($em, $query, ++$tryTimes);
            } else {
                throw new \Exception('Cant execute query: "' . $query .'"  5 times');
            }
        }


        return $exResult;
    }

    protected function tryBeginTransaction($em, $tryTimes = 0)
    {
        try {
            $em->getConnection()->beginTransaction();
        } catch(\Exception $e) {
            if ($tryTimes < 5) {
                sleep(3); //пусть постгрес отдохнет несколько секунд
                $this->tryBeginTransaction($em, ++$tryTimes);
            } else {
                throw new \Exception('Cant begin Transaction 5 times');
            }
        }
    }

    protected function tryCommitTransaction($em, $tryTimes = 0)
    {
        try {
            $em->getConnection()->commit();
        } catch(\Exception $e) {
            if ($tryTimes < 5) {
                sleep(3); //пусть постгрес отдохнет несколько секунд
                $this->tryBeginTransaction($em, ++$tryTimes);
            } else {
                throw new \Exception('Cant commit Transaction 5 times');
            }
        }
    }
}
