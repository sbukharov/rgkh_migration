<?php

namespace Migration\Mapper;

class UsersMapper extends AbstractMapper
{

    /** ---------------------------------- Users ------------------------------------------- */

    public function clearDataFromTblUsers_rgkh2()
    {
        $this->togglePgTblAllTriggers('users_v2', 'disable');

        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM users_v2"
        );

        $this->togglePgTblAllTriggers('users_v2', 'enable');

        return $resultData;
    }

    public function selectDataFromTblUsers_rgkh1()
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT u.mail, u.uid, u.name, u.login, u.pass, u.status, u.created, n.title as display_name FROM users as u join node n on n.uid = u.uid WHERE n.type='profile' GROUP BY u.mail ORDER BY u.uid ASC"
        );

        return $resultData;
    }

    public function insertDataToTblUsers_rgkh2($data)
    {
        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('users_v2', 'disable');

        $sql = 'INSERT INTO users_v2 ("user_id", "display_name", "username", "password", "email", "date_blocked", "date_deleted", "date_created","advanced") VALUES';
        $sql_values = array();


        foreach ($data as $row) {
            if ($row['name']=='?') continue;
            $row['name'] = $this->escape_string($row['name']);
            $row['display_name'] = $this->escape_string($row['display_name']);
            $row['mail'] = $this->escape_string($row['mail']);
            $row['login'] = $this->escape_string($row['login']);
            $row['blocked'] = $row['status'] == '1' ? 'null' : 'now()'; //@TODO верно ли, что flag_blocked != (bool)status ?
            $row['deleted'] = 'null';
            $row['created'] = date(self::DATE_FORMAT, $row['created']);
            $sql_values[] = "(".$row['uid'].", '".$row['display_name']."', '".$row['name']."', '".$row['pass']."', '".$row['mail']."', ".$row['blocked'].", ".$row['deleted'].", '".$row['created']."','')";
        }

        $result = $this->insertByParts($this->getPgSqlEntityManager(), $sql, $sql_values);

        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('users_v2', 'enable');

        return $result;
    }

    /** ---------------------------------- Roles ------------------------------------------- */

    public function clearDataFromTblRoles_rgkh2()
    {
        $this->togglePgTblAllTriggers('roles_v2', 'disable');

        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM roles_v2"
        );

        $this->togglePgTblAllTriggers('roles_v2', 'enable');

        return $resultData;
    }

    public function selectDataFromTblRoles_rgkh1()
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT * FROM role as r GROUP BY r.name ORDER BY r.rid DESC"
        );

        return $resultData;
    }

    public function insertDataToTblRoles_rgkh2($data)
    {
        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('roles_v2', 'disable');

        $sql = 'INSERT INTO roles_v2 ("id","name","description","role_type") VALUES';
        $sql_values = array();


        foreach ($data as $row) {
            $row['name'] = $this->escape_string($row['name']);
            $sql_values[] = "(".$row['rid'].", '".$row['name']."', 'no-description','global')";  //@TODO: 'global' || 'geo' признак?
        }

        $result = $this->insertByParts($this->getPgSqlEntityManager(), $sql, $sql_values);

        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('roles_v2', 'enable');

        return $result;
    }

    /** ---------------------------------- Users Term/Departament Roles ------------------------------------------- */

    public function clearDataFromTblUsersTermRoles_rgkh2()
    {
        $this->togglePgTblAllTriggers('departament_users', 'disable');

        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM departament_users"
        );

        $this->togglePgTblAllTriggers('departament_users', 'enable');

        return $resultData;
    }

    public function selectDataFromTblUsersTermRoles_rgkh1()
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT ur.* FROM users_term_roles as ur where ur.uid in (select u.uid from users u) ORDER BY ur.uid ASC"
        );

        return $resultData;
    }

    public function insertDataToTblUsersTermRoles_rgkh2($data)
    {
        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('departament_users', 'disable');

        $sql = 'INSERT INTO departament_users ("user_id","geo_tag_id","role_id") VALUES';
        $sql_values = array();


        foreach ($data as $row) {
            $sql_values[] = "(".$row['uid'].", ".$row['tid'].", ".$row['rid'].")";
        }

        $result = $this->insertByParts($this->getPgSqlEntityManager(), $sql, $sql_values);

        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('departament_users', 'enable');

        return $result;
    }

    /** ---------------------------------- Users_Roles ------------------------------------------- */

    public function clearDataFromTblUsersRoles_rgkh2()
    {
        $this->togglePgTblAllTriggers('users_to_roles_v2', 'disable');

        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM users_to_roles_v2"
        );

        $this->togglePgTblAllTriggers('users_to_roles_v2', 'enable');

        return $resultData;
    }

    public function selectDataFromTblUsersRoles_rgkh1()
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT ur.* FROM users_roles as ur where ur.uid in (select u.uid from users u) ORDER BY ur.uid ASC"
        );

        return $resultData;
    }

    public function insertDataToTblUsersRoles_rgkh2($data)
    {
        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('users_to_roles_v2', 'disable');

        $sql = 'INSERT INTO users_to_roles_v2 ("user_id","role_id") VALUES';
        $sql_values = array();


        foreach ($data as $row) {
            $sql_values[] = "(".$row['uid'].", ".$row['rid'].")";
        }

        $result = $this->insertByParts($this->getPgSqlEntityManager(), $sql, $sql_values);

        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('users_to_roles_v2', 'enable');

        return $result;
    }
}
