<?php

namespace Migration\Mapper;

class MkdMapper extends AbstractMapper
{
    protected $_fiasArray = array();
    protected $_addressesArray = array();
    protected $_h_adr_id = "null";
    protected $_count_idents = 0;
    protected $_adr_id = 0;
    protected $_house_revision_id = 0;
    protected $_house_pss_builder_id = 0;
    protected $_profile_periodic_id = 0;
    protected $_contracts_id = 0;
    protected $_houses_array = array();
    protected $_houses_vids = array();
    protected $_company_contracts = array();
    protected $_offset = 0;
    protected $_ctp_ids = array();



    public function getCountIdents()
    {
        return $this->_count_idents;
    }

    public function clearDataFromTblHouses_rgkh2()
    {
        $this->togglePgTblAllTriggers('houses', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM houses"
        );
        $this->togglePgTblAllTriggers('houses', 'enable');

        $this->togglePgTblAllTriggers('house_supply_systems', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM house_supply_systems"
        );
        $this->togglePgTblAllTriggers('house_supply_systems', 'enable');

        $this->togglePgTblAllTriggers('house_facade', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM house_facade"
        );
        $this->togglePgTblAllTriggers('house_facade', 'enable');

        $this->togglePgTblAllTriggers('house_roof', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM house_roof"
        );
        $this->togglePgTblAllTriggers('house_roof', 'enable');

        $this->togglePgTblAllTriggers('house_passport', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM house_passport"
        );
        $this->togglePgTblAllTriggers('house_passport', 'enable');

        $this->togglePgTblAllTriggers('house_passport_builder', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM house_passport_builder"
        );
        $this->togglePgTblAllTriggers('house_passport_builder', 'enable');

        $this->togglePgTblAllTriggers('house_revision', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM house_revision"
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "ALTER TABLE house_revision ADD COLUMN temp_id_mkd bigint"
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "ALTER TABLE house_revision ADD COLUMN temp_vid bigint"
        );
        $this->togglePgTblAllTriggers('house_revision', 'enable');

        $this->togglePgTblAllTriggers('house_profile_supply_systems', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM house_profile_supply_systems"
        );
        $this->togglePgTblAllTriggers('house_profile_supply_systems', 'enable');

        $this->togglePgTblAllTriggers('house_profile_facade', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM house_profile_facade"
        );
        $this->togglePgTblAllTriggers('house_profile_facade', 'enable');

        $this->togglePgTblAllTriggers('house_profile_roof', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM house_profile_roof"
        );
        $this->togglePgTblAllTriggers('house_profile_roof', 'enable');

        $this->togglePgTblAllTriggers('house_profile_passport', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM house_profile_passport"
        );
        $this->togglePgTblAllTriggers('house_profile_passport', 'enable');

        $this->togglePgTblAllTriggers('house_profile_builder', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM house_profile_builder"
        );
        $this->togglePgTblAllTriggers('house_profile_builder', 'enable');

        $this->togglePgTblAllTriggers('house_profile_revision', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM house_profile_revision"
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "ALTER TABLE house_profile_revision ADD COLUMN temp_vid bigint"
        );
        $this->togglePgTblAllTriggers('house_profile_revision', 'enable');

        $this->togglePgTblAllTriggers('house_profile_periodic_data', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM house_profile_periodic_data"
        );
        $this->togglePgTblAllTriggers('house_profile_periodic_data', 'enable');

        $this->togglePgTblAllTriggers('house_profile_accumulative_data', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM house_profile_accumulative_data"
        );
        $this->togglePgTblAllTriggers('house_profile_accumulative_data', 'enable');

        $this->togglePgTblAllTriggers('contracts', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM contracts"
        );
        $this->togglePgTblAllTriggers('contracts', 'enable');

        $this->togglePgTblAllTriggers('contract_periodic_data', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM contract_periodic_data"
        );
        $this->togglePgTblAllTriggers('contract_periodic_data', 'enable');

        $this->togglePgTblAllTriggers('providers_rel_house_profile', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM providers_rel_house_profile"
        );
        $this->togglePgTblAllTriggers('providers_rel_house_profile', 'enable');

        $this->togglePgTblAllTriggers('system_event', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM system_event"
        );
        $this->togglePgTblAllTriggers('system_event', 'enable');

        $this->togglePgTblAllTriggers('reporting_periods', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM reporting_periods"
        );
        $this->togglePgTblAllTriggers('reporting_periods', 'enable');

    }

    public function clearDataFromTblAddresses_rgkh2()
    {
        $this->togglePgTblAllTriggers('addresses', 'disable');

        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM addresses"
        );

        $this->togglePgTblAllTriggers('addresses', 'enable');

        return $resultData;
    }

    public function selectFiasData_rgkh2()
    {
        $fiasData = $this->executeQueryWithLog($this->getPgSqlEntityManager(), "SELECT \"aoguid\", \"shortname\", \"formalname\", \"aisid\" FROM fias_addrobj WHERE actstatus = '1'");
        foreach ($fiasData as $i=> &$fiasRow) {
            $this->_fiasArray[$fiasRow['aisid']] = array('aoguid' => $fiasRow['aoguid'], 'shortname' => $fiasRow['shortname'], 'formalname' => $fiasRow['formalname']);
            unset($fiasData[$i]);
            unset($fiasRow);
        }
        unset($fiasData);
    }

    public function unsetFiasData()
    {
        unset($this->_fiasArray);
        unset($this->_addressesArray);
    }

    public function selectLastIds()
    {


        echo "Get last house revisions...\n";
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "select max(\"id\") as last_id from house_profile_revision");

        $this->_house_revision_id = (int)$resultData[0]['last_id'];

        echo "Get last house builders...\n";
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "select max(\"id\") as last_id from house_profile_builder");

        $this->_house_pss_builder_id = (int)$resultData[0]['last_id'];

        echo "Get last contracts...\n";
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "select \"id\",\"house_id\" from contracts");

        foreach ($resultData as $i=>$r) {
            $this->_company_contracts[(int)$r['house_id']] =(int) $r['id'];
            unset($resultData[$i]);
        }
        unset($resultData);

        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "select max(\"id\") as last_id from contracts");

        $this->_contracts_id = (int)$resultData[0]['last_id'];
        unset($resultData);

        echo "Get last houses...\n";
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "select \"id\" from houses");

        foreach ($resultData as $i=>$r) {
            $this->_houses_array[] =(int) $r['id'];
            unset($resultData[$i]);
        }
        unset($resultData);

        echo "Get last addresses...\n";
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "select max(\"id\") as last_id from addresses");

        $this->_adr_id = (int)$resultData[0]['last_id'];
        unset($resultData);

        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "select \"id\", \"full_address\" from addresses");

        foreach ($resultData as $i=>$row) {
            $this->_addressesArray[(int)$row['full_address']] = (int)$row['id'];
            unset($resultData[$i]);
        }
        unset($resultData);
    }

    public function selectReportingPeriods_rgkh1()
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT r.id, r.date_start, r.date_end, r.date_result, r.name, r.status, r.avg_rating FROM rating_uo r"
        );

        foreach ($resultData as &$row) {
            if ($row['id'] == 3) $row['date_start'] = '2012-07-01';
            if ($row['id'] == 4) $row['date_end'] = '2014-04-23';   // @TODO  2014-05-06
            if ($row['id'] == 8) $row['date_start'] = '2014-04-24';   // @TODO 2014-05-07
        }

        $resultData[] = array('id'=>9,'date_start'=>'2011-01-01','date_end'=>'2013-08-31','date_result'=>'2013-08-31','name'=>'фиктивный период','status'=>'fictitious','avg_rating'=>'');
     //   $resultData[] = array('id'=>5, 'date_start'=>'2014-01-01', 'date_end'=>'2015-01-01','status'=>'0');

        return $resultData;
    }

    public function insertDataToTblPeriods_rgkh2($data)
    {
        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('reporting_periods', 'disable');

        $sql = 'INSERT INTO reporting_periods ("id", "name", "state", "date_start", "date_end", "date_result", "rating") VALUES';
        $sql_values = array();

        foreach ($data as $row) {
            $values = array();
            if ($row['id'] == 4) $row['date_end'] = '2013-12-31';   // @TODO
            if ($row['id'] == 8) $row['date_start'] = '2014-01-01';   // @TODO
            $values[] = $this->prepareValue($row['id'],'integer');
            $values[] = $this->prepareValue($row['name'],'string');
            if ($row['status']==1) $row['status'] = 'current';
            else if ($row['status']==0) $row['status'] = 'archive';
            $values[] = $this->prepareValue($row['status'],'string');
            $values[] = $this->prepareValue($row['date_start'],'string');
            $values[] = $this->prepareValue($row['date_end'],'string');
            $values[] = $this->prepareValue($row['date_result'],'string');
            $values[] = (empty($row['avg_rating']) ? "false" : "true");
            $sql_values[] = '('.implode(',',$values).')';
        }

        $this->insertByParts($this->getPgSqlEntityManager(), $sql, $sql_values); unset($sql_values);

        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('reporting_periods', 'enable');
    }


    public function insertDataToTblSysEvents_rgkh2()
    {
        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('system_event', 'disable');

        $this->executeQueryWithLog($this->getPgsqlEntityManager(),
            'INSERT INTO system_event ("event_id", "event_name") VALUES(1, \'newPeriod\')');
        $this->executeQueryWithLog($this->getPgsqlEntityManager(),
            'INSERT INTO system_event ("event_id", "event_name") VALUES(2, \'linkToOrganization\')');
        $this->executeQueryWithLog($this->getPgsqlEntityManager(),
            'INSERT INTO system_event ("event_id", "event_name") VALUES(3, \'unlinkExpiredFromOrganization\')');
        $this->executeQueryWithLog($this->getPgsqlEntityManager(),
            'INSERT INTO system_event ("event_id", "event_name") VALUES(4, \'unlinkIncorrectFromOrganization\')');
        $this->executeQueryWithLog($this->getPgsqlEntityManager(),
            'INSERT INTO system_event ("event_id", "event_name") VALUES(5, \'unlinkRecoveryFromOrganization\')');

        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('system_event', 'enable');
    }

    public function selectCountFromTblsMkd_rgkh1($startPeriod, $endPeriod)
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT count(*) as count FROM node_revisions as nr
             INNER JOIN node as n ON nr.nid = n.nid
             INNER JOIN content_type_tp ctp ON ctp.vid = nr.vid
             LEFT JOIN company_tp ct ON ct.tp_nid = nr.nid and ct.status = 1
             LEFT JOIN content_type_finance_values ctf ON ctf.field_fin_mkd_nid = nr.nid AND ctf.vid = (select MAX(nrf.vid) from node nf join node_revisions nrf on nrf.nid = nf.nid where nf.type='finance_values' and nf.nid = ctf.nid and nrf.timestamp <=  {$endPeriod} )
             LEFT JOIN content_type_dogovor ctd ON ctd.field_dgvr_mkd_nid = nr.nid AND ctd.vid = (select MAX(nrd.vid) from node nd join node_revisions nrd on nrd.nid = nd.nid where nd.type='dogovor' and nd.nid = ctd.nid and nrd.timestamp <=  {$endPeriod})
             LEFT JOIN content_type_kladr_region rg ON rg.nid = ctp.field_tp_kladr_region_nid
             LEFT JOIN content_type_kladr_raion r ON r.nid = ctp.field_tp_kladr_raion_nid
             LEFT JOIN content_type_kladr_naspunkt np ON np.nid = ctp.field_tp_kladr_naspunkt_nid
             LEFT JOIN content_type_kladr_street s ON s.nid = ctp.field_tp_kladr_street_nid
             WHERE n.type = 'tp'
             and nr.vid = (select MAX(nr2.vid) from node_revisions nr2 where nr2.nid = nr.nid and nr2.timestamp <=  {$endPeriod})
             "
        );

        return $resultData;
    }

    public function selectOffsetFromTblsMkd_rgkh1($startPeriod, $endPeriod, $min)
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT nr.nid FROM node_revisions as nr
             INNER JOIN node as n ON nr.nid = n.nid
             INNER JOIN content_type_tp ctp ON ctp.vid = nr.vid
             LEFT JOIN company_tp ct ON ct.tp_nid = nr.nid and ct.status = 1
             LEFT JOIN content_type_finance_values ctf ON ctf.field_fin_mkd_nid = nr.nid AND ctf.vid = (select MAX(nrf.vid) from node nf join node_revisions nrf on nrf.nid = nf.nid where nf.type='finance_values' and nf.nid = ctf.nid and nrf.timestamp <=  {$endPeriod} )
             LEFT JOIN content_type_dogovor ctd ON ctd.field_dgvr_mkd_nid = nr.nid AND ctd.vid = (select MAX(nrd.vid) from node nd join node_revisions nrd on nrd.nid = nd.nid where nd.type='dogovor' and nd.nid = ctd.nid and nrd.timestamp <=  {$endPeriod})
             LEFT JOIN content_type_kladr_region rg ON rg.nid = ctp.field_tp_kladr_region_nid
             LEFT JOIN content_type_kladr_raion r ON r.nid = ctp.field_tp_kladr_raion_nid
             LEFT JOIN content_type_kladr_naspunkt np ON np.nid = ctp.field_tp_kladr_naspunkt_nid
             LEFT JOIN content_type_kladr_street s ON s.nid = ctp.field_tp_kladr_street_nid
             WHERE n.type = 'tp'
             and nr.vid = (select MAX(nr2.vid) from node_revisions nr2 where nr2.nid = nr.nid and nr2.timestamp <=  {$endPeriod})
             ORDER BY nr.nid ".($min?'ASC':'DESC')." LIMIT 1"
        );

        return $resultData;
    }

    public function selectDataFromTblsMkd_rgkh1($startPeriod, $endPeriod, $offset, $limit)
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT
             IF (n.vid = nr.vid, 1, 0) as is_current,
             IF (ctf.nid is not null, 1, 0) as has_fin_values,
             IF (ctd.nid is not null, 1, 0) as has_dgvr_values,
             (SELECT ctl.company_nid FROM company_tp_log ctl WHERE ctl.tp_nid = nr.nid and UNIX_TIMESTAMP(ctl.op_time) >= {$startPeriod} ORDER BY ctl.op_time ASC LIMIT 1) as org_id_log,
             company_nid  as org_id,
             'NEW_PERIOD' as op_time,
             nr.timestamp as rev_date,
             n.nid as house_id, nr.vid as revision_id, n.title as address_text, n.created, if(nr.uid = 0, 1, nr.uid) as uid,  ctp.*, ctf.*, ctd.*,
             rg.field_kladr_region_ais_id_value as region_ais_id, r.field_kladr_raion_ais_id_value as raion_ais_id, np.field_kladr_naspunkt_ais_id_value as naspunkt_ais_id,
             s.field_kladr_street_ais_id_value as street_ais_id
             FROM node_revisions as nr
             INNER JOIN node as n ON nr.nid = n.nid
             INNER JOIN content_type_tp ctp ON ctp.vid = nr.vid
             LEFT JOIN company_tp ct ON ct.tp_nid = nr.nid and ct.status = 1
             LEFT JOIN content_type_finance_values ctf ON ctf.field_fin_mkd_nid = nr.nid AND ctf.vid = (select MAX(nrf.vid) from node nf join node_revisions nrf on nrf.nid = nf.nid where nf.type='finance_values' and nf.nid = ctf.nid and nrf.timestamp <=  {$endPeriod} )
             LEFT JOIN content_type_dogovor ctd ON ctd.field_dgvr_mkd_nid = nr.nid AND ctd.vid = (select MAX(nrd.vid) from node nd join node_revisions nrd on nrd.nid = nd.nid where nd.type='dogovor' and nd.nid = ctd.nid and nrd.timestamp <=  {$endPeriod})
             LEFT JOIN content_type_kladr_region rg ON rg.nid = ctp.field_tp_kladr_region_nid
             LEFT JOIN content_type_kladr_raion r ON r.nid = ctp.field_tp_kladr_raion_nid
             LEFT JOIN content_type_kladr_naspunkt np ON np.nid = ctp.field_tp_kladr_naspunkt_nid
             LEFT JOIN content_type_kladr_street s ON s.nid = ctp.field_tp_kladr_street_nid
             WHERE n.type = 'tp' and nr.nid between {$offset} and {$limit}
             and nr.vid = (select MAX(nr2.vid) from node_revisions nr2 where nr2.nid = nr.nid and nr2.timestamp <=  {$endPeriod})
             group by nr.vid"
        );

        return $resultData;
    }

    public function selectCountFromTblsMkdUO_rgkh1($startPeriod, $endPeriod)
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT
             count(*) as count
             FROM company_tp_log ct
             INNER JOIN node_revisions as nr ON nr.nid = ct.tp_nid
             INNER JOIN node as n ON n.nid = nr.nid
             INNER JOIN content_type_tp ctp ON ctp.vid = nr.vid
             LEFT JOIN content_type_finance_values ctf ON ctf.field_fin_mkd_nid = nr.nid AND ctf.vid = (select MAX(nrf.vid) from node nf join node_revisions nrf on nrf.nid = nf.nid where nf.type='finance_values' and nf.nid = ctf.nid and nrf.timestamp <=  {$endPeriod} )
             LEFT JOIN content_type_dogovor ctd ON ctd.field_dgvr_mkd_nid = nr.nid AND ctd.vid = (select MAX(nrd.vid) from node nd join node_revisions nrd on nrd.nid = nd.nid where nd.type='dogovor' and nd.nid = ctd.nid and nrd.timestamp <=  {$endPeriod})
             LEFT JOIN content_type_kladr_region rg ON rg.nid = ctp.field_tp_kladr_region_nid
             LEFT JOIN content_type_kladr_raion r ON r.nid = ctp.field_tp_kladr_raion_nid
             LEFT JOIN content_type_kladr_naspunkt np ON np.nid = ctp.field_tp_kladr_naspunkt_nid
             LEFT JOIN content_type_kladr_street s ON s.nid = ctp.field_tp_kladr_street_nid
             WHERE UNIX_TIMESTAMP(ct.op_time) BETWEEN {$startPeriod} and  {$endPeriod}
             and n.type = 'tp'
             and nr.vid = (select MAX(nr2.vid) from node_revisions nr2 where nr2.nid = ct.tp_nid and nr2.timestamp <= {$endPeriod})"
        );

        return $resultData;
    }

    public function selectOffsetFromTblsMkdUO_rgkh1($startPeriod, $endPeriod, $min)
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT
             nr.nid
             FROM company_tp_log ct
             INNER JOIN node_revisions as nr ON nr.nid = ct.tp_nid
             INNER JOIN node as n ON n.nid = nr.nid
             INNER JOIN content_type_tp ctp ON ctp.vid = nr.vid
             LEFT JOIN content_type_finance_values ctf ON ctf.field_fin_mkd_nid = nr.nid AND ctf.vid = (select MAX(nrf.vid) from node nf join node_revisions nrf on nrf.nid = nf.nid where nf.type='finance_values' and nf.nid = ctf.nid and nrf.timestamp <=  {$endPeriod} )
             LEFT JOIN content_type_dogovor ctd ON ctd.field_dgvr_mkd_nid = nr.nid AND ctd.vid = (select MAX(nrd.vid) from node nd join node_revisions nrd on nrd.nid = nd.nid where nd.type='dogovor' and nd.nid = ctd.nid and nrd.timestamp <=  {$endPeriod})
             LEFT JOIN content_type_kladr_region rg ON rg.nid = ctp.field_tp_kladr_region_nid
             LEFT JOIN content_type_kladr_raion r ON r.nid = ctp.field_tp_kladr_raion_nid
             LEFT JOIN content_type_kladr_naspunkt np ON np.nid = ctp.field_tp_kladr_naspunkt_nid
             LEFT JOIN content_type_kladr_street s ON s.nid = ctp.field_tp_kladr_street_nid
             WHERE UNIX_TIMESTAMP(ct.op_time) BETWEEN {$startPeriod} and  {$endPeriod}
             and n.type = 'tp'
             and nr.vid = (select MAX(nr2.vid) from node_revisions nr2 where nr2.nid = ct.tp_nid and
							(
								nr2.timestamp <= {$endPeriod}
							)
						  )
			  GROUP BY nr.nid ORDER BY nr.nid ".($min?'ASC':'DESC')." LIMIT 1"
        );

        return $resultData;
    }

    public function selectDataFromTblsMkdUO_rgkh1($startPeriod, $endPeriod, $offset, $limit)
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT
             IF (n.vid = nr.vid, 1, 0) as is_current,
             IF (ctf.nid is not null, 1, 0) as has_fin_values,
             IF (ctd.nid is not null, 1, 0) as has_dgvr_values,
             company_nid  as org_id,
             UNIX_TIMESTAMP(ct.op_time) as rev_date,
             ct.op_type,
             ct.id as ctp_id,
             n.nid as house_id, nr.vid as revision_id, n.title as address_text, n.created, if(nr.uid = 0, 1, nr.uid) as uid,  ctp.*, ctf.*, ctd.*,
             rg.field_kladr_region_ais_id_value as region_ais_id, r.field_kladr_raion_ais_id_value as raion_ais_id, np.field_kladr_naspunkt_ais_id_value as naspunkt_ais_id,
             s.field_kladr_street_ais_id_value as street_ais_id
             FROM company_tp_log ct
             INNER JOIN node_revisions as nr ON nr.nid = ct.tp_nid
             INNER JOIN node as n ON n.nid = nr.nid
             INNER JOIN content_type_tp ctp ON ctp.vid = nr.vid
             LEFT JOIN content_type_finance_values ctf ON ctf.field_fin_mkd_nid = nr.nid AND ctf.vid = (select MAX(nrf.vid) from node nf join node_revisions nrf on nrf.nid = nf.nid where nf.type='finance_values' and nf.nid = ctf.nid and nrf.timestamp <=  {$endPeriod} )
             LEFT JOIN content_type_dogovor ctd ON ctd.field_dgvr_mkd_nid = nr.nid AND ctd.vid = (select MAX(nrd.vid) from node nd join node_revisions nrd on nrd.nid = nd.nid where nd.type='dogovor' and nd.nid = ctd.nid and nrd.timestamp <=  {$endPeriod})
             LEFT JOIN content_type_kladr_region rg ON rg.nid = ctp.field_tp_kladr_region_nid
             LEFT JOIN content_type_kladr_raion r ON r.nid = ctp.field_tp_kladr_raion_nid
             LEFT JOIN content_type_kladr_naspunkt np ON np.nid = ctp.field_tp_kladr_naspunkt_nid
             LEFT JOIN content_type_kladr_street s ON s.nid = ctp.field_tp_kladr_street_nid
             WHERE UNIX_TIMESTAMP(ct.op_time) BETWEEN {$startPeriod} and  {$endPeriod}
             and n.type = 'tp' and nr.nid between {$offset} and {$limit}
             and nr.vid = (select MAX(nr2.vid) from node_revisions nr2 where nr2.nid = ct.tp_nid and
							(
								nr2.timestamp <= {$endPeriod}
							)
						  )"
        );

        return $resultData;
    }


    public function selectCountFromTblsMkdUOold_rgkh1($endPeriod)
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT
             count(*) as count
             FROM company_tp ct
             INNER JOIN node_revisions as nr ON nr.nid = ct.tp_nid
             INNER JOIN node as n ON n.nid = nr.nid
             INNER JOIN content_type_tp ctp ON ctp.vid = nr.vid
             LEFT JOIN content_type_finance_values ctf ON ctf.field_fin_mkd_nid = nr.nid AND ctf.vid = (select MAX(nrf.vid) from node nf join node_revisions nrf on nrf.nid = nf.nid where nf.type='finance_values' and nf.nid = ctf.nid and nrf.timestamp <=  {$endPeriod} )
             LEFT JOIN content_type_dogovor ctd ON ctd.field_dgvr_mkd_nid = nr.nid AND ctd.vid = (select MAX(nrd.vid) from node nd join node_revisions nrd on nrd.nid = nd.nid where nd.type='dogovor' and nd.nid = ctd.nid and nrd.timestamp <=  {$endPeriod})
             LEFT JOIN content_type_kladr_region rg ON rg.nid = ctp.field_tp_kladr_region_nid
             LEFT JOIN content_type_kladr_raion r ON r.nid = ctp.field_tp_kladr_raion_nid
             LEFT JOIN content_type_kladr_naspunkt np ON np.nid = ctp.field_tp_kladr_naspunkt_nid
             LEFT JOIN content_type_kladr_street s ON s.nid = ctp.field_tp_kladr_street_nid
             WHERE ct.status = 0
             and n.type = 'tp'
             and nr.vid = (select MAX(nr2.vid) from node_revisions nr2 where nr2.nid = ct.tp_nid and
							(
								nr2.timestamp <= {$endPeriod}
							)
						  )"
        );

        return $resultData;
    }

    public function selectOffsetFromTblsMkdUOold_rgkh1($startPeriod, $endPeriod, $min)
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT
             nr.nid
             FROM company_tp ct
             INNER JOIN node_revisions as nr ON nr.nid = ct.tp_nid
             INNER JOIN node as n ON n.nid = nr.nid
             INNER JOIN content_type_tp ctp ON ctp.vid = nr.vid
             LEFT JOIN content_type_finance_values ctf ON ctf.field_fin_mkd_nid = nr.nid AND ctf.vid = (select MAX(nrf.vid) from node nf join node_revisions nrf on nrf.nid = nf.nid where nf.type='finance_values' and nf.nid = ctf.nid and nrf.timestamp <=  {$endPeriod} )
             LEFT JOIN content_type_dogovor ctd ON ctd.field_dgvr_mkd_nid = nr.nid AND ctd.vid = (select MAX(nrd.vid) from node nd join node_revisions nrd on nrd.nid = nd.nid where nd.type='dogovor' and nd.nid = ctd.nid and nrd.timestamp <=  {$endPeriod})
             LEFT JOIN content_type_kladr_region rg ON rg.nid = ctp.field_tp_kladr_region_nid
             LEFT JOIN content_type_kladr_raion r ON r.nid = ctp.field_tp_kladr_raion_nid
             LEFT JOIN content_type_kladr_naspunkt np ON np.nid = ctp.field_tp_kladr_naspunkt_nid
             LEFT JOIN content_type_kladr_street s ON s.nid = ctp.field_tp_kladr_street_nid
             WHERE ct.status = 0
             and n.type = 'tp'
             and nr.vid = (select MAX(nr2.vid) from node_revisions nr2 where nr2.nid = ct.tp_nid and
							(
								nr2.timestamp <= {$endPeriod}
							)
						  )
			  ORDER BY nr.nid ".($min?'ASC':'DESC')." LIMIT 1"
        );

        return $resultData;
    }

    public function selectDataFromTblsMkdUOold_rgkh1($startPeriod, $endPeriod, $offset, $limit)
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT
             0 as is_current,
             IF (ctf.nid is not null, 1, 0) as has_fin_values,
             IF (ctd.nid is not null, 1, 0) as has_dgvr_values,
             company_nid  as org_id,
             {$endPeriod} as rev_date,
             'LINK' as op_type,
             n.nid as house_id, nr.vid as revision_id, n.title as address_text, n.created, if(nr.uid = 0, 1, nr.uid) as uid,  ctp.*, ctf.*, ctd.*,
             rg.field_kladr_region_ais_id_value as region_ais_id, r.field_kladr_raion_ais_id_value as raion_ais_id, np.field_kladr_naspunkt_ais_id_value as naspunkt_ais_id,
             s.field_kladr_street_ais_id_value as street_ais_id
             FROM company_tp ct
             INNER JOIN node_revisions as nr ON nr.nid = ct.tp_nid
             INNER JOIN node as n ON n.nid = nr.nid
             INNER JOIN content_type_tp ctp ON ctp.vid = nr.vid
             LEFT JOIN content_type_finance_values ctf ON ctf.field_fin_mkd_nid = nr.nid AND ctf.vid = (select MAX(nrf.vid) from node nf join node_revisions nrf on nrf.nid = nf.nid where nf.type='finance_values' and nf.nid = ctf.nid and nrf.timestamp <=  {$endPeriod} )
             LEFT JOIN content_type_dogovor ctd ON ctd.field_dgvr_mkd_nid = nr.nid AND ctd.vid = (select MAX(nrd.vid) from node nd join node_revisions nrd on nrd.nid = nd.nid where nd.type='dogovor' and nd.nid = ctd.nid and nrd.timestamp <=  {$endPeriod})
             LEFT JOIN content_type_kladr_region rg ON rg.nid = ctp.field_tp_kladr_region_nid
             LEFT JOIN content_type_kladr_raion r ON r.nid = ctp.field_tp_kladr_raion_nid
             LEFT JOIN content_type_kladr_naspunkt np ON np.nid = ctp.field_tp_kladr_naspunkt_nid
             LEFT JOIN content_type_kladr_street s ON s.nid = ctp.field_tp_kladr_street_nid
             WHERE ct.status = 0
             and n.type = 'tp' and nr.nid between {$offset} and {$limit}
             and nr.vid = (select MAX(nr2.vid) from node_revisions nr2 where nr2.nid = ct.tp_nid and
							(
								nr2.timestamp <= {$endPeriod}
							)
						  ) group by nr.nid"
        );

        return $resultData;
    }


    public function insertDataToTblHouses_rgkh2($data, $startPeriod, $reportPeriodId=null)
    {
        if ($this->_forceMode) {
            $this->togglePgTblAllTriggers('houses', 'disable');
            $this->togglePgTblAllTriggers('addresses', 'disable');
            $this->togglePgTblAllTriggers('house_revision', 'disable');
            $this->togglePgTblAllTriggers('house_passport_builder', 'disable');
            $this->togglePgTblAllTriggers('house_supply_systems', 'disable');
            $this->togglePgTblAllTriggers('house_facade', 'disable');
            $this->togglePgTblAllTriggers('house_roof', 'disable');
            $this->togglePgTblAllTriggers('house_passport', 'disable');
            $this->togglePgTblAllTriggers('house_profile_revision', 'disable');
            $this->togglePgTblAllTriggers('house_profile_builder', 'disable');
            $this->togglePgTblAllTriggers('house_profile_supply_systems', 'disable');
            $this->togglePgTblAllTriggers('house_profile_facade', 'disable');
            $this->togglePgTblAllTriggers('house_profile_roof', 'disable');
            $this->togglePgTblAllTriggers('house_profile_passport', 'disable');
            $this->togglePgTblAllTriggers('house_profile_facade', 'disable');
            $this->togglePgTblAllTriggers('house_profile_periodic_data', 'disable');
            $this->togglePgTblAllTriggers('house_profile_accumulative_data', 'disable');
            $this->togglePgTblAllTriggers('contracts', 'disable');
            $this->togglePgTblAllTriggers('contract_periodic_data', 'disable');
            $this->togglePgTblAllTriggers('providers_rel_house_profile', 'disable');
        }

        $sql = 'INSERT INTO houses ("id", "address_id", "individual_name", "geo_tag_id", "date_created", "ais_house_id") VALUES';
        $sql_values = array();

        // техпаспорт

        $sql_addr = 'INSERT INTO addresses ("id","region_id", "area_id", "city_id", "street_id", "house_number", "block", "building", "comment", "flag_invalid", "full_address") VALUES';
        $sql_values_addr = array();

        $sql_house_revision = 'INSERT INTO house_revision ("id", "parent_id", "users_id", "system_event_id", "is_current", "create_date", "houses_id", "stage", "state", "temp_id_mkd", "extra") VALUES';
        $sql_values_house_revision = array();

        $sql_pssbuilder = 'INSERT INTO house_passport_builder ("id", "house_revision_id", "component_name") VALUES';
        $sql_values_pssbuilder = array();

        $sql_supsys = 'INSERT INTO house_supply_systems ("house_passport_builder_id", "system_name", "system_length", "system_length_no_requirements", "input_points_count", "control_nodes_count", "metering_devices_count", "last_overhaul_date", "provisioning", "elevators") VALUES';
        $sql_values_supsys = array();

        $sql_facade = 'INSERT INTO house_facade ("house_passport_builder_id", "area_total", "area_plastered", "area_unplastered", "area_panel", "area_tiled", "area_lined_siding", "area_wooden", "area_insulated_decorative_plaster", "area_insulated_tiles", "area_insulated_siding", "area_riprap"
                        , "area_glazing_common_wooden", "area_glazing_common_plastic", "area_glazing_individual_wooden", "area_glazing_individual_plastic", "area_door_fillings_metal", "area_door_fillings_others", "last_overhaul_date") VALUES';
        $sql_values_facade = array();

        $sql_roof = 'INSERT INTO house_roof ("house_passport_builder_id", "area_total", "area_pitched_slate", "area_pitched_metal", "area_pitched_others", "area_flat", "last_overhaul_date") VALUES';
        $sql_values_roof = array();

        $sql_passport = 'INSERT INTO house_passport ("house_passport_builder_id", "project_type", "location_description", "individual_name", "house_type", "exploitation_start_year", "privatization_start_date", "wall_material", "floor_type", "storeys_count", "entrance_count", "elevators_count"
                        , "area_total", "area_residential", "area_private", "area_municipal", "area_national", "area_non_residential", "area_land", "area_territory", "inventory_number", "cadastral_number", "flats_count", "residents_count", "accounts_count", "constuction_features"
                        , "thermal_actual_expense", "thermal_normative_expense", "energy_efficiency", "energy_audit_date", "deterioration_total", "deterioration_foundation", "deterioration_bearing_walls", "deterioration_floor", "emergency_date", "emergency_document_number"
                        , "emergency_reason", "last_overhaul_date", "basement", "basement_area", "basement_last_overhaul_date", "common_space_area",
                        "common_space_overhaul_date", "chute_count", "chute_last_overhaul_date") VALUES';
        $sql_values_passport = array();


        // анкеты
        $sql_pf_house_revision = 'INSERT INTO house_profile_revision ("id", "parent_id", "users_id", "system_event_id", "is_current", "create_date", "last_update", "houses_id", "reporting_period_id", "management_organizations_id",  "flag_rating", "temp_vid") VALUES';
        $sql_pf_values_house_revision = array();

        $sql_pf_pssbuilder = 'INSERT INTO house_profile_builder ("id", "house_profile_revision_id", "component_name") VALUES';
        $sql_pf_values_pssbuilder = array();

        $sql_pf_supsys = 'INSERT INTO house_profile_supply_systems ("house_profile_builder_id", "system_name", "system_length", "system_length_no_requirements", "input_points_count", "control_nodes_count", "metering_devices_count", "last_overhaul_date", "provisioning", "elevators") VALUES';
        $sql_pf_values_supsys = array();

        $sql_pf_facade = 'INSERT INTO house_profile_facade ("house_profile_builder_id", "area_total", "area_plastered", "area_unplastered", "area_panel", "area_tiled", "area_lined_siding", "area_wooden", "area_insulated_decorative_plaster", "area_insulated_tiles", "area_insulated_siding", "area_riprap"
                        , "area_glazing_common_wooden", "area_glazing_common_plastic", "area_glazing_individual_wooden", "area_glazing_individual_plastic", "area_door_fillings_metal", "area_door_fillings_others", "last_overhaul_date") VALUES';
        $sql_pf_values_facade = array();

        $sql_pf_roof = 'INSERT INTO house_profile_roof ("house_profile_builder_id", "area_total", "area_pitched_slate", "area_pitched_metal", "area_pitched_others", "area_flat", "last_overhaul_date") VALUES';
        $sql_pf_values_roof = array();

        $sql_pf_passport = 'INSERT INTO house_profile_passport ("house_profile_builder_id", "project_type", "location_description", "individual_name", "house_type", "exploitation_start_year", "privatization_start_date", "wall_material", "floor_type", "storeys_count", "entrance_count", "elevators_count"
                        , "area_total", "area_residential", "area_private", "area_municipal", "area_national", "area_non_residential", "area_land", "area_territory", "inventory_number", "cadastral_number", "flats_count", "residents_count", "accounts_count", "constuction_features"
                        , "thermal_actual_expense", "thermal_normative_expense", "energy_efficiency", "energy_audit_date", "deterioration_total", "deterioration_foundation", "deterioration_bearing_walls", "deterioration_floor", "emergency_date", "emergency_document_number"
                        , "emergency_reason", "last_overhaul_date", "basement", "basement_area", "basement_last_overhaul_date", "common_space_area",
                        "common_space_overhaul_date", "chute_count", "chute_last_overhaul_date") VALUES';
        $sql_pf_values_passport = array();

        $sql_pf_periodic = 'INSERT INTO house_profile_periodic_data ("house_profile_builder_id", "income_management", "income_management_common_property", "management_costs", "debt_owners_total", "charged_owners_total", "repair_work", "beautification_work", "payment_claims", "payment_claims_compensation", "payment_claims_refusal", "payment_claims_not_delivered",
        "raised_funds", "raised_funds_subsidies", "raised_funds_credits", "raised_funds_leasing", "raised_funds_energy_service", "raised_funds_contributions", "raised_funds_others", "income_supply", "income_supply_heating", "income_supply_electricity", "income_supply_gaz", "income_supply_hot_water", "income_supply_cold_water",
        "income_supply_wastewater", "debt_owners", "debt_owners_heating", "debt_owners_electricity", "debt_owners_gaz", "debt_owners_hot_water", "debt_owners_cold_water", "debt_owners_wastewater", "charged_owners", "charged_services_heating", "charged_services_electricity", "charged_services_gaz", "charged_services_hot_water", "charged_services_cold_water", "charged_services_wastewater", "paid_services", "paid_services_heating", "paid_services_electricity", "paid_services_gaz", "paid_services_hot_water",
        "paid_services_cold_water", "paid_resources", "paid_resources_heating", "paid_resources_electricity", "paid_resources_gaz", "paid_resources_hot_water", "paid_resources_cold_water") VALUES';
        $sql_pf_values_periodic = array();

        $sql_pf_accumulative = 'INSERT INTO house_profile_accumulative_data ("house_profile_builder_id", "contract_id") VALUES';
        $sql_pf_accumulative_values = array();

        $sql_contracts = 'INSERT INTO contracts ("id", "house_id", "management_organization_id", "type", "date_start", "plan_date_stop", "date_stop", "stop_reason", "heating_supplier_via_mo",
        "electricity_supplier_via_mo", "gaz_supplier_via_mo", "hot_water_supplier_via_mo", "cold_water_supplier_via_mo", "drainage_supplier_via_mo") VALUES';
        $sql_contracts_values = array();

        $sql_contracts_periodic = 'INSERT INTO contract_periodic_data ("contracts_id", "reporting_periods_id", "jobs", "responsibility", "notice", "terms_service_tsz_zsk", "cost_service", "resources_tsz_zsk", "is_current") VALUES';
        $sql_contracts_periodic_values = array();

        $sql_providers_house = 'INSERT INTO providers_rel_house_profile ("house_profile_revision_id", "heating_supplier_id", "electricity_supplier_id", "gaz_supplier_id", "hot_water_supplier_id", "cold_water_supplier_id", "drainage_supplier_id") VALUES';
        $sql_providers_house_values = array();



        foreach ($data as $i_row => $row) { // такая хуйня вместо group by ct.id ибо он долгий (убираем дубли связей с уо)
            if (!empty($row['ctp_id'])) {
                if (isset($this->_ctp_ids[$row['ctp_id']])) continue;
                $this->_ctp_ids[$row['ctp_id']] = $row['ctp_id'];
            }

            $row['geo_tag_id'] = !empty($row['field_tp_geo_tid_value']) ? $row['field_tp_geo_tid_value'] : "null";


            $row['address_text'] = "";
            //@TODO:
            $row['individual_name'] = $this->escape_string($row['field_tp_indiv_name_house_value']);
            switch (iconv('windows-1251', 'utf-8',strtoupper($row['field_tp_status_value']))){
                case 'АВАРИЙНЫЙ':
                    $row['state'] = 'alarm'; break;
                case 'ИСПРАВНЫЙ':
                    $row['state'] = 'normal';break;
                default:
                    $row['state'] = 'noinfo';
            }
            $row['stage'] = 'exploited';
            $this->prepareValue($row['created'], 'date');
            $this->prepareValue($row['field_tp_id_mkd_value'], 'integer');




            if (!empty($row['org_id_log'])) $row['org_id'] = $row['org_id_log'];

            $sysEventId = 1;
            switch ($row['op_type']){
                case 'NEW_PERIOD': $sysEventId = 1;break;
                case 'LINK': $sysEventId = 2;break;
                case 'UNLINK_EXPIRED': $sysEventId = 3; $row['org_id'] = null; break;
                case 'UNLINK_INCORRECT': $sysEventId = 4; $row['org_id'] = null; break;
                case 'UNLINK_RECOVERY': $sysEventId = 5; $row['org_id'] = null; break;
            }

            $this->prepareValue($row['org_id'], 'integer');

       //     $h_is = in_array($row['house_id'],$this->_houses_array);
            if ($row['is_current'] == 1 && $row['rev_date'] < $startPeriod) $row['is_current'] = '0';

            $row['update_date'] = $row['rev_date'];
            if ($sysEventId == 1) {
                $row['rev_date'] = $startPeriod;
            }
            if ($row['rev_date'] < $startPeriod) {
                $row['rev_date'] = $startPeriod;
                $row['update_date'] = $startPeriod;
            }


            if ($row['is_current'] == 1){

                // сохраняем адрес
                $dubl = false;
                $adr_value = $this->addAddresesValuesRow($row, $dubl);
                if ($dubl && in_array($row['house_id'],$this->_houses_array)) $row['is_current'] = 0;
                else if ($dubl && isset($this->_houses_array[$this->_h_adr_id])) continue;
                if (!$dubl && !is_null($adr_value)) $sql_values_addr[] = $adr_value;
            }


            $sql_pf_values_house_revision[] = $this->addProfileRevisionValues($row, $reportPeriodId, $sysEventId);
            if ($row['is_current'] == 1) $sql_values_house_revision[] = $this->addRevisionValues($row, $sysEventId);

            // сохраняем компоненты пасспорта
            $supsys_value = $this->addSupSysHeatingValuesRow($row);
            if (!is_null($supsys_value)) {
                $sql_pf_values_supsys[] = $supsys_value;
                if ($row['is_current'] == 1) $sql_values_supsys[] = $supsys_value;

                $pssbuilder_value = $this->addComponentToHouseRow('heating');
                if (!is_null($pssbuilder_value)) {
                    $sql_pf_values_pssbuilder[] = $pssbuilder_value;
                    if ($row['is_current'] == 1)  $sql_values_pssbuilder[] = $pssbuilder_value;
                }
            }

            $supsys_value = $this->addSupSysHWaterValuesRow($row);
            if (!is_null($supsys_value)) {
                $sql_pf_values_supsys[] = $supsys_value;
                if ($row['is_current'] == 1) $sql_values_supsys[] = $supsys_value;

                $pssbuilder_value = $this->addComponentToHouseRow('hwater');
                if (!is_null($pssbuilder_value)) {
                    $sql_pf_values_pssbuilder[] = $pssbuilder_value;
                    if ($row['is_current'] == 1) $sql_values_pssbuilder[] = $pssbuilder_value;
                }
            }

            $supsys_value = $this->addSupSysCWaterValuesRow($row);
            if (!is_null($supsys_value)) {
                $sql_pf_values_supsys[] = $supsys_value;
                if ($row['is_current'] == 1) $sql_values_supsys[] = $supsys_value;

                $pssbuilder_value = $this->addComponentToHouseRow('cwater');
                if (!is_null($pssbuilder_value)) {
                    $sql_pf_values_pssbuilder[] = $pssbuilder_value;
                    if ($row['is_current'] == 1) $sql_values_pssbuilder[] = $pssbuilder_value;
                }
            }

            $supsys_value = $this->addSupSysSewerageValuesRow($row);
            if (!is_null($supsys_value)) {
                $sql_pf_values_supsys[] = $supsys_value;
                if ($row['is_current'] == 1) $sql_values_supsys[] = $supsys_value;

                $pssbuilder_value = $this->addComponentToHouseRow('sewerage');
                if (!is_null($pssbuilder_value)) {
                    $sql_pf_values_pssbuilder[] = $pssbuilder_value;
                    if ($row['is_current'] == 1) $sql_values_pssbuilder[] = $pssbuilder_value;
                }
            }

            $supsys_value = $this->addSupSysElectroValuesRow($row);
            if (!is_null($supsys_value)) {
                $sql_pf_values_supsys[] = $supsys_value;
                if ($row['is_current'] == 1) $sql_values_supsys[] = $supsys_value;

                $pssbuilder_value = $this->addComponentToHouseRow('electro');
                if (!is_null($pssbuilder_value)) {
                    $sql_pf_values_pssbuilder[] = $pssbuilder_value;
                    if ($row['is_current'] == 1) $sql_values_pssbuilder[] = $pssbuilder_value;
                }
            }

            $supsys_value = $this->addSupSysGasValuesRow($row);
            if (!is_null($supsys_value)) {
                $sql_pf_values_supsys[] = $supsys_value;
                if ($row['is_current'] == 1) $sql_values_supsys[] = $supsys_value;

                $pssbuilder_value = $this->addComponentToHouseRow('gas');
                if (!is_null($pssbuilder_value)) {
                    $sql_pf_values_pssbuilder[] = $pssbuilder_value;
                    if ($row['is_current'] == 1) $sql_values_pssbuilder[] = $pssbuilder_value;
                }
            }

            $facade_value = $this->addFacadeValuesRow($row);
            if (!is_null($facade_value)) {
                $sql_pf_values_facade[] = $facade_value;
                if ($row['is_current'] == 1) $sql_values_facade[] = $facade_value;

                $pssbuilder_value = $this->addComponentToHouseRow('facade');
                if (!is_null($pssbuilder_value)) {
                    $sql_pf_values_pssbuilder[] = $pssbuilder_value;
                    if ($row['is_current'] == 1) $sql_values_pssbuilder[] = $pssbuilder_value;
                }
            }

            $roof_value = $this->addRoofValuesRow($row);
            if (!is_null($roof_value)) {
                $sql_pf_values_roof[] = $roof_value;
                if ($row['is_current'] == 1)  $sql_values_roof[] = $roof_value;

                $pssbuilder_value = $this->addComponentToHouseRow('roof');
                if (!is_null($pssbuilder_value)) {
                    $sql_pf_values_pssbuilder[] = $pssbuilder_value;
                    if ($row['is_current'] == 1) $sql_values_pssbuilder[] = $pssbuilder_value;
                }
            }

            $passport_value = $this->addPassportValuesRow($row);
            if (!is_null($passport_value)) {
                $sql_pf_values_passport[] = $passport_value;
                if ($row['is_current'] == 1) $sql_values_passport[] = $passport_value;

                $pssbuilder_value = $this->addComponentToHouseRow('main');
                if (!is_null($pssbuilder_value)) {
                    $sql_pf_values_pssbuilder[] = $pssbuilder_value;
                    if ($row['is_current'] == 1) $sql_values_pssbuilder[] = $pssbuilder_value;
                }
            }

            if ($row['has_fin_values'] == 1) {
                $periodic_value = $this->addPeriodicValuesRow($row);
                if (!is_null($periodic_value)) {
                    $sql_pf_values_periodic[] = $periodic_value;

                    $pssbuilder_value = $this->addComponentToHouseRow('periodic');
                    if (!is_null($pssbuilder_value)) {
                        $sql_pf_values_pssbuilder[] = $pssbuilder_value;
                    }
                }
            }
                // создаем пустые договора
            if (/*$row['has_dgvr_values'] == 1 && */$sysEventId < 3) {
                    // добавляем контракт для дома с уо, если его еще небыло
                    if (empty($this->_company_contracts[$row['house_id']]) || $sysEventId == 2) {
                        $contracts_value = $this->addContractsValuesRow($row,null,$sysEventId);
                        if (!is_null($contracts_value)) {
                            $sql_contracts_values[$this->_contracts_id] = $contracts_value;
                        }
                        $contract_id = $this->_contracts_id;
                    } else {
                        $contract_id = $this->_company_contracts[$row['house_id']];
                        $contracts_value = $this->addContractsValuesRow($row, $contract_id);
                        if (!is_null($contracts_value)) {
                            $this->executeQueryWithLog($this->getPgsqlEntityManager(), "DELETE FROM contracts WHERE \"id\" = $contract_id");
                            $sql_contracts_values[$contract_id] = $contracts_value;
                        }
                    }

                    // добавляем периодические данные котракта данного отчетного периода
                    $sql_contracts_periodic_values[] = $this->addContractsPeriodicValuesRow($contract_id, $reportPeriodId, $row);


                    $sql_pf_accumulative_values[] = $this->addAccumulativeValuesRow($contract_id);

                    $pssbuilder_value = $this->addComponentToHouseRow('accumulative');
                    if (!is_null($pssbuilder_value)) {
                        $sql_pf_values_pssbuilder[] = $pssbuilder_value;
                    }

                    $sql_providers_house_values[] = $this->addProvidersHouseValuesRow($row);
            }

            if ($row['is_current'] == 1) {
                if ($row['geo_tag_id'] == 2276348) $row['geo_tag_id'] = 2276347;
                $sql_values[] = "(".$row['house_id'].",".$this->_h_adr_id.",'".$row['individual_name']."',".$row['geo_tag_id'].",".$row['created'].", ".$row['field_tp_id_mkd_value'].")";
            }

        }
        unset($data);
        $STEP = 5000;
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_addr, $sql_values_addr, $STEP, false); unset($sql_values_addr);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql, $sql_values, $STEP); unset($sql_values);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_house_revision, $sql_values_house_revision, $STEP, false); unset($sql_values_house_revision);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_pssbuilder, $sql_values_pssbuilder, $STEP); unset($sql_values_pssbuilder);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_supsys, $sql_values_supsys, $STEP); unset($sql_values_supsys);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_facade, $sql_values_facade, $STEP); unset($sql_values_facade);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_roof, $sql_values_roof, $STEP); unset($sql_values_roof);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_passport, $sql_values_passport, $STEP); unset($sql_values_passport);

        $result = $this->insertByParts($this->getPgSqlEntityManager(), $sql_pf_house_revision, $sql_pf_values_house_revision, $STEP, false); unset($sql_pf_values_house_revision);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_pf_pssbuilder, $sql_pf_values_pssbuilder, $STEP); unset($sql_pf_values_pssbuilder);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_pf_supsys, $sql_pf_values_supsys, $STEP); unset($sql_pf_values_supsys);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_pf_facade, $sql_pf_values_facade, $STEP); unset($sql_pf_values_facade);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_pf_roof, $sql_pf_values_roof, $STEP); unset($sql_pf_values_roof);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_pf_passport, $sql_pf_values_passport, $STEP); unset($sql_pf_values_passport);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_pf_periodic, $sql_pf_values_periodic, $STEP); unset($sql_pf_values_periodic);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_contracts, $sql_contracts_values, $STEP); unset($sql_contracts_values);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_contracts_periodic, $sql_contracts_periodic_values, $STEP); unset($sql_contracts_periodic_values);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_providers_house, $sql_providers_house_values, $STEP); unset($sql_providers_house_values);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_pf_accumulative, $sql_pf_accumulative_values, $STEP); unset($sql_pf_accumulative_values);

        if ($this->_forceMode) {
            $this->togglePgTblAllTriggers('houses', 'enable');
            $this->togglePgTblAllTriggers('addresses', 'enable');
            $this->togglePgTblAllTriggers('house_revision', 'enable');
            $this->togglePgTblAllTriggers('house_passport_builder', 'enable');
            $this->togglePgTblAllTriggers('house_supply_systems', 'enable');
            $this->togglePgTblAllTriggers('house_facade', 'enable');
            $this->togglePgTblAllTriggers('house_roof', 'enable');
            $this->togglePgTblAllTriggers('house_passport', 'enable');
            $this->togglePgTblAllTriggers('house_profile_revision', 'enable');
            $this->togglePgTblAllTriggers('house_profile_builder', 'enable');
            $this->togglePgTblAllTriggers('house_profile_supply_systems', 'enable');
            $this->togglePgTblAllTriggers('house_profile_facade', 'enable');
            $this->togglePgTblAllTriggers('house_profile_roof', 'enable');
            $this->togglePgTblAllTriggers('house_profile_passport', 'enable');
            $this->togglePgTblAllTriggers('house_profile_facade', 'enable');
            $this->togglePgTblAllTriggers('house_profile_periodic_data', 'enable');
            $this->togglePgTblAllTriggers('house_profile_accumulative_data', 'enable');
            $this->togglePgTblAllTriggers('contracts', 'enable');
            $this->togglePgTblAllTriggers('contract_periodic_data', 'enable');
            $this->togglePgTblAllTriggers('providers_rel_house_profile', 'enable');
        }

        return $result;
    }

    private function addAddresesValuesRow($row, &$dubl)
    {
        $values = null;
        if (empty($row['region_ais_id']) && empty($row['raion_ais_id']) && empty($row['naspunkt_ais_id']) && empty($row['street_ais_id']) && empty($row['field_tp_number_value']))
        {
            $this->_h_adr_id = "null";
        }else {
            $addressRow = $this->compareAisWithFias($row['region_ais_id'], $row['raion_ais_id'], $row['naspunkt_ais_id'], $row['street_ais_id'],
                $row['field_tp_number_value'], $row['field_tp_korpus_value'], $row['field_tp_part_value'], '', '');

            $row['address_text'] = $this->getAddressManager()->generateSerializedAddress($addressRow);

            if (isset($this->_addressesArray[$row['address_text']])){
                $this->_h_adr_id = $this->_addressesArray[$row['address_text']];
                $this->_count_idents++;
                $dubl = true;
            } else {
                $this->_h_adr_id = ++$this->_adr_id;
                $this->prepareValue($addressRow['region_guid']);
                $this->prepareValue($addressRow['area_guid']);
                $this->prepareValue($addressRow['city_guid']);
                $this->prepareValue($addressRow['street_guid']);
                $this->prepareValue($addressRow['house_number']);
                $this->prepareValue($addressRow['housing']);
                $this->prepareValue($addressRow['building']);
                $this->prepareValue($addressRow['comment']);
                $this->prepareValue($addressRow['flag_invalid']);
                $address_v = $row['address_text'];
                $this->prepareValue($address_v);
                $values = "(".$this->_h_adr_id.", ".$addressRow['region_guid'].",".$addressRow['area_guid'].",".$addressRow['city_guid'].",".$addressRow['street_guid'].",".$addressRow['house_number']."
                        ,".$addressRow['housing'].",".$addressRow['building'].",".$addressRow['comment'].",".$addressRow['flag_invalid'].", ".$address_v." )";

                $this->_addressesArray[$row['address_text']] = $this->_h_adr_id;

                $dubl = false;
                $this->_houses_array[$this->_h_adr_id] = $row['house_id'];
            }

            unset($addressRow);
        }

        return $values;
    }

    private function addProfileRevisionValues($row, $reportPeriodId, $sysEventId)
    {
        $this->_house_revision_id++;
        $this->prepareValue($row['uid'], 'integer');
        $this->prepareValue($row['is_current'], 'integer');
        $this->prepareValue($row['rev_date'], 'date');
        $this->prepareValue($row['update_date'], 'date');
        $this->prepareValue($row['house_id'], 'integer');

        $current = 0;
     //   if ($reportPeriodId == 8 && $row['is_current'] == '1') $current = 1;

        $values = "(".$this->_house_revision_id.", null, ".$row['uid'].", ".$sysEventId.", ".$current.", ".$row['rev_date'].", ".$row['update_date'].", ".$row['house_id'].", ".$reportPeriodId.", ".$row['org_id'].", false, ".$row['revision_id']." )";
        return $values;
    }

    private function addRevisionValues($row, $sysEventId)
    {
        $this->prepareValue($row['uid'], 'integer');
        $this->prepareValue($row['is_current'], 'integer');
        $this->prepareValue($row['rev_date'], 'date');
        $this->prepareValue($row['house_id'], 'integer');
        $this->prepareValue($row['state'], 'string');
        $this->prepareValue($row['stage'], 'string');
        $row['revision_extra'] = null;
        if ($row['state']=='alarm') {
            $row['revision_extra'] = '{"date":"'.date('Y-m-d',$row['field_tp_dangerous_date_value']).'","number":"'.$row['field_tp_dangerous_doc_value'].'","reason":"'.$row['field_tp_dangerous_osnovanie_value'].'"';
        }
        $this->prepareValue($row['revision_extra'], 'string');

        $current = 1;

        $values = "(".$this->_house_revision_id.", null, ".$row['uid'].", ".$sysEventId.", ".$current.", ".$row['rev_date'].", ".$row['house_id'].", ".$row['stage'].", ".$row['state'].", ".$row['field_tp_id_mkd_value'].", ".$row['revision_extra']."  )";
        return $values;
    }

    private function addComponentToHouseRow($component_name)
    {
        $values = "(".$this->_house_pss_builder_id.", ".$this->_house_revision_id.", '".$component_name."')";
        return $values;
    }

    private function addSupSysHeatingValuesRow($row)
    {
        $flag_null = true; $values = null;
        if (is_null($row['field_tp_heating_system_value'])) $row['field_tp_heating_system_value'] = "null"; else { $flag_null = false; $row['field_tp_heating_system_value'] = "'{$this->escape_string($row['field_tp_heating_system_value'])}'";}
        if (is_null($row['field_tp_pipe_length_value'])) $row['field_tp_pipe_length_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_inputs_count_value'])) $row['field_tp_inputs_count_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_control_units_count_value'])) $row['field_tp_control_units_count_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_meters_count_value'])) $row['field_tp_meters_count_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_heating_system_last_rep_value'])) $row['field_tp_heating_system_last_rep_value'] = "null"; else { $flag_null = false; $row['field_tp_heating_system_last_rep_value'] = "'{$this->escape_string($row['field_tp_heating_system_last_rep_value'])}'";}
        if (is_null($row['field_tp_resources_type_value'])) $row['field_tp_resources_type_value'] = "null"; else { $flag_null = false; $row['field_tp_resources_type_value'] = "'{$this->escape_string($row['field_tp_resources_type_value'])}'";}
        if (is_null($row['field_tp_elevators_value'])) $row['field_tp_elevators_value'] = "null"; else $flag_null = false;

        if (!$flag_null)
        {
            $this->_house_pss_builder_id++;
            $values = "(".$this->_house_pss_builder_id.", ".$row['field_tp_heating_system_value'].", ".$row['field_tp_pipe_length_value'].", null, ".$row['field_tp_inputs_count_value'].", ".$row['field_tp_control_units_count_value']."
                        , ".$row['field_tp_meters_count_value'].", ".$row['field_tp_heating_system_last_rep_value'].", ".$row['field_tp_resources_type_value'].", ".$row['field_tp_elevators_value'].")";
        }
        return $values;
    }

    private function addSupSysHWaterValuesRow($row)
    {
        $flag_null = true; $values = null;
        if (is_null($row['field_tp_water_system_value'])) $row['field_tp_water_system_value'] = "null"; else { $flag_null = false; $row['field_tp_water_system_value'] = "'{$this->escape_string($row['field_tp_water_system_value'])}'";}
        if (is_null($row['field_tp_water_system_length_value'])) $row['field_tp_water_system_length_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_water_inputs_count_value'])) $row['field_tp_water_inputs_count_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_water_control_units_cnt_value'])) $row['field_tp_water_control_units_cnt_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_water_meters_count_value'])) $row['field_tp_water_meters_count_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_water_system_last_rep_value'])) $row['field_tp_water_system_last_rep_value'] = "null"; else { $flag_null = false; $row['field_tp_water_system_last_rep_value'] = "'{$this->escape_string($row['field_tp_water_system_last_rep_value'])}'";}
        if (is_null($row['field_tp_water_resourses_type_value'])) $row['field_tp_water_resourses_type_value'] = "null"; else { $flag_null = false; $row['field_tp_water_resourses_type_value'] = "'{$this->escape_string($row['field_tp_water_resourses_type_value'])}'";}

        if (!$flag_null)
        {
            $this->_house_pss_builder_id++;
            $values = "(".$this->_house_pss_builder_id.", ".$row['field_tp_water_system_value'].", ".$row['field_tp_water_system_length_value'].", null, ".$row['field_tp_water_inputs_count_value'].", ".$row['field_tp_water_control_units_cnt_value']."
                        , ".$row['field_tp_water_meters_count_value'].", ".$row['field_tp_water_system_last_rep_value'].", ".$row['field_tp_water_resourses_type_value'].", null)";
        }

        return $values;
    }

    private function addSupSysCWaterValuesRow($row)
    {
        $flag_null = true; $values = null;
        if (is_null($row['field_tp_cold_water_value'])) $row['field_tp_cold_water_value'] = "null"; else { $flag_null = false; $row['field_tp_cold_water_value'] = "'{$this->escape_string($row['field_tp_cold_water_value'])}'";}
        if (is_null($row['field_tp_cold_water_system_len_value'])) $row['field_tp_cold_water_system_len_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_cold_water_inputs_cnt_value'])) $row['field_tp_cold_water_inputs_cnt_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_cold_water_meters_cnt_value'])) $row['field_tp_cold_water_meters_cnt_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_cold_water_last_rep_value'])) $row['field_tp_cold_water_last_rep_value'] = "null"; else { $flag_null = false; $row['field_tp_cold_water_last_rep_value'] = "'{$this->escape_string($row['field_tp_cold_water_last_rep_value'])}'";}
        if (is_null($row['field_tp_cold_water_res_type_value'])) $row['field_tp_cold_water_res_type_value'] = "null"; else { $flag_null = false; $row['field_tp_cold_water_res_type_value'] = "'{$this->escape_string($row['field_tp_cold_water_res_type_value'])}'";}

        if (!$flag_null)
        {
            $this->_house_pss_builder_id++;
            $values = "(".$this->_house_pss_builder_id.", ".$row['field_tp_cold_water_value'].", ".$row['field_tp_cold_water_system_len_value'].", null, ".$row['field_tp_cold_water_inputs_cnt_value'].", null
                        , ".$row['field_tp_cold_water_meters_cnt_value'].", ".$row['field_tp_cold_water_last_rep_value'].", ".$row['field_tp_cold_water_res_type_value'].", null)";
        }

        return $values;
    }

    private function addSupSysSewerageValuesRow($row)
    {
        $flag_null = true; $values = null;
        if (is_null($row['field_tp_cold_water_value'])) $row['field_tp_sewerage_system_value'] = "null"; else { $flag_null = false; $row['field_tp_sewerage_system_value'] = "'{$this->escape_string($row['field_tp_sewerage_system_value'])}'";}
        if (is_null($row['field_tp_sewerage_system_length_value'])) $row['field_tp_sewerage_system_length_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_sewerage_sys_last_rep_value'])) $row['field_tp_sewerage_sys_last_rep_value'] = "null"; else { $flag_null = false; $row['field_tp_sewerage_sys_last_rep_value'] = "'{$this->escape_string($row['field_tp_sewerage_sys_last_rep_value'])}'";}

        if (!$flag_null)
        {
            $this->_house_pss_builder_id++;
            $values = "(".$this->_house_pss_builder_id.", ".$row['field_tp_sewerage_system_value'].", ".$row['field_tp_sewerage_system_length_value'].", null, null, null
                        , null, ".$row['field_tp_sewerage_sys_last_rep_value'].", null, null)";
        }

        return $values;
    }

    private function addSupSysElectroValuesRow($row)
    {
        $flag_null = true; $values = null;
        if (is_null($row['field_tp_electrical_power_system_value'])) $row['field_tp_electrical_power_system_value'] = "null"; else { $flag_null = false; $row['field_tp_electrical_power_system_value'] = "'{$this->escape_string($row['field_tp_electrical_power_system_value'])}'";}
        if (is_null($row['field_tp_networks_length_value'])) $row['field_tp_networks_length_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_electric_sys_inputs_cnt_value'])) $row['field_tp_electric_sys_inputs_cnt_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_electric_sys_meters_cnt_value'])) $row['field_tp_electric_sys_meters_cnt_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_electrical_sys_last_rep_value'])) $row['field_tp_electrical_sys_last_rep_value'] = "null"; else { $flag_null = false; $row['field_tp_electrical_sys_last_rep_value'] = "'{$this->escape_string($row['field_tp_electrical_sys_last_rep_value'])}'";}
        if (is_null($row['field_tp_electric_sys_res_type_value'])) $row['field_tp_electric_sys_res_type_value'] = "null"; else { $flag_null = false; $row['field_tp_electric_sys_res_type_value'] = "'{$this->escape_string($row['field_tp_electric_sys_res_type_value'])}'";}

        if (!$flag_null)
        {
            $this->_house_pss_builder_id++;
            $values = "(".$this->_house_pss_builder_id.", ".$row['field_tp_electrical_power_system_value'].", ".$row['field_tp_networks_length_value'].", null, ".$row['field_tp_electric_sys_inputs_cnt_value'].", null
                        , ".$row['field_tp_electric_sys_meters_cnt_value'].", ".$row['field_tp_electrical_sys_last_rep_value'].", ".$row['field_tp_electric_sys_res_type_value'].", null)";
        }
        return $values;
    }

    private function addSupSysGasValuesRow($row)
    {
        $flag_null = true; $values = null;
        if (is_null($row['field_tp_gas_system_value'])) $row['field_tp_gas_system_value'] = "null"; else { $row['field_tp_gas_system_value'] = "'{$this->escape_string($row['field_tp_gas_system_value'])}'"; $flag_null = false;}
        if (is_null($row['field_tp_gas_system_length_value'])) $row['field_tp_gas_system_length_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_gas_system_not_rel_leng_value'])) $row['field_tp_gas_system_not_rel_leng_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_gas_inputs_cnt_value'])) $row['field_tp_gas_inputs_cnt_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_gas_meters_cnt_value'])) $row['field_tp_gas_meters_cnt_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_gas_sys_last_rep_value'])) $row['field_tp_gas_sys_last_rep_value'] = "null"; else { $row['field_tp_gas_sys_last_rep_value'] = "'{$this->escape_string($row['field_tp_gas_sys_last_rep_value'])}'"; $flag_null = false;}
        if (is_null($row['field_tp_gas_sys_res_type_value'])) $row['field_tp_gas_sys_res_type_value'] = "null"; else { $row['field_tp_gas_sys_res_type_value'] = "'{$this->escape_string($row['field_tp_gas_sys_res_type_value'])}'"; $flag_null = false;}

        if (!$flag_null)
        {
            $this->_house_pss_builder_id++;
            $values = "(".$this->_house_pss_builder_id.", ".$row['field_tp_gas_system_value'].", ".$row['field_tp_gas_system_length_value'].", ".$row['field_tp_gas_system_not_rel_leng_value'].", ".$row['field_tp_gas_inputs_cnt_value'].", null
                        , ".$row['field_tp_gas_meters_cnt_value'].", ".$row['field_tp_gas_sys_last_rep_value'].", ".$row['field_tp_gas_sys_res_type_value'].", null)";
        }
        return $values;
    }

    private function addFacadeValuesRow($row)
    {
        $flag_null = true; $values = null;
        if (is_null($row['field_tp_fasad_square_all_value'])) $row['field_tp_fasad_square_all_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_fasad_square_oshtucatur_value'])) $row['field_tp_fasad_square_oshtucatur_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_fasad_square_not_oshtuc_value'])) $row['field_tp_fasad_square_not_oshtuc_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_fasad_square_panel_value'])) $row['field_tp_fasad_square_panel_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_fasad_square_plit_value'])) $row['field_tp_fasad_square_plit_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_fasad_square_saiding_value'])) $row['field_tp_fasad_square_saiding_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_fasad_square_tree_value'])) $row['field_tp_fasad_square_tree_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_fasad_square_warm_shtuc_value'])) $row['field_tp_fasad_square_warm_shtuc_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_fasad_square_warm_plit_value'])) $row['field_tp_fasad_square_warm_plit_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_fasad_square_warm_said_value'])) $row['field_tp_fasad_square_warm_said_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_fasad_square_otmost_value'])) $row['field_tp_fasad_square_otmost_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_fasad_square_glaz_tree_value'])) $row['field_tp_fasad_square_glaz_tree_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_fasad_square_glaz_plast_value'])) $row['field_tp_fasad_square_glaz_plast_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_fasad_square_indiv_tree_value'])) $row['field_tp_fasad_square_indiv_tree_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_fasad_square_indiv_plas_value'])) $row['field_tp_fasad_square_indiv_plas_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_fasad_square_metal_value'])) $row['field_tp_fasad_square_metal_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_fasad_square_other_value'])) $row['field_tp_fasad_square_other_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_last_rep_value'])) $row['field_tp_last_rep_value'] = "null"; else { $this->prepareValue($row['field_tp_last_rep_value']); $flag_null = false;}

        if (!$flag_null)
        {
            $this->_house_pss_builder_id++;
            $values = "(".$this->_house_pss_builder_id.", ".$row['field_tp_fasad_square_all_value'].", ".$row['field_tp_fasad_square_oshtucatur_value'].", ".$row['field_tp_fasad_square_not_oshtuc_value'].", ".$row['field_tp_fasad_square_panel_value'].",
            ".$row['field_tp_fasad_square_plit_value'].", ".$row['field_tp_fasad_square_saiding_value'].", ".$row['field_tp_fasad_square_tree_value'].", ".$row['field_tp_fasad_square_warm_shtuc_value'].", ".$row['field_tp_fasad_square_warm_plit_value'].",
            ".$row['field_tp_fasad_square_warm_said_value'].", ".$row['field_tp_fasad_square_otmost_value'].", ".$row['field_tp_fasad_square_glaz_tree_value'].", ".$row['field_tp_fasad_square_glaz_plast_value'].", ".$row['field_tp_fasad_square_indiv_tree_value'].",
            ".$row['field_tp_fasad_square_indiv_plas_value'].",".$row['field_tp_fasad_square_metal_value'].",".$row['field_tp_fasad_square_other_value'].",".$row['field_tp_last_rep_value'].")";
        }

        return $values;
    }

    private function addPassportValuesRow($row)
    {
        $row['field_tp_series_type_project_value'] = is_null($row['field_tp_series_type_project_value']) ? "null": "'{$this->escape_string($row['field_tp_series_type_project_value'])}'";
        $row['field_tp_description_value'] = is_null($row['field_tp_description_value']) ? "null": "'{$this->escape_string($row['field_tp_description_value'])}'";
        $row['field_tp_indiv_name_house_value'] = is_null($row['field_tp_indiv_name_house_value']) ? "null": "'{$this->escape_string($row['field_tp_indiv_name_house_value'])}'";
        $row['field_tp_house_type_value'] = is_null($row['field_tp_house_type_value']) ? "null": "'{$this->escape_string($row['field_tp_house_type_value'])}'";
        $row['field_tp_exploitation_date_value'] = is_null($row['field_tp_exploitation_date_value']) ? "null": "'{$this->escape_string($row['field_tp_exploitation_date_value'])}'";
        $row['field_tp_priv_start_date_value'] = strtotime($row['field_tp_priv_start_date_value']);
        $this->prepareValue($row['field_tp_priv_start_date_value'],'date');
        $row['field_tp_wall_material_value'] = is_null($row['field_tp_wall_material_value']) ? "null": "'{$this->escape_string($row['field_tp_wall_material_value'])}'";
        $row['field_tp_floor_type_value'] = is_null($row['field_tp_floor_type_value']) ? "null": "'{$this->escape_string($row['field_tp_floor_type_value'])}'";
        if (is_null($row['field_tp_etajnost_value'])) $row['field_tp_etajnost_value'] = "null";
        if (is_null($row['field_tp_entrance_count_value'])) $row['field_tp_entrance_count_value'] = "null";
        if (is_null($row['field_tp_elevators_count_value'])) $row['field_tp_elevators_count_value'] = "null";
        if (is_null($row['field_tp_square_all_all_value'])) $row['field_tp_square_all_all_value'] = "null";
        if (is_null($row['field_tp_square_all_value'])) $row['field_tp_square_all_value'] = "null";
        if (is_null($row['field_tp_square_private_value'])) $row['field_tp_square_private_value'] = "null";
        if (is_null($row['field_tp_square_municipal_value'])) $row['field_tp_square_municipal_value'] = "null";
        if (is_null($row['field_tp_square_gos_value'])) $row['field_tp_square_gos_value'] = "null";
        if (is_null($row['field_tp_non_residential_square_value'])) $row['field_tp_non_residential_square_value'] = "null";
        if (is_null($row['field_tp_squre_uchastok_value'])) $row['field_tp_squre_uchastok_value'] = "null";
        if (is_null($row['field_tp_square_near_mkd_value'])) $row['field_tp_square_near_mkd_value'] = "null";
        $row['field_tp_inv_number_value'] = is_null($row['field_tp_inv_number_value']) ? "null": "'{$this->escape_string($row['field_tp_inv_number_value'])}'";
        $row['field_tp_kad_number_value'] = is_null($row['field_tp_kad_number_value']) ? "null": "'{$this->escape_string($row['field_tp_kad_number_value'])}'";
        if (is_null($row['field_tp_flats_count_value'])) $row['field_tp_flats_count_value'] = "null";
        if (is_null($row['field_tp_residents_count_value'])) $row['field_tp_residents_count_value'] = "null";
        if (is_null($row['field_tp_accounts_count_value'])) $row['field_tp_accounts_count_value'] = "null";
        $row['field_tp_constuction_feature_value'] = is_null($row['field_tp_constuction_feature_value']) ? "null": "'{$this->escape_string($row['field_tp_constuction_feature_value'])}'";
        if (is_null($row['field_tp_fact_rashod_value'])) $row['field_tp_fact_rashod_value'] = "null";
        if (is_null($row['field_tp_norm_rashod_value'])) $row['field_tp_norm_rashod_value'] = "null";
        $row['field_tp_class_effect_value'] = is_null($row['field_tp_class_effect_value']) ? "null": "'{$this->escape_string($row['field_tp_class_effect_value'])}'";
        if (is_numeric($row['field_tp_audit_date_value'])) $row['field_tp_audit_date_value'] = date('d.m.Y', $row['field_tp_audit_date_value']);
        $row['field_tp_audit_date_value'] = is_null($row['field_tp_audit_date_value']) ? "null": "'{$this->escape_string($row['field_tp_audit_date_value'])}'";
        if (is_null($row['field_tp_iznos_all_value'])) $row['field_tp_iznos_all_value'] = "null";
        if (is_null($row['field_tp_iznos_fundament_value'])) $row['field_tp_iznos_fundament_value'] = "null";
        if (is_null($row['field_tp_iznos_steny_value'])) $row['field_tp_iznos_steny_value'] = "null";
        if (is_null($row['field_tp_iznos_floor_value'])) $row['field_tp_iznos_floor_value'] = "null";
        $row['field_tp_dangerous_date_value'] = is_null($row['field_tp_dangerous_date_value']) ? "null": "'{$this->escape_string($row['field_tp_dangerous_date_value'])}'";
        $row['field_tp_dangerous_doc_value'] = is_null($row['field_tp_dangerous_doc_value']) ? "null": "'{$this->escape_string(str_replace('?','',$row['field_tp_dangerous_doc_value']))}'";
        $row['field_tp_dangerous_osnovanie_value'] = is_null($row['field_tp_dangerous_osnovanie_value']) ? "null": "'{$this->escape_string($row['field_tp_dangerous_osnovanie_value'])}'";
        $row['field_tp_last_rep_value'] = is_null($row['field_tp_last_rep_value']) ? "null": "'{$this->escape_string($row['field_tp_last_rep_value'])}'";
        $row['field_tp_last_rep_roof_value'] = is_null($row['field_tp_last_rep_roof_value']) ? "null": "'{$this->escape_string($row['field_tp_last_rep_roof_value'])}'";
        $row['field_tp_basement_value'] = is_null($row['field_tp_basement_value']) ? "null": "'{$this->escape_string($row['field_tp_basement_value'])}'";
        if (is_null($row['field_tp_square_basement_value'])) $row['field_tp_square_basement_value'] = "null";
        $row['field_tp_last_per_basement_value'] = is_null($row['field_tp_last_per_basement_value']) ? "null": "'{$this->escape_string($row['field_tp_last_per_basement_value'])}'";
        if (is_null($row['field_tp_common_areas_square_value'])) $row['field_tp_common_areas_square_value'] = "null";
        $row['field_tp_common_areas_last_rep_value'] = is_null($row['field_tp_common_areas_last_rep_value']) ? "null": "'{$this->escape_string($row['field_tp_common_areas_last_rep_value'])}'";
        if (is_null($row['field_tp_chute_count_value'])) $row['field_tp_chute_count_value'] = "null";
        $row['field_tp_chute_last_rep_value'] = is_null($row['field_tp_chute_last_rep_value']) ? "null": "'{$this->escape_string($row['field_tp_chute_last_rep_value'])}'";

        $this->_house_pss_builder_id++;
        $values = "(".$this->_house_pss_builder_id.", ".$row['field_tp_series_type_project_value'].", ".$row['field_tp_description_value'].", ".$row['field_tp_indiv_name_house_value'].", ".$row['field_tp_house_type_value'].",
        ".$row['field_tp_exploitation_date_value'].", ".$row['field_tp_priv_start_date_value'].", ".$row['field_tp_wall_material_value'].", ".$row['field_tp_floor_type_value'].", ".$row['field_tp_etajnost_value'].",
        ".$row['field_tp_entrance_count_value'].", ".$row['field_tp_elevators_count_value'].", ".$row['field_tp_square_all_all_value'].", ".$row['field_tp_square_all_value'].", ".$row['field_tp_square_private_value'].",
        ".$row['field_tp_square_municipal_value'].",".$row['field_tp_square_gos_value'].",".$row['field_tp_non_residential_square_value'].",".$row['field_tp_squre_uchastok_value'].",".$row['field_tp_square_near_mkd_value'].",
        ".$row['field_tp_inv_number_value'].",".$row['field_tp_kad_number_value'].",".$row['field_tp_flats_count_value'].",".$row['field_tp_residents_count_value'].",".$row['field_tp_accounts_count_value'].",".$row['field_tp_constuction_feature_value'].",
        ".$row['field_tp_fact_rashod_value'].",".$row['field_tp_norm_rashod_value'].",".$row['field_tp_class_effect_value'].",".$row['field_tp_audit_date_value'].",".$row['field_tp_iznos_all_value'].",
        ".$row['field_tp_iznos_fundament_value'].",".$row['field_tp_iznos_steny_value'].",".$row['field_tp_iznos_floor_value'].",".$row['field_tp_dangerous_date_value'].",".$row['field_tp_dangerous_doc_value'].",
        ".$row['field_tp_dangerous_osnovanie_value'].",".$row['field_tp_last_rep_value'].",
        ".$row['field_tp_basement_value'].",".$row['field_tp_square_basement_value'].",".$row['field_tp_last_per_basement_value'].",
        ".$row['field_tp_common_areas_square_value'].",".$row['field_tp_common_areas_last_rep_value'].",".$row['field_tp_chute_count_value'].",".$row['field_tp_chute_last_rep_value'].")";

        return $values;
    }

    private function addRoofValuesRow($row)
    {
        $flag_null = true;
        $values = null;
        if (is_null($row['field_tp_roof_square_all_value'])) $row['field_tp_roof_square_all_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_roof_square_slate_value'])) $row['field_tp_roof_square_slate_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_roof_square_metal_value'])) $row['field_tp_roof_square_metal_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_roof_square_other_value'])) $row['field_tp_roof_square_other_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_roof_square_flat_value'])) $row['field_tp_roof_square_flat_value'] = "null"; else $flag_null = false;
        if (is_null($row['field_tp_last_rep_roof_value'])) $row['field_tp_last_rep_roof_value'] = "null"; else $flag_null = false;

        if (!$flag_null)
        {
            $this->_house_pss_builder_id++;
            $values = "(".$this->_house_pss_builder_id.", ".$row['field_tp_roof_square_all_value'].", ".$row['field_tp_roof_square_slate_value'].", ".$row['field_tp_roof_square_metal_value'].", ".$row['field_tp_roof_square_other_value'].", ".$row['field_tp_roof_square_flat_value'].", '".$row['field_tp_last_rep_roof_value']."')";
        }

        return $values;
    }

    private function addPeriodicValuesRow($row) {
        $this->_house_pss_builder_id++;
        $valuesRow[] = $this->_house_pss_builder_id;
        $valuesRow[] = $this->prepareValue($row['field_fin_dohod_upr_bez_ku_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_dohod_upr_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_rash_upr_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_zadolg_za_upr_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_vzisk_za_upr_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_obem_remont_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_obem_blag_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_viplat_isk_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_viplat_isk_kompens_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_viplat_isk_snizhen_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_viplat_isk_snizhen_res_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_obem_sredstv_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_obem_sredstv_subsid_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_obem_sredstv_kredit_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_obem_sredstv_lizing_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_obem_sredstv_energo_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_obem_sredstv_celev_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_obem_sredstv_inie_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_dohod_ku_za_period_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_dohod_otop_za_period_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_dohod_elect_za_period_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_dohod_gaz_za_period_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_dohod_gor_za_period_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_dohod_hol_za_period_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_dohod_vodot_za_period_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_zadolg_za_ku_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_zadolg_za_ku_otop_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_zadolg_za_ku_electr_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_zadolg_za_ku_gaz_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_zadolg_za_ku_gor_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_zadolg_za_ku_hol_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_zadolg_za_ku_vodotvd_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_vzisk_za_ku_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_vzisk_za_ku_otop_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_vzisk_za_ku_electr_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_vzisk_za_ku_gaz_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_vzisk_za_ku_gor_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_vzisk_za_ku_hol_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_vzisk_za_ku_vodotvd_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_oplach_ku_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_oplach_ku_otop_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_oplach_ku_electr_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_oplach_ku_gaz_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_oplach_ku_gor_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_oplach_ku_hol_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_oplach_resurs_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_oplach_resurs_otop_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_oplach_resurs_electr_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_oplach_resurs_gaz_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_oplach_resurs_gor_value']);
        $valuesRow[] = $this->prepareValue($row['field_fin_oplach_resurs_hol_value']);

        return "(".implode(',',$valuesRow).")";
    }

    private function addAccumulativeValuesRow($contract_id)
    {
        $this->_house_pss_builder_id++;
        return "(".$this->_house_pss_builder_id.", ".$contract_id.")";
    }

    private function addContractsValuesRow($row, $contractId = null, $sysEventId=null)
    {
        if (!$contractId) {
            $this->_contracts_id++;
            $valuesRow[] = $this->_contracts_id;
            $this->_company_contracts[$row['house_id']] =  $this->_contracts_id;
        } else {
            $valuesRow[] = $contractId;
        }
        $valuesRow[] = $this->prepareValue($row['house_id'], 'integer');
        $valuesRow[] = $row['org_id'];
        $valuesRow[] = $this->prepareValue($row['field_dgvr_type_value'], 'string');
        if ($sysEventId==2) $row['field_dgvr_date_start_value'] = $row['rev_date'];
        $valuesRow[] = $this->prepareValue($row['field_dgvr_date_start_value'], 'date');
        $valuesRow[] = $this->prepareValue($row['field_dgvr_plan_date_stop_value'], 'date');
        $valuesRow[] = $this->prepareValue($row['field_dgvr_date_stop_value'], 'date');
        $valuesRow[] = $this->prepareValue($row['field_dgvr_stop_reason_value'], 'string');

        if (!is_null($row['field_dgvr_post_otop_nid'])) $row['field_dgvr_post_otop_uo_flag_value'] = 0;
        if (!is_null($row['field_dgvr_post_elect_nid'])) $row['field_dgvr_post_elect_uo_flag_value'] = 0;
        if (!is_null($row['field_dgvr_post_gaz_nid'])) $row['field_dgvr_post_gaz_uo_flag_value'] = 0;
        if (!is_null($row['field_dgvr_post_gor_vod_nid'])) $row['field_dgvr_post_gor_vod_uo_flag_value'] = 0;
        if (!is_null($row['field_dgvr_post_hol_vod_nid'])) $row['field_dgvr_post_hol_vod_uo_flag_value'] = 0;
        if (!is_null($row['field_dgvr_post_vodotvd_nid'])) $row['field_dgvr_post_vodotvd_uo_flag_value'] = 0;

        $valuesRow[] = $this->prepareValue($row['field_dgvr_post_otop_uo_flag_value'], 'integer');
        $valuesRow[] = $this->prepareValue($row['field_dgvr_post_elect_uo_flag_value'], 'integer');
        $valuesRow[] = $this->prepareValue($row['field_dgvr_post_gaz_uo_flag_value'], 'integer');
        $valuesRow[] = $this->prepareValue($row['field_dgvr_post_gor_vod_uo_flag_value'], 'integer');
        $valuesRow[] = $this->prepareValue($row['field_dgvr_post_hol_vod_uo_flag_value'], 'integer');
        $valuesRow[] = $this->prepareValue($row['field_dgvr_post_vodotvd_uo_flag_value'], 'integer');



        return "(".implode(',',$valuesRow).")";
    }

    private function addContractsPeriodicValuesRow($contract_id, $reportPeriodId, $row)
    {
        $valuesRow[] = $contract_id;
        $valuesRow[] = $reportPeriodId;
        $valuesRow[] = $this->prepareValue($row['field_dgvr_jobs_value'], 'string');
        $valuesRow[] = $this->prepareValue($row['field_dgvr_obyaz_value'], 'string');
        $valuesRow[] = $this->prepareValue($row['field_dgvr_notice_value'], 'string');
        $valuesRow[] = $this->prepareValue($row['field_dgvr_services_terms_value'], 'string');
        $valuesRow[] = $this->prepareValue($row['field_dgvr_prices_value'], 'string');
        $valuesRow[] = $this->prepareValue($row['field_dgvr_res_tsg_value'], 'string');
        $valuesRow[] = $reportPeriodId==8?'1':'0';  // @TODO

        return "(".implode(',',$valuesRow).")";
    }

    private function addProvidersHouseValuesRow($row)
    {
        $valuesRow[] = $this->_house_revision_id;
        $valuesRow[] = $this->prepareValue($row['field_dgvr_post_otop_nid'], 'integer');
        $valuesRow[] = $this->prepareValue($row['field_dgvr_post_elect_nid'], 'integer');
        $valuesRow[] = $this->prepareValue($row['field_dgvr_post_gaz_nid'], 'integer');
        $valuesRow[] = $this->prepareValue($row['field_dgvr_post_gor_vod_nid'], 'integer');
        $valuesRow[] = $this->prepareValue($row['field_dgvr_post_hol_vod_nid'], 'integer');
        $valuesRow[] = $this->prepareValue($row['field_dgvr_post_vodotvd_nid'], 'integer');

        return "(".implode(',',$valuesRow).")";
    }

    /**
     * ЛИФТЫ
     */

    public function clearDataFromTblLifts_rgkh2(){
        $this->togglePgTblAllTriggers('house_lifts', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM house_lifts"
        );
        $this->togglePgTblAllTriggers('house_lifts', 'enable');
        $this->togglePgTblAllTriggers('house_profile_lifts', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM house_profile_lifts"
        );
        $this->togglePgTblAllTriggers('house_profile_lifts', 'enable');

        $this->togglePgTblAllTriggers('house_profile_builder', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "delete from house_profile_builder where component_name = 'elevators';"
        );
        $this->togglePgTblAllTriggers('house_profile_builder', 'enable');

        $this->togglePgTblAllTriggers('house_passport_builder', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "delete from house_passport_builder where component_name = 'elevators';"
        );
        $this->togglePgTblAllTriggers('house_passport_builder', 'enable');
    }

    public function selectHousesVids_rgkh2($period){
        $res = $this->executeQueryWithLog($this->getPgSqlEntityManager(), "select id,\"temp_vid\" from house_profile_revision where \"reporting_period_id\" = $period");
        foreach($res as $r){
            $this->_houses_vids[$r['temp_vid']][] = $r['id'];
        }
    }

    public function selectCountTblLifts_rgkh1($startPeriod, $endPeriod)
    {
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "SELECT max(\"id\") as last_id
             FROM house_profile_builder"
        );

        $this->_house_pss_builder_id = $resultData[0]['last_id'];

        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT count(*) as cnt FROM content_type_elevators e
                inner join content_type_tp tp on tp.nid = e.nid
                inner join node n on n.nid = tp.nid and n.vid = tp.vid
                inner join content_field_tp_lifts tplf on tplf.nid = e.nid and tplf.field_tp_lifts_item_id = e.vid
                inner join node_revisions nr on nr.nid = e.nid and nr.vid = tplf.vid
                where nr.vid = (select MAX(nr2.vid) from node_revisions nr2 where nr2.nid = e.nid and nr2.timestamp <= {$endPeriod} )"
        );

        return $resultData[0]['cnt'];
    }

    public function selectDataFromTblLifts_rgkh1($startPeriod, $endPeriod, $offset, $limit )
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT nr.vid as nvid, nr.nid as nnid, e.*, if (n.vid = nr.vid, 1, 0) AS is_current FROM content_type_elevators e
                inner join content_type_tp tp on tp.nid = e.nid
                inner join node n on n.nid = tp.nid and n.vid = tp.vid
                inner join content_field_tp_lifts tplf on tplf.nid = e.nid and tplf.field_tp_lifts_item_id = e.vid
                inner join node_revisions nr on nr.nid = e.nid and nr.vid = tplf.vid
                where nr.vid = (select MAX(nr2.vid) from node_revisions nr2 where nr2.nid = nr.nid and nr2.timestamp <= {$endPeriod} )
             ORDER BY e.nid ASC LIMIT {$offset}, {$limit}"
        );

        return $resultData;
    }

    public function insertDataToTblLifts_rgkh2($data)
    {
        if ($this->_forceMode) {
            $this->togglePgTblAllTriggers('house_lifts', 'disable');
            $this->togglePgTblAllTriggers('house_profile_lifts', 'disable');
            $this->togglePgTblAllTriggers('house_passport_builder', 'disable');
            $this->togglePgTblAllTriggers('house_profile_builder', 'disable');
        }

        $sql = 'INSERT INTO house_lifts ("house_passport_builder_id", "number", "stops_count", "capacity", "plan_period", "date_exploitation", "date_last_repair", "manufacturer", "factory_number", "date_created") VALUES';
        $sql_values = array();

        $sql_pf = 'INSERT INTO house_profile_lifts ("house_profile_builder_id", "number", "stops_count", "capacity", "plan_period", "date_exploitation", "date_last_repair", "manufacturer", "factory_number", "date_created") VALUES';
        $sql_pf_values = array();

        $sql_pssbuilder = 'INSERT INTO house_passport_builder ("id", "house_revision_id", "component_name") VALUES';
        $sql_values_pssbuilder = array();

        $sql_pf_pssbuilder = 'INSERT INTO house_profile_builder ("id", "house_profile_revision_id", "component_name") VALUES';
        $sql_pf_values_pssbuilder = array();

        foreach ($data as $lifts_row) {

            if (!is_null($lifts_row['field_e_exploitation_date_value']) && strlen($lifts_row['field_e_exploitation_date_value']) == 4 && $lifts_row['field_e_exploitation_date_value'] != '0000' && is_numeric($lifts_row['field_e_exploitation_date_value'])) $lifts_row['field_e_exploitation_date_value'] = $lifts_row['field_e_exploitation_date_value'].'-01-01'; else $lifts_row['field_e_exploitation_date_value'] = null;
            if (!is_null($lifts_row['field_e_last_rep_date_value']) && strlen($lifts_row['field_e_last_rep_date_value']) == 4 && $lifts_row['field_e_last_rep_date_value'] != '0000' && is_numeric($lifts_row['field_e_last_rep_date_value'])) $lifts_row['field_e_last_rep_date_value'] = $lifts_row['field_e_last_rep_date_value'].'-01-01'; else $lifts_row['field_e_last_rep_date_value'] = null;
        //    if (is_null($lifts_row['field_e_num_value'])) $lifts_row['field_e_num_value'] = 1;
            $this->prepareValue($lifts_row['field_tp_lifts_item_id_value'], 'integer');
            $this->prepareValue($lifts_row['field_e_num_value'], 'string');
            $this->prepareValue($lifts_row['field_e_stops_count_value'], 'integer');
            $this->prepareValue($lifts_row['field_e_capacity_value'], 'integer');
            $this->prepareValue($lifts_row['field_e_plan_period_value'], 'string');
            $this->prepareValue($lifts_row['field_e_exploitation_date_value'], 'string');
            $this->prepareValue($lifts_row['field_e_last_rep_date_value'], 'string');
            $this->prepareValue($lifts_row['field_e_manufacturer_value'], 'string');
            $this->prepareValue($lifts_row['field_e_porch_num_value'], 'string');



            $hr_ids = $this->_houses_vids[$lifts_row['nvid']];

            if (!empty($hr_ids)) {

                foreach ($hr_ids as $hr_id) {

                    $this->_house_pss_builder_id++;

                    $sql_lift_values = "(".$this->_house_pss_builder_id.", ".$lifts_row['field_e_porch_num_value'].", ".$lifts_row['field_e_stops_count_value'].", ".$lifts_row['field_e_capacity_value'].",
                    ".$lifts_row['field_e_plan_period_value'].", ".$lifts_row['field_e_exploitation_date_value'].", ".$lifts_row['field_e_last_rep_date_value'].",
                    ".$lifts_row['field_e_manufacturer_value'].", ".$lifts_row['field_e_num_value'].", now() )";

                    $sql_pf_values[] = $sql_lift_values;
                    if ($lifts_row['is_current'] == 1) $sql_values[] = $sql_lift_values;


                    $this->_house_revision_id = $hr_id;
                    $pssbuilder_value = $this->addComponentToHouseRow('elevators');
                    if (!is_null($pssbuilder_value)) {
                        $sql_pf_values_pssbuilder[] = $pssbuilder_value;
                        if ($lifts_row['is_current'] == 1) $sql_values_pssbuilder[] = $pssbuilder_value;
                    }
                }

            }
            unset($lifts_row);
        }

        $STEP = count($data);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_pssbuilder, $sql_values_pssbuilder, $STEP); unset($sql_values_pssbuilder);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_pf_pssbuilder, $sql_pf_values_pssbuilder, $STEP); unset($sql_pf_values_pssbuilder);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql, $sql_values, $STEP);unset($sql_values);
        $result = $this->insertByParts($this->getPgSqlEntityManager(), $sql_pf, $sql_pf_values, $STEP);unset($sql_pf_values);


        if ($this->_forceMode) {
            $this->togglePgTblAllTriggers('house_lifts', 'enable');
            $this->togglePgTblAllTriggers('house_profile_lifts', 'enable');
            $this->togglePgTblAllTriggers('house_passport_builder', 'enable');
            $this->togglePgTblAllTriggers('house_profile_builder', 'enable');
        }

        return $result;
    }

    // ----------------------------------------------------------------ФОНД-----------------------------------------------

    public function clearDataFromTblsFond_rgkh2(){
        $this->togglePgTblAllTriggers('crashed_mkd', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM crashed_mkd"
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),"ALTER TABLE crashed_mkd DROP COLUMN \"import_data_id\"");
        $this->togglePgTblAllTriggers('crashed_mkd', 'enable');

        $this->togglePgTblAllTriggers('stat_overhaul', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM stat_overhaul"
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),"ALTER TABLE stat_overhaul DROP COLUMN \"import_data_id\"");
        $this->togglePgTblAllTriggers('stat_overhaul', 'enable');

        $this->togglePgTblAllTriggers('stat_migration', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM stat_migration"
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),"ALTER TABLE stat_migration DROP COLUMN \"import_data_id\"");
        $this->togglePgTblAllTriggers('stat_migration', 'enable');

        $this->togglePgTblAllTriggers('crashed_mkd_ext', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM crashed_mkd_ext"
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),"ALTER TABLE crashed_mkd_ext DROP COLUMN \"import_data_id\"");
        $this->togglePgTblAllTriggers('crashed_mkd_ext', 'enable');

        $this->togglePgTblAllTriggers('stat_overhaul_ext', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM stat_overhaul_ext"
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),"ALTER TABLE stat_overhaul_ext DROP COLUMN \"import_data_id\"");
        $this->togglePgTblAllTriggers('stat_overhaul_ext', 'enable');

        $this->togglePgTblAllTriggers('stat_migration_ext', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM stat_migration_ext"
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),"ALTER TABLE stat_migration_ext DROP COLUMN \"import_data_id\"");
        $this->togglePgTblAllTriggers('stat_migration_ext', 'enable');

        $this->togglePgTblAllTriggers('import_data_from_ais', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM import_data_from_ais"
        );
        $this->togglePgTblAllTriggers('import_data_from_ais', 'enable');

    }

    public function selectCountTblFond_rgkh1($tableName)
    {
        $resultData = $this->executeQueryWithLog($this->getMysqlEntityManagerSecond(),
            "SELECT COUNT(*) as cnt FROM $tableName"
        );

        return $resultData[0]['cnt'];
    }

    public function selectDataFromTblCrashedMkd_rgkh1($offset, $limit, $ext=false)
    {
        $resultData = $this->executeQueryWithLog($this->getMysqlEntityManagerSecond(),
            "SELECT *
             FROM crashed_mkd".($ext ? '_ext ':' ')."
             ORDER BY id_mkd ASC LIMIT {$offset}, {$limit}"
        );

        return $resultData;
    }

    public function selectDataFromTblStatOverhaul_rgkh1($offset, $limit, $ext=false)
    {
        $resultData = $this->executeQueryWithLog($this->getMysqlEntityManagerSecond(),
            "SELECT *
             FROM stat_overhaul".($ext ? '_ext ':' ')."
             ORDER BY id_mkd ASC LIMIT {$offset}, {$limit}"
        );

        return $resultData;
    }

    public function selectDataFromTblStatMigration_rgkh1($offset, $limit, $ext=false)
    {
        $resultData = $this->executeQueryWithLog($this->getMysqlEntityManagerSecond(),
            "SELECT *
             FROM stat_migration".($ext ? '_ext ':' ')."
             ORDER BY id_mkd ASC LIMIT {$offset}, {$limit}"
        );

        return $resultData;
    }

    public function insertDataToTblFond_rgkh2($data, $tableName,  &$id)
    {
        if ($this->_forceMode)
            $this->togglePgTblAllTriggers($tableName, 'disable');

        $sql = 'INSERT INTO '.$tableName.' VALUES';
        $sql_values = array();

        foreach ($data as $row) {
            if ($tableName!='crashed_mkd') {
                array_unshift ($row, $id);
                $id++;
            }
            foreach($row as $field => &$value) {
                if ($field == 'num_doc') $value = str_replace(chr(0),' ',$value);
                $this->prepareValue($value);
            }

            $sql_value = '('.implode(',',$row).')';

            array_push($sql_values, $sql_value);
        }

        $result = $this->insertByParts($this->getPgSqlEntityManager(), $sql, $sql_values, count($data));

        if ($this->_forceMode)
            $this->togglePgTblAllTriggers($tableName, 'enable');

        return $result;
    }

    public function updateHousesStateAndStage_rgkh2()
    {
        if ($this->_forceMode) {
            $this->togglePgTblAllTriggers('house_revision', 'disable');
        }

        echo "Updating ALARM houses...\n";
        $affected = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "UPDATE house_revision SET state = 'alarm', extra = '{\"date\":\"'||cr.avar_date||'\",\"number\":\"'||cr.avar_no||'\",\"reason\":\"'||cr.avar_why||'\",\"after\":\"'||cr.dal_ispolz||'\"}' FROM crashed_mkd cr WHERE cr.id_mkd = temp_id_mkd and cr.id_mkd is not NULL"
            , true
        );
        echo "Finish update ALARM houses - $affected count\n";

        echo "Updating DECOMMISSIONED houses...\n";
        $affected = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "UPDATE house_revision SET \"stage\" = 'decommissioned' WHERE \"temp_id_mkd\" in (SELECT \"id_mkd\" FROM stat_migration where \"f_date_end_migr\" is not NULL and \"id_mkd\" is not NULL) and \"temp_id_mkd\" not in (SELECT \"id_mkd\" FROM crashed_mkd WHERE \"snos_fact_date\" is not null and \"id_mkd\" is not NULL )"
            , true
        );
        echo "Finish update DECOMMISSIONED houses - $affected count\n";

        echo "Updating DRIFTING houses...\n";
        $affected = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "UPDATE house_revision SET \"stage\" = 'drifting', \"state\" = null WHERE \"temp_id_mkd\" in (SELECT \"id_mkd\" FROM crashed_mkd AS c WHERE \"snos_fact_date\" is not null and \"id_mkd\" is not NULL)"
            , true
        );
        echo "Finish update DRIFTING houses - $affected count\n";
/*
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "ALTER TABLE house_revision DROP COLUMN \"temp_id_mkd\""
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "ALTER TABLE house_revision DROP COLUMN \"temp_vid\""
        );

        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "ALTER TABLE house_profile_revision DROP COLUMN \"temp_vid\""
        );
*/
        if ($this->_forceMode) {
            $this->togglePgTblAllTriggers('house_revision', 'enable');
        }
    }

    public function updateImportTable($table_name)
    {
        if ($this->_forceMode) {
            $this->togglePgTblAllTriggers($table_name, 'disable');
            $this->togglePgTblAllTriggers('import_data_from_ais', 'disable');
        }

        echo "Updating import_data_from_ais in $table_name\n";
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),"ALTER TABLE $table_name ADD COLUMN \"import_data_id\" bigint");

        $res = $this->executeQueryWithLog($this->getPgSqlEntityManager(),"SELECT max(id) as last_id FROM import_data_from_ais");
        $last_id =  $res[0]['last_id'];
        $import_data_id = $last_id+1;
        $values = array();
        $values[] = $import_data_id;
        $filename = 'import_'.$table_name.'.csv';
        $values[] = $this->prepareValue($filename,'string');
        $type = $table_name.'_houses';
        $values[] = $this->prepareValue($type,'string');
        $date = date('Y-m-d H:i:s',time());
        $values[] = $this->prepareValue($date,'string');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),"INSERT INTO import_data_from_ais (\"id\",\"file_name\",\"type\",\"start_date\") VALUES(".implode(',',$values).")");
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),"UPDATE $table_name SET \"import_data_id\" = $import_data_id");

        if ($this->_forceMode) {
            $this->togglePgTblAllTriggers($table_name, 'enable');
            $this->togglePgTblAllTriggers('import_data_from_ais', 'enable');
        }
    }

    public function dublicates()
    {
        echo "update titles\n";
        $this->executeQueryWithLog($this->getMysqlEntityManager(),
            "UPDATE content_type_tp tp JOIN node n on n.nid = tp.nid  SET tp.field_tp_number_value = n.title WHERE tp.field_tp_kladr_region_nid is null and tp.field_tp_kladr_raion_nid is null and tp.field_tp_kladr_naspunkt_nid is null and tp.field_tp_kladr_street_nid is null and n.title != '  д.'",true);

        $dublAddresses = $this->executeQueryWithLog($this->getMysqlEntityManager(),
            "SELECT tp.field_tp_kladr_region_nid as region, tp.field_tp_kladr_raion_nid as raion, tp.field_tp_kladr_naspunkt_nid as naspunkt, tp.field_tp_kladr_street_nid as street, tp.field_tp_number_value as num, tp.field_tp_korpus_value as korp, tp.field_tp_part_value as part, count(*) FROM rgkh_drupal.content_type_tp tp
                inner join rgkh_drupal.node n on n.nid = tp.nid and n.vid = tp.vid
                group by tp.field_tp_kladr_region_nid, tp.field_tp_kladr_raion_nid, tp.field_tp_kladr_naspunkt_nid, tp.field_tp_kladr_street_nid, tp.field_tp_number_value, tp.field_tp_korpus_value, tp.field_tp_part_value having count(*) > 1 order by 8 desc");
        $affected = 0;
        foreach ($dublAddresses as $a => $addr){
            if (empty($addr['region']) && empty($addr['raion']) && empty($addr['naspunkt']) && empty($addr['street']) && empty($addr['num']) && empty($addr['korp']) && empty($addr['part'])) {
                continue;
            }

            $this->prepareValue($addr['region'],'integer');
            $this->prepareValue($addr['raion'],'integer');
            $this->prepareValue($addr['naspunkt'],'integer');
            $this->prepareValue($addr['street'],'integer');
            $this->prepareValue($addr['num'],'string');
            $this->prepareValue($addr['korp'],'string');
            $this->prepareValue($addr['part'],'string');

            if ($addr['region'] == 'null') $addr['region'] = 'is null'; else $addr['region'] = " = {$addr['region']}";
            if ($addr['raion'] == 'null') $addr['raion'] = 'is null'; else $addr['raion'] = " = {$addr['raion']}";
            if ($addr['naspunkt'] == 'null') $addr['naspunkt'] = 'is null'; else $addr['naspunkt'] = " = {$addr['naspunkt']}";
            if ($addr['street'] == 'null') $addr['street'] = 'is null'; else $addr['street'] = " = {$addr['street']}";
            if ($addr['num'] == 'null') $addr['num'] = 'is null'; else $addr['num'] = " = {$addr['num']}";
            if ($addr['korp'] == 'null') $addr['korp'] = 'is null'; else $addr['korp'] = " = {$addr['korp']}";
            if ($addr['part'] == 'null') $addr['part'] = 'is null'; else $addr['part'] = " = {$addr['part']}";

            $addr['num'] = str_replace("\\","\\\\",$addr['num']);
            $addr['korp'] = str_replace("\\","\\\\",$addr['korp']);
            $addr['part'] = str_replace("\\","\\\\",$addr['part']);

       //     echo "\n".$addr['region']." ".$addr['raion']." ".$addr['naspunkt']." ".$addr['street']." ".$addr['num']." ".$addr['korp']." ".$addr['part']."\n";
            $houses = $this->executeQueryWithLog($this->getMysqlEntityManager(),
                "SELECT n.nid, tp.field_tp_number_value FROM content_type_tp tp join node n on n.nid = tp.nid and n.vid = tp.vid left join company_tp ctp on ctp.tp_nid = tp.nid and ctp.status = 1
                where tp.field_tp_kladr_region_nid {$addr['region']} and tp.field_tp_kladr_raion_nid {$addr['raion']} and tp.field_tp_kladr_naspunkt_nid {$addr['naspunkt']} and tp.field_tp_kladr_street_nid {$addr['street']} and tp.field_tp_number_value {$addr['num']} and tp.field_tp_korpus_value {$addr['korp']} and tp.field_tp_part_value {$addr['part']} ORDER BY ctp.status DESC, ctp.tp_nid ASC", false, false);
     //       echo "\rselected a $a (".count($houses).")\n";

            foreach($houses as $i => $house){
                    if ($i>0) {
                        $value = "CONCAT(field_tp_number_value,' (дубль $i)')";
                        if (empty($house['field_tp_number_value'])) $value = "'(дубль $i)'";
                        $affected_ = $this->executeQueryWithLog($this->getMysqlEntityManager(),
                            iconv("utf-8","windows-1251","UPDATE content_type_tp SET field_tp_number_value = {$value} WHERE nid = {$house['nid']}"), true, false);
                        $affected += $affected_;
                    }
            }

        }
        echo "updated $affected dublicates\n";
    }

}
