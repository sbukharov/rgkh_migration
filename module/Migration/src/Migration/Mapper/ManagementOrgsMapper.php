<?php

namespace Migration\Mapper;

use Migration\Utils\SqlLogger;

class ManagementOrgsMapper extends OrgPeriodicData
{
    protected $_fiasArray = array();
    protected $_addressesArray = array();
    protected $_legal_adr_id = "null";
    protected $_actual_adr_id = "null";
    protected $_post_adr_id = "null";
    protected $_count_idents = 0;
    protected $_adr_id = 0;
    protected $_org_requis_id = 0;
    protected $_org_employee_id = 0;
    protected $_company_array = array();
    protected $_profile_static_id = 0;
    protected $_profile_periodic_id = 0;
    protected $_update_mo = array();
    protected $_update_prov = array();

    public function getCountIdents()
    {
        return $this->_count_idents;
    }

    public function selectFiasData_rgkh2()
    {
        $fiasData = $this->executeQueryWithLog($this->getPgSqlEntityManager(), "SELECT \"aoguid\", \"shortname\", \"formalname\", \"aisid\" FROM fias_addrobj WHERE actstatus = '1'");
        foreach ($fiasData as $i=> &$fiasRow) {
            $this->_fiasArray[$fiasRow['aisid']] = array('aoguid' => $fiasRow['aoguid'], 'shortname' => $fiasRow['shortname'], 'formalname' => $fiasRow['formalname']);
            unset($fiasData[$i]);
            unset($fiasRow);
        }
        unset($fiasData);
    }

    public function unsetFiasData()
    {
        unset($this->_fiasArray);
        unset($this->_addressesArray);
    }

    public function clearDataFromTblAddresses_rgkh2()
    {
        $this->togglePgTblAllTriggers('addresses', 'disable');

        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM addresses"
        );

        $this->togglePgTblAllTriggers('addresses', 'enable');

        return $resultData;
    }

    /** ---------------------------------- Management Organizations ------------------------------------------- */
    public function clearDataFromTblManagementOrgs_rgkh2()
    {
        $this->togglePgTblAllTriggers('management_organizations', 'disable');
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM management_organizations"
        );
        $this->togglePgTblAllTriggers('management_organizations', 'enable');

        $this->togglePgTblAllTriggers('providers', 'disable');
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM providers"
        );
        $this->togglePgTblAllTriggers('providers', 'enable');

        $this->togglePgTblAllTriggers('user_registration_requests', 'disable');
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM user_registration_requests"
        );
        $this->togglePgTblAllTriggers('user_registration_requests', 'enable');

        $this->togglePgTblAllTriggers('company_employee', 'disable');
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM company_employee"
        );
        $this->togglePgTblAllTriggers('company_employee', 'enable');

        $this->togglePgTblAllTriggers('company_requisites', 'disable');
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM company_requisites"
        );
        $this->togglePgTblAllTriggers('company_requisites', 'enable');

        $this->togglePgTblAllTriggers('company_profiles', 'disable');
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM company_profiles"
        );
        $this->togglePgTblAllTriggers('company_profiles', 'enable');


        $this->togglePgTblAllTriggers('company_profile_static_data', 'disable');
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM company_profile_static_data"
        );
        $this->togglePgTblAllTriggers('company_profile_static_data', 'enable');

        $this->togglePgTblAllTriggers('company_profile_periodic_data', 'disable');
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM company_profile_periodic_data"
        );
        $this->togglePgTblAllTriggers('company_profile_periodic_data', 'enable');

        $this->togglePgTblAllTriggers('company_profile_criteria_values', 'disable');
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM company_profile_criteria_values"
        );
        $this->togglePgTblAllTriggers('company_profile_criteria_values', 'enable');

        $this->togglePgTblAllTriggers('company_profile_criteria_items', 'disable');
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM company_profile_criteria_items"
        );
        $this->togglePgTblAllTriggers('company_profile_criteria_items', 'enable');



        $this->togglePgTblAllTriggers('company_profile_items', 'disable');
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM company_profile_items"
        );
        $this->togglePgTblAllTriggers('company_profile_items', 'enable');

        $this->togglePgTblAllTriggers('reporting_periods', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM reporting_periods"
        );
        $this->togglePgTblAllTriggers('reporting_periods', 'enable');

        return $resultData;
    }

    public function selectCountFromTblNode_rgkh1($period)
    {
        if ($period['status'] == '0') {
            $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
                "SELECT count(*) as cnt
                FROM uo_anketa_archive a JOIN node n ON n.nid = a.field_org_company_nid JOIN node_revisions nr ON nr.vid = n.vid JOIN content_type_company ctc ON ctc.vid = n.vid
                WHERE n.type = 'company' and rating_uo_id = ".$period['id']
            );
        } else {
            $startPeriod = $period['date_start'];
            $endPeriod = $period['date_end'];
            $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
                "SELECT count(*) as cnt
                FROM node_revisions as nr INNER JOIN node as n ON n.nid = nr.nid
                LEFT JOIN content_type_company ctc ON ctc.nid = n.nid and ctc.vid = n.vid
                LEFT JOIN content_type_organization o ON o.field_org_company_nid = n.nid AND o.vid = (SELECT max(nr2.vid) FROM node_revisions nr2 WHERE nr2.nid = o.nid AND nr2.timestamp BETWEEN {$startPeriod} AND {$endPeriod})
                WHERE n.type = 'company'"
            );
        }

        return $resultData[0]['cnt'];
    }

    public function selectProviderUO(){
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),"select distinct d.field_dgvr_post_otop_nid from content_type_dogovor d join content_type_company c on c.nid = d.field_dgvr_post_otop_nid  where c.field_company_type_value = 1");
        foreach($resultData as $i=>$r) {
            $this->_providers_uo[] = (int)$r['field_dgvr_post_otop_nid'];
            unset($resultData[$i]);
        }
        unset($resultData);

        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),"select distinct d.field_dgvr_post_elect_nid from content_type_dogovor d join content_type_company c on c.nid = d.field_dgvr_post_elect_nid  where c.field_company_type_value = 1");
        foreach($resultData as $i=>$r) {
            $this->_providers_uo[] = (int)$r['field_dgvr_post_elect_nid'];
            unset($resultData[$i]);
        }
        unset($resultData);

        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),"select distinct d.field_dgvr_post_gaz_nid from content_type_dogovor d join content_type_company c on c.nid = d.field_dgvr_post_gaz_nid  where c.field_company_type_value = 1");
        foreach($resultData as $i=>$r) {
            $this->_providers_uo[] = (int)$r['field_dgvr_post_gaz_nid'];
            unset($resultData[$i]);
        }
        unset($resultData);

        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),"select distinct d.field_dgvr_post_gor_vod_nid from content_type_dogovor d join content_type_company c on c.nid = d.field_dgvr_post_gor_vod_nid  where c.field_company_type_value = 1");
        foreach($resultData as $i=>$r) {
            $this->_providers_uo[] = (int)$r['field_dgvr_post_gor_vod_nid'];
            unset($resultData[$i]);
        }
        unset($resultData);

        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),"select distinct d.field_dgvr_post_hol_vod_nid from content_type_dogovor d join content_type_company c on c.nid = d.field_dgvr_post_hol_vod_nid  where c.field_company_type_value = 1");
        foreach($resultData as $i=>$r) {
            $this->_providers_uo[] = (int)$r['field_dgvr_post_hol_vod_nid'];
            unset($resultData[$i]);
        }
        unset($resultData);

        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),"select distinct d.field_dgvr_post_vodotvd_nid from content_type_dogovor d join content_type_company c on c.nid = d.field_dgvr_post_vodotvd_nid  where c.field_company_type_value = 1");
        foreach($resultData as $i=>$r) {
            $this->_providers_uo[] = (int)$r['field_dgvr_post_vodotvd_nid'];
            unset($resultData[$i]);
        }
        unset($resultData);
    }

    public function selectDataFromTblNode_rgkh1($period, $offset, $limit)
    {
        if ($period['status'] == '0') {
            $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
                "SELECT
                 0 as is_current,
                 n.nid as company_id, n.status, n.uid as user_id, nr.log as log, n.created,
                 (SELECT count(*) FROM company_tp ctp WHERE ctp.company_nid = n.nid AND ctp.status = 1) as tp_count,
                (SELECT rg.field_kladr_region_ais_id_value FROM content_type_kladr_region rg WHERE rg.nid = ctc.field_company_adr_region_nid AND rg.field_kladr_region_archaic_value is null LIMIT 1) as legal_region_ais_id,
                (SELECT rg.field_kladr_region_ais_id_value FROM content_type_kladr_region rg WHERE rg.nid = ctc.field_company_real_adr_region_nid AND rg.field_kladr_region_archaic_value is null LIMIT 1) as actual_region_ais_id,
                (SELECT rg.field_kladr_region_ais_id_value FROM content_type_kladr_region rg WHERE rg.nid = ctc.field_company_post_adr_region_nid AND rg.field_kladr_region_archaic_value is null LIMIT 1) as post_region_ais_id,
                (SELECT r.field_kladr_raion_ais_id_value FROM content_type_kladr_raion r WHERE r.nid = ctc.field_company_adr_raion_nid AND r.field_kladr_raion_archaic_value is null LIMIT 1) as legal_raion_ais_id,
                (SELECT r.field_kladr_raion_ais_id_value FROM content_type_kladr_raion r WHERE r.nid = ctc.field_company_real_adr_raion_nid AND r.field_kladr_raion_archaic_value is null LIMIT 1) as actual_raion_ais_id,
                (SELECT r.field_kladr_raion_ais_id_value FROM content_type_kladr_raion r WHERE r.nid = ctc.field_company_post_adr_raion_nid AND r.field_kladr_raion_archaic_value is null LIMIT 1) as post_raion_ais_id,
                (SELECT np.field_kladr_naspunkt_ais_id_value FROM content_type_kladr_naspunkt np WHERE np.nid = ctc.field_company_adr_naspunkt_nid AND np.field_kladr_naspunkt_archaic_value is null LIMIT 1) as legal_naspunkt_ais_id,
                (SELECT np.field_kladr_naspunkt_ais_id_value FROM content_type_kladr_naspunkt np WHERE np.nid = ctc.field_company_real_adr_naspunkt_nid AND np.field_kladr_naspunkt_archaic_value is null LIMIT 1) as actual_naspunkt_ais_id,
                (SELECT np.field_kladr_naspunkt_ais_id_value FROM content_type_kladr_naspunkt np WHERE np.nid = ctc.field_company_post_adr_naspunkt_nid AND np.field_kladr_naspunkt_archaic_value is null LIMIT 1) as post_naspunkt_ais_id,
                (SELECT st.field_kladr_street_ais_id_value FROM content_type_kladr_street st WHERE st.nid = ctc.field_company_adr_street_nid AND st.field_kladr_street_archaic_value is null LIMIT 1) as legal_street_ais_id,
                (SELECT st.field_kladr_street_ais_id_value FROM content_type_kladr_street st WHERE st.nid = ctc.field_company_real_adr_street_nid AND st.field_kladr_street_archaic_value is null LIMIT 1) as actual_street_ais_id,
                (SELECT st.field_kladr_street_ais_id_value FROM content_type_kladr_street st WHERE st.nid = ctc.field_company_post_adr_street_nid AND st.field_kladr_street_archaic_value is null LIMIT 1) as post_street_ais_id,
                ctc.field_company_adr_num_value as legal_h_number, ctc.field_company_adr_korpus_value as legal_h_korp, ctc.field_company_adr_part_value as legal_h_part, ctc.field_company_adr_rm_num_value as legal_h_rm,
                ctc.field_company_real_adr_num_value as actual_h_number, ctc.field_company_real_adr_korpus_value as actual_h_korp, ctc.field_company_real_adr_par_value as actual_h_part, ctc.field_company_real_adr_rm_num_value as actual_h_rm,
                ctc.field_company_post_adr_num_value as post_h_number, ctc.field_company_post_adr_korpus_value as post_h_korp, ctc.field_company_post_adr_part_value as post_h_part, ctc.field_company_post_adr_rm_num_value as post_h_rm,
                ctc.field_company_adr_comment_value as legal_comment, ctc.field_company_real_adr_comment_value as actual_comment, ctc.field_company_post_adr_comment_value as post_comment,
                ctc.field_company_type_value,
                a.*
                FROM uo_anketa_archive a JOIN node n ON n.nid = a.field_org_company_nid JOIN node_revisions nr ON nr.vid = n.vid JOIN content_type_company ctc ON ctc.vid = n.vid
                WHERE n.type = 'company' and rating_uo_id = ".$period['id']." GROUP BY n.nid ORDER BY n.nid ASC LIMIT $offset, $limit"
            );
        } else {
            $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
                "SELECT
                1 as is_current,
                n.nid as company_id, n.title, n.status, n.uid as user_id, nr.log as log, n.created,
                ctc.field_company_fullname_value, ctc.field_company_type_value, ctc.field_company_okopf_value,
                ctc.field_company_inn_value, ctc.field_company_ogrn_value, ctc.field_company_ogrn_value, ctc.field_company_ogrn_date_value, ctc.field_company_register_dep_value, ctc.field_company_kpp_value,
                ctc.field_company_surname_value, ctc.field_company_firstname_value, ctc.field_company_middlename_value,
                ctc.field_company_position_value, ctc.field_company_phone_value, ctc.field_company_email_email,
                ctc.field_company_work_time_value, ctc.field_company_date_begin_value, ctc.field_company_date_end_value,
                ctc.field_company_notes_value, ctc.field_company_site_value, ctc.field_compant_dolya_sf_value, ctc.field_compant_dolya_mo_value,
                ctc.field_company_admin_mo_value, ctc.field_company_is_declined_value,
                (SELECT count(*) FROM company_tp ctp WHERE ctp.company_nid = n.nid AND ctp.status = 1) as tp_count,
                (SELECT rg.field_kladr_region_ais_id_value FROM content_type_kladr_region rg WHERE rg.nid = ctc.field_company_adr_region_nid AND rg.field_kladr_region_archaic_value is null LIMIT 1) as legal_region_ais_id,
                (SELECT rg.field_kladr_region_ais_id_value FROM content_type_kladr_region rg WHERE rg.nid = ctc.field_company_real_adr_region_nid AND rg.field_kladr_region_archaic_value is null LIMIT 1) as actual_region_ais_id,
                (SELECT rg.field_kladr_region_ais_id_value FROM content_type_kladr_region rg WHERE rg.nid = ctc.field_company_post_adr_region_nid AND rg.field_kladr_region_archaic_value is null LIMIT 1) as post_region_ais_id,
                (SELECT r.field_kladr_raion_ais_id_value FROM content_type_kladr_raion r WHERE r.nid = ctc.field_company_adr_raion_nid AND r.field_kladr_raion_archaic_value is null LIMIT 1) as legal_raion_ais_id,
                (SELECT r.field_kladr_raion_ais_id_value FROM content_type_kladr_raion r WHERE r.nid = ctc.field_company_real_adr_raion_nid AND r.field_kladr_raion_archaic_value is null LIMIT 1) as actual_raion_ais_id,
                (SELECT r.field_kladr_raion_ais_id_value FROM content_type_kladr_raion r WHERE r.nid = ctc.field_company_post_adr_raion_nid AND r.field_kladr_raion_archaic_value is null LIMIT 1) as post_raion_ais_id,
                (SELECT np.field_kladr_naspunkt_ais_id_value FROM content_type_kladr_naspunkt np WHERE np.nid = ctc.field_company_adr_naspunkt_nid AND np.field_kladr_naspunkt_archaic_value is null LIMIT 1) as legal_naspunkt_ais_id,
                (SELECT np.field_kladr_naspunkt_ais_id_value FROM content_type_kladr_naspunkt np WHERE np.nid = ctc.field_company_real_adr_naspunkt_nid AND np.field_kladr_naspunkt_archaic_value is null LIMIT 1) as actual_naspunkt_ais_id,
                (SELECT np.field_kladr_naspunkt_ais_id_value FROM content_type_kladr_naspunkt np WHERE np.nid = ctc.field_company_post_adr_naspunkt_nid AND np.field_kladr_naspunkt_archaic_value is null LIMIT 1) as post_naspunkt_ais_id,
                (SELECT st.field_kladr_street_ais_id_value FROM content_type_kladr_street st WHERE st.nid = ctc.field_company_adr_street_nid AND st.field_kladr_street_archaic_value is null LIMIT 1) as legal_street_ais_id,
                (SELECT st.field_kladr_street_ais_id_value FROM content_type_kladr_street st WHERE st.nid = ctc.field_company_real_adr_street_nid AND st.field_kladr_street_archaic_value is null LIMIT 1) as actual_street_ais_id,
                (SELECT st.field_kladr_street_ais_id_value FROM content_type_kladr_street st WHERE st.nid = ctc.field_company_post_adr_street_nid AND st.field_kladr_street_archaic_value is null LIMIT 1) as post_street_ais_id,
                ctc.field_company_adr_num_value as legal_h_number, ctc.field_company_adr_korpus_value as legal_h_korp, ctc.field_company_adr_part_value as legal_h_part, ctc.field_company_adr_rm_num_value as legal_h_rm,
                ctc.field_company_real_adr_num_value as actual_h_number, ctc.field_company_real_adr_korpus_value as actual_h_korp, ctc.field_company_real_adr_par_value as actual_h_part, ctc.field_company_real_adr_rm_num_value as actual_h_rm,
                ctc.field_company_post_adr_num_value as post_h_number, ctc.field_company_post_adr_korpus_value as post_h_korp, ctc.field_company_post_adr_part_value as post_h_part, ctc.field_company_post_adr_rm_num_value as post_h_rm,
                ctc.field_company_adr_comment_value as legal_comment, ctc.field_company_real_adr_comment_value as actual_comment, ctc.field_company_post_adr_comment_value as post_comment,
                o.*
                FROM node_revisions as nr INNER JOIN node as n ON n.vid = nr.vid
                LEFT JOIN content_type_company ctc ON ctc.vid = n.vid
                LEFT JOIN content_type_organization o ON o.vid = (select max(o2.vid) from content_type_organization o2 where o2.field_org_company_nid = n.nid)
                WHERE n.type = 'company' GROUP BY n.nid ORDER BY n.nid ASC LIMIT $offset, $limit"
            );
        }

        return $resultData;
    }

    public function generatePeriodicItems_rgkh2()
    {
        if ($this->_forceMode){
            $this->togglePgTblAllTriggers('company_profile_items', 'disable');
            $this->togglePgTblAllTriggers('company_profile_criteria_items', 'disable');
        }


        foreach ($this->_org_groups as $org_group_id=>$org_group_title) {
            $this->prepareValue($org_group_title, 'string');
            $this->executeQueryWithLog($this->getPgsqlEntityManager(),
                'INSERT INTO company_profile_items ("id","item_name") VALUES('.$org_group_id.','.$org_group_title.')');
        }

        foreach ($this->_org_fields as $org_field_id=>$org_field_title) {
            $this->prepareValue($org_field_title, 'string');
            $this->executeQueryWithLog($this->getPgsqlEntityManager(),
                'INSERT INTO company_profile_criteria_items ("id","criteria_name", "criteria_type_value") VALUES('.$org_field_id.','.$org_field_title.', \'numeric(5.2)\')');
        }

        if ($this->_forceMode){
            $this->togglePgTblAllTriggers('company_profile_items', 'enable');
            $this->togglePgTblAllTriggers('company_profile_criteria_items', 'enable');
        }
    }

    public function selectReportingPeriods_rgkh1()
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT r.id, r.date_start, r.date_end, r.date_result, r.name, r.status, r.avg_rating FROM rating_uo r"
        );

        foreach ($resultData as &$row) {
            if ($row['id'] == 3) $row['date_start'] = '2012-07-01';
            if ($row['id'] == 4) $row['date_end'] = '2014-04-23';   // @TODO  2014-05-06
            if ($row['id'] == 8) $row['date_start'] = '2014-04-24';   // @TODO 2014-05-07
        }
        //   $resultData[] = array('id'=>5, 'date_start'=>'2014-01-01', 'date_end'=>'2015-01-01','status'=>'0');

        return $resultData;
    }

    public function insertDataToTblPeriods_rgkh2($data)
    {
        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('reporting_periods', 'disable');

        $sql = 'INSERT INTO reporting_periods ("id", "name", "state", "date_start", "date_end", "date_result", "rating") VALUES';
        $sql_values = array();

        foreach ($data as $row) {
            $values = array();
            if ($row['id'] == 4) $row['date_end'] = '2013-12-31';   // @TODO
            if ($row['id'] == 8) $row['date_start'] = '2014-01-01';   // @TODO
            $values[] = $this->prepareValue($row['id'],'integer');
            $values[] = $this->prepareValue($row['name'],'string');
            if ($row['status']==1) $row['status'] = 'current';
            else if ($row['status']==0) $row['status'] = 'archive';
            $values[] = $this->prepareValue($row['status'],'string');
            $values[] = $this->prepareValue($row['date_start'],'string');
            $values[] = $this->prepareValue($row['date_end'],'string');
            $values[] = $this->prepareValue($row['date_result'],'string');
            $values[] = (empty($row['avg_rating']) ? "false" : "true");
            $sql_values[] = '('.implode(',',$values).')';
        }

        $this->insertByParts($this->getPgSqlEntityManager(), $sql, $sql_values); unset($sql_values);

        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('reporting_periods', 'enable');
    }



    public function selectLastIdAddresses()
    {
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "select max(\"id\") as last_id from addresses");

        $this->_adr_id = $resultData[0]['last_id'];
/*
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "select \"id\", \"full_address\" from addresses");

        foreach ($resultData as $i=>$row) {
            $this->_addressesArray[$row['full_address']] = $row['id'];
            unset($resultData[$i]);
        }*/
        unset($resultData);
    }

    public function insertDataToTblManagementOrgs_rgkh2($data, $reportPeriodId = null)
    {
        if ($this->_forceMode) {
            $this->togglePgTblAllTriggers('management_organizations', 'disable');
            $this->togglePgTblAllTriggers('providers', 'disable');
            $this->togglePgTblAllTriggers('user_registration_requests', 'disable');
            $this->togglePgTblAllTriggers('addresses', 'disable');
            $this->togglePgTblAllTriggers('company_requisites', 'disable');
            $this->togglePgTblAllTriggers('company_employee', 'disable');
            $this->togglePgTblAllTriggers('company_profiles', 'disable');
            $this->togglePgTblAllTriggers('company_profile_static_data', 'disable');
            $this->togglePgTblAllTriggers('company_profile_periodic_data', 'disable');
            $this->togglePgTblAllTriggers('company_profile_criteria_values', 'disable');
        }

        $sql = 'INSERT INTO management_organizations
        ("id", "company_requisite_id", "company_employee_id", "type", "status", "activity_start_date", "activity_end_date",
        "activity_end_reason",  "notes", "proportion_sf", "proportion_mo", "date_created")
         VALUES';
        $sql_values = array();

        $sql_providers = 'INSERT INTO providers
        ("id", "requisite_id", "company_employee_id", "proportion_sf", "proportion_mo")
         VALUES';
        $sql_values_providers = array();

        $sql_requests = 'INSERT INTO user_registration_requests
        ("id", "user_id", "company_requisites_id", "company_employee_id", "status", "cancel_reason", "type", "notes", "proportion_sf", "proportion_mo", "date_created", "date_processing")
         VALUES';
        $sql_values_requests = array();

        $sql_addr = 'INSERT INTO addresses ("id","region_id", "area_id", "city_id", "street_id", "house_number", "block", "building", "room_number", "comment", "flag_invalid", "full_address", "address_comment") VALUES';
        $sql_values_addr = array();

        $sql_org_requis = 'INSERT INTO company_requisites ("id","name_short", "name_full", "okopf", "inn", "ogrn", "date_assignment_ogrn", "name_authority_assigning_ogrn", "kpp", "legal_address_id", "actual_address_id", "post_address_id", "phone", "email", "site", "work_time") VALUES';
        $sql_values_org_requis = array();

        $sql_org_employee = 'INSERT INTO company_employee ("id","firstname", "middlename", "surname", "position") VALUES';
        $sql_values_org_employee = array();

        $sql_profiles = 'INSERT INTO company_profiles
        ("reporting_period_id", "company_id", "static_data_id", "periodic_data_id", "flag_current", "flag_rating")
         VALUES';
        $sql_values_profiles = array();

        $sql_profiles_static = 'INSERT INTO company_profile_static_data
        ("id", "type", "company_employee_id", "company_requisites_id", "proportion_sf", "proportion_mo", "notes", "participation_in_associations", "srf_count", "mo_count", "offices_count", "staff_regular_total", "staff_regular_administrative", "staff_regular_engineers", "staff_regular_labor", "residents_count", "additional_info", "org_sro", "prosecute_copies_of_documents", "tsg_management_members", "audit_commision_members", "additional_info_freeform")
         VALUES';
        $sql_values_profiles_static = array();

        $sql_profiles_periodic = 'INSERT INTO company_profile_periodic_data
        ("id", "net_assets", "annual_financial_statements", "debt_owners", "management_contract", "services_cost", "tariffs", "prosecute_count", "accidents_count", "revenues_expenditures_estimates", "performance_report", "members_meetings_minutes", "audit_commision_report","audit_report","debt_owners_communal")
         VALUES';
        $sql_values_profiles_periodic = array();

        $sql_profiles_criteries = 'INSERT INTO company_profile_criteria_values
        ("company_profile_criteria_items_id", "company_profile_items_id", "company_profile_periodic_data_id", "value")
         VALUES';
        $sql_values_criteries = array();


        foreach ($data as $row) {

            switch ($row['field_company_type_value']) {
                case '1': $row['field_company_type_value'] = 'company'; break;
                case '2': $row['field_company_type_value'] = 'partnership'; break;
                case '3': $row['field_company_type_value'] = 'partnership-uo'; break;
                case 'OKK': $row['field_company_type_value'] = 'partnership'; break;
                default: $row['field_company_type_value'] = 'partnership-uo';
            }

            if ($row['field_company_type_value'] == 'company' && in_array($row['company_id'],$this->_providers_uo)) {   // если уо указано в договорах как поставщик, то определяем тип уо-поставщик
                $row['field_company_type_value'] = 'partnership-uo';
            }
            if ($row['field_company_type_value'] == 'partnership' && $row['tp_count'] > 0) { // если поставщик имеет список привзяанных мкд, то определяем тип уо-поставщик
                $row['field_company_type_value'] = 'partnership-uo';
            }

            $row['org_status'] = $row['status'] == '1' ? 'active' : 'inactive'; // @TODO: inactive ?
            $row['field_company_date_begin_value'] = date('Y-m-d', strtotime($row['field_company_date_begin_value']));
            $row['field_company_date_end_value'] = date('Y-m-d', strtotime($row['field_company_date_end_value']));
            $row['end_reason'] = '';    // @TODO: откуда берется это значение?

            if (empty($row['field_compant_dolya_sf_value'])) $row['field_compant_dolya_sf_value'] = '0.00';
            if (empty($row['field_compant_dolya_mo_value'])) $row['field_compant_dolya_mo_value'] = '0.00';

            //pg_escape_string
            $row['end_reason'] = $this->escape_string($row['end_reason']);
            $row['field_company_notes_value'] = $this->escape_string($row['field_company_notes_value']);

            $is_request = false;
            $row['req_status'] = 'approved';

            if ($row['field_company_type_value'] == 'company') {
                if ($row['status'] == '0' && $row['field_company_is_declined_value'] == '0' && empty($row['field_company_admin_mo_value'])) {
                    $row['req_status'] = 'not_mo';
                    $is_request = true;
                }
                else if ($row['status'] == '1' && $row['field_company_is_declined_value'] == '0' && !empty($row['field_company_admin_mo_value'])) {
                    $row['req_status'] = 'approved';
                    $is_request = false;
                }
                else if ($row['field_company_is_declined_value'] == '1' && !empty($row['log']) && !empty($row['field_company_admin_mo_value'])) {
                    $row['req_status'] = 'rejected';
                    $is_request = true;
                }
                else if ($row['status'] == '0' && $row['field_company_is_declined_value'] == '1' && empty($row['log']) && !empty($row['field_company_admin_mo_value'])) {
                    $row['req_status'] = 'canceled';
                    $is_request = true;
                }
                else if ($row['status'] == '0' && $row['field_company_is_declined_value'] == '0' && !empty($row['field_company_admin_mo_value'])) {
                    $row['req_status'] = 'in_checking';
                    $is_request = true;
                }
                else {
                    $row['req_status'] = 'approved';
                    $is_request = false;
                }
            }

            $row['cancel_reason'] = $row['field_company_is_declined_value'] == '1' ? "'{$row['log']}'" : 'null';

            $row['created'] = (isset($row['created'])) ? "'".date(self::DATE_FORMAT, $row['created'])."'" : $row['created'] = "null";

            $l_adr_value = $this->addLegalAddresesValuesRow($row);
            if (!is_null($l_adr_value)) $sql_values_addr[] = $l_adr_value;
            $a_adr_value = $this->addActualAddresesValuesRow($row);
            if (!is_null($a_adr_value)) $sql_values_addr[] = $a_adr_value;
            $p_adr_value = $this->addPostAddresesValuesRow($row);
            if (!is_null($p_adr_value)) $sql_values_addr[] = $p_adr_value;


            if (!in_array($row['company_id'], $this->_company_array)) {


                if (!$is_request) {
                    $org_requis_value = $this->addOrgRequisitesValuesRow($row);
                    if (!is_null($org_requis_value)) $sql_values_org_requis[] = $org_requis_value;

                    $org_employee_value = $this->addOrgEmployeeValuesRow($row);
                    if (!is_null($org_employee_value)) $sql_values_org_employee[] = $org_employee_value;

                    $company_org_requis_id = $this->_org_requis_id;
                    $company_org_employee_id = $this->_org_employee_id;
                }



                    $org_requis_value = $this->addOrgRequisitesValuesRow($row);
                    if (!is_null($org_requis_value)) $sql_values_org_requis[] = $org_requis_value;

                    $org_employee_value = $this->addOrgEmployeeValuesRow($row);
                    if (!is_null($org_employee_value)) $sql_values_org_employee[] = $org_employee_value;

                    $this->prepareValue($row['field_company_admin_mo_value'], 'integer');
                    $this->prepareValue($row['user_id'], 'integer');

                if ($row['field_company_type_value'] != 'partnership') {
                    $c_type = $row['field_company_type_value'];
                    if ($row['field_company_type_value'] == 'partnership-uo') $c_type= 'partnership';
                    $p_sf = $row['field_compant_dolya_sf_value'];
                    $p_mo = $row['field_compant_dolya_mo_value'];
                    $this->prepareValue($p_sf,'string');
                    $this->prepareValue($p_mo,'string');
                    $sql_values_requests[] = "(".$row['company_id'].", ".$row['user_id'].", ".$this->_org_requis_id.", ".$this->_org_employee_id.", '".$row['req_status']."', ".$row['cancel_reason'].", '".$c_type."'
                    , '".$row['field_company_notes_value']."', ".$p_sf.", ".$p_mo.",".$row['created'].", null)";
                }


                            if ($row['field_company_type_value'] == 'partnership' || $row['field_company_type_value'] == 'partnership-uo') {
                                $sql_values_providers[] = "(".$row['company_id'].", ".$company_org_requis_id.", ".$company_org_employee_id.", ".$row['field_compant_dolya_sf_value'].", ".$row['field_compant_dolya_mo_value'].")";
                            }
                            if (!$is_request && $row['field_company_type_value'] != 'partnership') {
                                $c_type = $row['field_company_type_value'];
                                if ($row['field_company_type_value'] == 'partnership-uo') $c_type= 'partnership';
                                $sql_values[] = "(".$row['company_id'].", ".$company_org_requis_id.", ".$company_org_employee_id.",'".$c_type."', '".$row['org_status']."', '".$row['field_company_date_begin_value']."', '".$row['field_company_date_end_value']."', '".$row['end_reason']."'
                                , '".$row['field_company_notes_value']."', ".$row['field_compant_dolya_sf_value'].", ".$row['field_compant_dolya_mo_value'].",".$row['created'].")";
                            }


                $this->_company_array[] = $row['company_id'];
            }else { // @TODO:
                $org_requis_value = $this->addOrgRequisitesValuesRow($row);
                if (!is_null($org_requis_value)) $sql_values_org_requis[] = $org_requis_value;

                $org_employee_value = $this->addOrgEmployeeValuesRow($row);
                if (!is_null($org_employee_value)) $sql_values_org_employee[] = $org_employee_value;

                $company_org_requis_id = $this->_org_requis_id;
                $company_org_employee_id = $this->_org_employee_id;

                if ($row['field_company_type_value'] == 'partnership-uo' || $row['field_company_type_value'] == 'company') {
                    $this->_update_mo[$row['company_id']] = '('.$company_org_requis_id.', '.$company_org_employee_id.', '.$row['company_id'].')';
                }
                if ($row['field_company_type_value'] == 'partnership-uo' || $row['field_company_type_value'] == 'partnership') {
                    $this->_update_prov[$row['company_id']] = '('.$company_org_requis_id.', '.$company_org_employee_id.', '.$row['company_id'].')';
                }
            }

            if (!$is_request && $row['field_company_type_value'] != 'partnership') {
                $sql_value = $this->addOrgProfilePeriodicValuesRow($row);
                if (!is_null($sql_value)) $sql_values_profiles_periodic[] = $sql_value;

                $sql_value = $this->addOrgProfileStaticValuesRow($row, $company_org_requis_id, $company_org_employee_id);
                if (!is_null($sql_value)) $sql_values_profiles_static[] = $sql_value;

                $sql_value = $this->addOrgProfileValuesRow($row, $reportPeriodId);
                if (!is_null($sql_value)) $sql_values_profiles[] = $sql_value;


                foreach ($row as $field => $field_value) {
                    if (in_array($field,array_keys($this->_org_items))) {
                        if (!is_null($field_value)){
                            $sql_value = $this->addOrgCriteriaValuesRow($this->_org_items[$field][0],$this->_org_items[$field][1], $field_value);
                            if (!is_null($sql_value)) $sql_values_criteries[] = $sql_value;
                        }
                    }
                }
            }
        }


        $this->insertByParts($this->getPgSqlEntityManager(), $sql_addr, $sql_values_addr, 1000, false); unset($sql_values_addr);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_org_requis, $sql_values_org_requis, 500); unset($sql_values_org_requis);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_org_employee, $sql_values_org_employee, 500); unset($sql_values_org_employee);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql, $sql_values, 500); unset($sql_values);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_providers, $sql_values_providers, 1000); unset($sql_values_providers);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_requests, $sql_values_requests, 1000); unset($sql_values_requests);

        $this->insertByParts($this->getPgSqlEntityManager(), $sql_profiles_periodic, $sql_values_profiles_periodic, 1000); unset($sql_values_profiles_periodic);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_profiles_criteries, $sql_values_criteries, 1000); unset($sql_values_criteries);
        $this->insertByParts($this->getPgSqlEntityManager(), $sql_profiles_static, $sql_values_profiles_static, 1000); unset($sql_values_profiles_static);
        $result = $this->insertByParts($this->getPgSqlEntityManager(), $sql_profiles, $sql_values_profiles, 1000); unset($sql_values_profiles);

        if (!empty($this->_update_mo))
            $this->executeQueryWithLog($this->getPgsqlEntityManager(), 'UPDATE management_organizations SET "company_requisite_id" = v.req, "company_employee_id" = v.emp FROM (VALUES '.implode(',',$this->_update_mo).') as v(req,emp,oid) WHERE "id" = v.oid');
        if (!empty($this->_update_prov))
            $this->executeQueryWithLog($this->getPgsqlEntityManager(), 'UPDATE providers SET "requisite_id" = v.req, "company_employee_id" = v.emp FROM (VALUES '.implode(',',$this->_update_prov).') as v(req,emp,oid) WHERE "id" = v.oid');

        if ($this->_forceMode) {
            $this->togglePgTblAllTriggers('management_organizations', 'enable');
            $this->togglePgTblAllTriggers('providers', 'enable');
            $this->togglePgTblAllTriggers('user_registration_requests', 'enable');
            $this->togglePgTblAllTriggers('addresses', 'enable');
            $this->togglePgTblAllTriggers('company_requisites', 'enable');
            $this->togglePgTblAllTriggers('company_employee', 'enable');
            $this->togglePgTblAllTriggers('company_profiles', 'enable');
            $this->togglePgTblAllTriggers('company_profile_static_data', 'enable');
            $this->togglePgTblAllTriggers('company_profile_periodic_data', 'enable');
            $this->togglePgTblAllTriggers('company_profile_criteria_values', 'enable');
        }

        return $result;
    }

    public function updateOrgNotMOStatus() {
        $this->executeQueryWithLog($this->getPgsqlEntityManager(), "update user_registration_requests set status = 'rejected' from(select ur.id from user_registration_requests ur join company_requisites urr on urr.id = ur.company_requisites_id join company_requisites orr on orr.inn = urr.inn join management_organizations o on o.company_requisite_id = orr.id where ur.status = 'not_mo') as v(urid) where id = urid");
        $this->executeQueryWithLog($this->getPgsqlEntityManager(), "update user_registration_requests set status = 'canceled' where status = 'not_mo'");
    }

    private function addOrgCriteriaValuesRow($cirteriaItemId, $itemId, $value){
        $values = array();

        $values[] = $this->prepareValue($cirteriaItemId, 'integer');
        $values[] = $this->prepareValue($itemId, 'string');
        $values[] = $this->_profile_periodic_id;
        $values[] = $this->prepareValue($value, 'string');

        return '('.implode(',',$values).')';
    }


    private function addOrgProfilePeriodicValuesRow($row)
    {
        $this->_profile_periodic_id++;

        $values = array();

        $values[] = $this->_profile_periodic_id;
        $values[] = $this->prepareValue($row['field_org_cache_uo_value'],'integer');
        $values[] = $this->prepareValue($row['field_org_god_bug_otch_value'],'string');
        $values[] = $this->prepareValue($row['field_org_trust_start_value'],'string');
        $values[] = $this->prepareValue($row['field_org_proj_dog_upr_value'],'string');
        $values[] = $this->prepareValue($row['field_org_stoimost_uslug_value'],'string');
        $values[] = $this->prepareValue($row['field_org_tarifs_value'],'string');
        $values[] = $this->prepareValue($row['field_org_adm_respons_value'],'integer');
        $values[] = $this->prepareValue($row['field_organization_num_accidents_value'],'integer');

        $values[] = $this->prepareValue($row['field_reports_gsk_value'], 'string');
        $values[] = $this->prepareValue($row['field_reports_finish_gsk_value'], 'string');
        $values[] = $this->prepareValue($row[''], 'string');
        $values[] = $this->prepareValue($row['field_answer_reports_value'], 'string');
        $values[] = $this->prepareValue($row['field_audit_reports_value'], 'string');
        $values[] = $this->prepareValue($row['field_org_trust_from_begin_per_value'], 'string');

        return '('.implode(',',$values).')';
    }


    private function addOrgProfileStaticValuesRow($row, $company_org_requis_id, $company_org_employee_id)
    {
        $this->_profile_static_id++;

        $values = array();

        $values[] = $this->_profile_static_id;
        $c_type = $row['field_company_type_value'];
        if ($row['field_company_type_value'] == 'partnership-uo') $c_type= 'partnership';
        $values[] = $this->prepareValue($c_type,'string');;
        $values[] = $company_org_employee_id;
        $values[] = $company_org_requis_id;
        $values[] = $this->prepareValue($row['field_compant_dolya_sf_value'],'string');
        $values[] = $this->prepareValue($row['field_compant_dolya_mo_value'],'string');
        $values[] = $this->prepareValue($row['field_company_notes_value'],'string');
        $values[] = '\'\'';
        $values[] = $this->prepareValue($row['field_organization_count_sf_value'], 'integer');
        $values[] = $this->prepareValue($row['field_organization_count_mo_value'], 'integer');
        $values[] = $this->prepareValue($row['field_organization_count_offices_value'], 'integer');
        $values[] = $this->prepareValue($row['field_org_count_all_value'], 'integer');
        $values[] = $this->prepareValue($row['field_org_count_adm_value'], 'integer');
        $values[] = $this->prepareValue($row['field_org_count_engineer_value'], 'integer');
        $values[] = $this->prepareValue($row['field_org_count_work_value'], 'integer');
        $values[] = $this->prepareValue($row['field_org_count_residents_value'], 'integer');
        $values[] = $this->prepareValue($row['field_org_additional_info_value'], 'string');
        $values[] = $this->prepareValue($row['field_org_sro_value'], 'string');

        $values[] = $this->prepareValue($row['field_org_copy_doc_admin_value'], 'string');
        $values[] = $this->prepareValue($row['field_org_members_tsg_value'], 'string');
        $values[] = $this->prepareValue($row['field_org_memers_revision_value'], 'string');
        $values[] = $this->prepareValue($row['field_org_info_userform_value'], 'string');

        return '('.implode(',',$values).')';

    }

    private function addOrgProfileValuesRow($row, $reportingPeriodId)
    {
        $this->prepareValue($row['company_id'], 'integer');

        if ($row['is_current']=='1') $row['is_current'] = 'true'; else $row['is_current'] = 'false';

        return '('.$reportingPeriodId.','.$row['company_id'].', '.$this->_profile_static_id.', '.$this->_profile_periodic_id.', false, false)';

    }

    private function addOrgRequisitesValuesRow($row)
    {
        $row['title'] = $this->escape_string($row['title']);
        if (empty($row['field_company_fullname_value']))     $row['field_company_fullname_value'] = $row['title'];
        $row['field_company_fullname_value'] = $this->escape_string($row['field_company_fullname_value']);
        $row['field_company_site_value'] = $this->escape_string($row['field_company_site_value']);
        $row['field_company_phone_value'] = $this->escape_string($row['field_company_phone_value']);
        $row['field_company_email_email'] = $this->escape_string($row['field_company_email_email']);
        $row['field_org_work_time_value'] = $this->escape_string($row['field_org_work_time_value']);
        $row['field_company_ogrn_date_value'] = strtotime($row['field_company_ogrn_date_value']);
        $this->prepareValue($row['field_company_ogrn_date_value'], 'date');
        $this->prepareValue($row['field_company_register_dep_value'], 'string');

        ++$this->_org_requis_id;

        $values = "(".$this->_org_requis_id.",'".$row['title']."','".$row['field_company_fullname_value']."','".$row['field_company_okopf_value']."','".$row['field_company_inn_value']."','".$row['field_company_ogrn_value']."',".$row['field_company_ogrn_date_value'].",".$row['field_company_register_dep_value'].",'".$row['field_company_kpp_value']."'
        , ".$this->_legal_adr_id.", ".$this->_actual_adr_id.", ".$this->_post_adr_id.",'".$row['field_company_phone_value']."','".$row['field_company_email_email']."','".$row['field_company_site_value']."', '".$row['field_org_work_time_value']."')";

        return $values;
    }

    private function addOrgEmployeeValuesRow($row)
    {
        $row['field_company_surname_value'] = $this->escape_string($row['field_company_surname_value']);
        $row['field_company_firstname_value'] = $this->escape_string($row['field_company_firstname_value']);
        $row['field_company_middlename_value'] = $this->escape_string($row['field_company_middlename_value']);
        $row['field_company_position_value'] = $this->escape_string($row['field_company_position_value']);

        ++$this->_org_employee_id;

        $values = "(".$this->_org_employee_id.",'".$row['field_company_firstname_value']."','".$row['field_company_middlename_value']."','".$row['field_company_surname_value']."','".$row['field_company_position_value']."')";

        return $values;
    }

    private function addLegalAddresesValuesRow($row)
    {
        $row['legal_address_text'] = "";
        $value = null;

        if (!(empty($row['legal_region_ais_id']) && empty($row['legal_raion_ais_id']) && empty($row['legal_naspunkt_ais_id']) && empty($row['legal_street_ais_id'])))
        {
            $addressRow = $this->compareAisWithFias($row['legal_region_ais_id'], $row['legal_raion_ais_id'], $row['legal_naspunkt_ais_id'], $row['legal_street_ais_id'],
                $row['legal_h_number'], $row['legal_h_korp'], $row['legal_h_part'], $row['legal_h_rm'], $row['legal_comment']);

            $row['legal_address_text'] = $this->getAddressManager()->generateSerializedAddress($addressRow);

            if (isset($this->_addressesArray[$row['legal_address_text']])){
                $this->_legal_adr_id = $this->_addressesArray[$row['legal_address_text']];
                $this->_count_idents++;
            } else {
                $this->_legal_adr_id = ++$this->_adr_id;

                $this->prepareValue($addressRow['region_guid']);
                $this->prepareValue($addressRow['area_guid']);
                $this->prepareValue($addressRow['city_guid']);
                $this->prepareValue($addressRow['street_guid']);
                $this->prepareValue($addressRow['house_number']);
                $this->prepareValue($addressRow['housing']);
                $this->prepareValue($addressRow['building']);
                $this->prepareValue($addressRow['comment']);
                $this->prepareValue($addressRow['address_comment']);
                $this->prepareValue($addressRow['room']);
                $this->prepareValue($addressRow['flag_invalid']);
                $address_v = $row['legal_address_text'];
                $this->prepareValue($address_v);

                $value = "(".$this->_legal_adr_id.", ".$addressRow['region_guid'].",".$addressRow['area_guid'].",".$addressRow['city_guid'].",".$addressRow['street_guid'].",".$addressRow['house_number']."
                        ,".$addressRow['housing'].",".$addressRow['building'].",".$addressRow['room'].",".$addressRow['comment'].",".$addressRow['flag_invalid'].", ".$address_v.",".$addressRow['address_comment']." )";

                $this->_addressesArray[$row['legal_address_text']] = $this->_legal_adr_id;
            }
            unset($addressRow);
        }else {
            $this->_legal_adr_id = "null";
        }
        return $value;
    }

    private function addActualAddresesValuesRow($row)
    {
        $row['actual_address_text'] = "";
        $value = null;
        if (!(empty($row['actual_region_ais_id']) && empty($row['actual_raion_ais_id']) && empty($row['actual_naspunkt_ais_id']) && empty($row['actual_street_ais_id'])))
        {
            $addressRow = $this->compareAisWithFias($row['actual_region_ais_id'], $row['actual_raion_ais_id'], $row['actual_naspunkt_ais_id'], $row['actual_street_ais_id'],
                $row['actual_h_number'], $row['actual_h_korp'], $row['actual_h_part'], $row['actual_h_rm'], $row['actual_comment']);

            $row['actual_address_text'] = $this->getAddressManager()->generateSerializedAddress($addressRow);

            if (isset($this->_addressesArray[$row['actual_address_text']])){
                $this->_actual_adr_id = $this->_addressesArray[$row['actual_address_text']];
                $this->_count_idents++;
            } else {
                $this->_actual_adr_id = ++$this->_adr_id;

                $this->prepareValue($addressRow['region_guid']);
                $this->prepareValue($addressRow['area_guid']);
                $this->prepareValue($addressRow['city_guid']);
                $this->prepareValue($addressRow['street_guid']);
                $this->prepareValue($addressRow['house_number']);
                $this->prepareValue($addressRow['housing']);
                $this->prepareValue($addressRow['building']);
                $this->prepareValue($addressRow['comment']);
                $this->prepareValue($addressRow['address_comment']);
                $this->prepareValue($addressRow['room']);
                $this->prepareValue($addressRow['flag_invalid']);
                $address_v = $row['actual_address_text'];
                $this->prepareValue($address_v);

                $value = "(".$this->_actual_adr_id.", ".$addressRow['region_guid'].",".$addressRow['area_guid'].",".$addressRow['city_guid'].",".$addressRow['street_guid'].",".$addressRow['house_number']."
                        ,".$addressRow['housing'].",".$addressRow['building'].",".$addressRow['room'].",".$addressRow['comment'].",".$addressRow['flag_invalid'].", ".$address_v.",".$addressRow['address_comment']." )";

                $this->_addressesArray[$row['actual_address_text']] = $this->_actual_adr_id;
            }
            unset($addressRow);
        }else {
            $this->_actual_adr_id = "null";
        }
        return $value;
    }

    private function addPostAddresesValuesRow($row)
    {
        $row['post_address_text'] = "";
        $value = null;
        if (!(empty($row['post_region_ais_id']) && empty($row['post_raion_ais_id']) && empty($row['post_naspunkt_ais_id']) && empty($row['post_street_ais_id'])))
        {
            $addressRow = $this->compareAisWithFias($row['post_region_ais_id'], $row['post_raion_ais_id'], $row['post_naspunkt_ais_id'], $row['post_street_ais_id'],
                $row['post_h_number'], $row['post_h_korp'], $row['post_h_part'], $row['post_h_rm'], $row['post_comment']);

            $row['post_address_text'] = $this->getAddressManager()->generateSerializedAddress($addressRow);

            if (isset($this->_addressesArray[$row['post_address_text']])){
                $this->_post_adr_id = $this->_addressesArray[$row['post_address_text']];
                $this->_count_idents++;
            } else {
                $this->_post_adr_id = ++$this->_adr_id;

                $this->prepareValue($addressRow['region_guid']);
                $this->prepareValue($addressRow['area_guid']);
                $this->prepareValue($addressRow['city_guid']);
                $this->prepareValue($addressRow['street_guid']);
                $this->prepareValue($addressRow['house_number']);
                $this->prepareValue($addressRow['housing']);
                $this->prepareValue($addressRow['building']);
                $this->prepareValue($addressRow['comment']);
                $this->prepareValue($addressRow['address_comment']);
                $this->prepareValue($addressRow['room']);
                $this->prepareValue($addressRow['flag_invalid']);
                $address_v = $row['post_address_text'];
                $this->prepareValue($address_v);

                $value = "(".$this->_post_adr_id.", ".$addressRow['region_guid'].",".$addressRow['area_guid'].",".$addressRow['city_guid'].",".$addressRow['street_guid'].",".$addressRow['house_number']."
                        ,".$addressRow['housing'].",".$addressRow['building'].",".$addressRow['room'].",".$addressRow['comment'].",".$addressRow['flag_invalid'].", ".$address_v.",".$addressRow['address_comment']." )";

                $this->_addressesArray[$row['post_address_text']] = $this->_post_adr_id;
            }
            unset($addressRow);
        }else {
            $this->_post_adr_id = "null";
        }
        return $value;
    }

    public function updateCurrentRevisions_rgkh2()
    {
        $this->togglePgTblAllTriggers('company_profiles', 'disable');

        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "UPDATE company_profiles c SET flag_current = true WHERE c.company_id in (SELECT p.company_id
                FROM company_profiles p where (select count(p2.*)from  company_profiles p2 where p2.company_id = p.company_id and flag_current = true) = 0 ) and c.reporting_period_id = (select max(p3.reporting_period_id) from company_profiles p3 where p3.company_id = c.company_id);"
        );

        $this->togglePgTblAllTriggers('company_profiles', 'enable');

        return $resultData;

    }

    /** ---------------------------------- Management Organizations Has Users ------------------------------------------- */
    public function clearDataFromTblManagementOrgsUsers_rgkh2()
    {
        $this->togglePgTblAllTriggers('management_organizations_has_users', 'disable');

        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM management_organizations_has_users"
        );

        $this->togglePgTblAllTriggers('management_organizations_has_users', 'enable');

        return $resultData;
    }

    public function selectDataFromTblAdminUO_rgkh1()
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT * FROM admin_uo a WHERE a.uid > 0  ORDER BY a.uid ASC;"
        );

        return $resultData;
    }

    public function insertDataToTblManagementOrgsUsers_rgkh2($data)
    {

        $this->togglePgTblAllTriggers('management_organizations_has_users', 'disable');


        $sql = "INSERT INTO management_organizations_has_users VALUES";
        $sql_values = array();


        foreach ($data as $row) {
            $val = "(".$row['nid'].",".$row['uid'].",".$row['role'].")";
            if (!in_array($val, $sql_values))
                $sql_values[] = $val;
        }

        $result = $this->insertByParts($this->getPgSqlEntityManager(), $sql, $sql_values);

        $this->togglePgTblAllTriggers('management_organizations_has_users', 'enable');

        return $result;
    }
}
