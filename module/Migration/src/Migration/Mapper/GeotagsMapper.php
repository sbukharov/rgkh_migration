<?php

namespace Migration\Mapper;

class GeotagsMapper extends AbstractMapper
{
    public function clearDataFromTblGeo_rgkh2()
    {
        $this->togglePgTblAllTriggers('geo_tags', 'disable');

        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM geo_tags"
        );

        $this->togglePgTblAllTriggers('geo_tags', 'enable');

        return $resultData;
    }

    public function selectCountFromTblGeo_rgkh1()
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT count(geo_id) as count FROM geo_full_tbl"
        );

        return $resultData;
    }

    //@TODO: level 6 не переносить
    public function selectDataFromTblGeo_rgkh1($from, $to)
    {
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT g.geo_id, gf.geo_name, gf.geo_okato, gf.geo_oktmo, gf.parent_id, g.geo_level, g.geo_lft, g.geo_rgt, gf.geo_type, gf.geo_archive FROM geo_tbl g
             LEFT JOIN geo_full_tbl gf ON gf.geo_id = g.geo_id WHERE
             gf.parent_id IS NOT NULL AND g.geo_lft != 0 AND g.geo_rgt != 0 ORDER BY g.geo_id ASC LIMIT {$from},{$to}"
        );

       return $resultData;
    }

    public function insertDataToTblGeo_rgkh2($data)
    {
        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('geo_tags', 'disable');

        $sql = 'INSERT INTO geo_tags ("id", "name", "okato", "oktmo", "level", "lft", "rgt", "parent_geo_tag_id", "type", "is_archive", "count_children", "count_children_mo") VALUES';
        $sql_values = array();


        foreach ($data as $row) {
            $row['geo_name'] = $this->escape_string($row['geo_name']);
            if ($row['parent_id'] == '0') $row['parent_id'] = 'null';   // @TODO: что делать с parent_id=0?
            $this->prepareValue($row['geo_name'], 'string');
            if (empty($row['geo_okato']) && in_array($row['geo_level'], array(0,1,2))) $row['geo_okato'] = '0';
            $this->prepareValue($row['geo_okato'], 'string');
            if (empty($row['geo_oktmo']) && in_array($row['geo_level'], array(0,1,2))) $row['geo_oktmo'] = '0';
            $this->prepareValue($row['geo_oktmo'], 'string');
            if ($row['geo_oktmo'] == 'null') $row['geo_oktmo'] = "'0'";
            if ($row['geo_type'] == null && $row['geo_level'] == 0) $row['geo_type'] = iconv('utf-8','windows-1251','РФ');
            else if ($row['geo_type'] == null && $row['geo_level'] == 1) $row['geo_type'] = iconv('utf-8','windows-1251','ФО');
            else if ($row['geo_type'] == null && $row['geo_level'] == 2) $row['geo_type'] = iconv('utf-8','windows-1251','СФ');
            else if ($row['geo_type'] == null) continue;
            $this->prepareValue($row['geo_type'], 'string');
            $this->prepareValue($row['parent_id'], 'integer');

            $this->prepareValue($row['geo_level'], 'integer');
            $this->prepareValue($row['geo_lft'], 'integer');
            $this->prepareValue($row['geo_rgt'], 'integer');
            $this->prepareValue($row['geo_archive'], 'integer');

            $sql_values[] = "(".$row['geo_id'].", ".$row['geo_name'].", ".$row['geo_okato'].", ".$row['geo_oktmo'].", ".$row['geo_level'].", ".$row['geo_lft'].", ".$row['geo_rgt'].", ".$row['parent_id'].", ".$row['geo_type'].", ".$row['geo_archive'].", 0, 0 )";
        }

        $result = $this->insertByParts($this->getPgSqlEntityManager(), $sql, $sql_values);

        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('geo_tags', 'enable');

        return $result;
    }
}
