<?php

namespace Migration\Mapper;

interface InterfaceMapper
{
    const DATE_FORMAT = 'Y-m-d H:i:s';
    const OPERSTATUS_OLDREVAO = 80;
    const OPERSTATUS_NEWAO = 81;
    const OPERSTATUS_NEWREVAO_NAME = 82;
    const OPERSTATUS_NEWREVAO_CURRCODE = 83;
}
