<?php

namespace Migration\Mapper;

class OrgPeriodicData extends AbstractMapper
{

    protected $_org_groups = array(
        1=>'count_houses_under_mng_report_date',
        2=>'count_houses_under_mng_start_period',
        3=>'sum_sq_houses_under_mng_report_date',
        4=>'sum_sq_houses_under_mng_start_period',
        5=>'avg_time_service_mkd',
        6=>'income_of_mng',
        7=>'income_of_usage',
        8=>'income_of_ku',
        9=>'spending_of_mng',
        10=>'claims_by_contracts_mng',
        11=>'claims_by_rso',
        12=>'debt_for_mng',
        13=>'debt_owners_for_ku',
        14=>'debt_uo_for_ku',
        15=>'charged_for_mng',
        16=>'charged_for_resources',
        17=>'spending_repair',
        18=>'spending_beaty',
        19=>'spending_repair_invests',
        20=>'payed_ku_by_statements',
        21=>'payed_ku_by_needs',
        22=>'count_residents',
        23=>'count_dismissed'
    );

    protected $_org_fields = array(
        1=>'count_houses_under_mng_report_date',
        2=>'count_houses_under_mng_start_period',
        3=>'sum_sq_houses_under_mng_report_date',
        4=>'sum_sq_houses_under_mng_start_period',
        5=>'avg_time_service_mkd',
        6=>'income_of_mng',
        7=>'income_of_usage',
        8=>'income_of_ku',
        9=>'spending_of_mng',
        10=>'claims_by_contracts_mng',
        11=>'claims_by_rso',
        12=>'debt_for_mng',
        13=>'debt_owners_for_ku',
        14=>'debt_uo_for_ku',
        15=>'charged_for_mng',
        16=>'charged_for_resources',
        17=>'spending_repair',
        18=>'spending_beaty',
        19=>'spending_repair_invests',
        20=>'payed_ku_by_statements',
        21=>'payed_ku_by_needs',
        22=>'serviced_by_tsg',
        23=>'serviced_by_tsg_uo',
        24=>'serviced_by_owner_uo',
        25=>'serviced_by_competition',
        26=>'by_houses_25',
        27=>'by_houses_26_50',
        28=>'by_houses_51_75',
        29=>'by_houses_76',
        30=>'by_houses_alarm',
        31=>'by_heating',
        32=>'by_electro',
        33=>'by_gaz',
        34=>'by_hot_water',
        35=>'by_cold_water',
        36=>'by_sewerage',
        37=>'sum_sq_houses_concluded_contracts',
        38=>'sum_sq_houses_terminated_contracts',
        39=>'claims_for_damage',
        40=>'claims_for_refusal',
        41=>'claims_for_short_shipment',
        42=>'subsidy',
        43=>'credits',
        44=>'fin_lising',
        45=>'fin_service',
        46=>'contributions_residents',
        47=>'other_sources',
        48=>'count_residents',
        49=>'count_dismissed',
        50=>'count_dismissed_admins',
        51=>'count_dismissed_engineers',
        52=>'count_dismissed_workers',
    );

    protected $_org_items = array(
        'field_org_count_house_value' => array(1,1),
            'field_org_count_house_tszh_value' => array(22,1),
            'field_org_count_house_tszh_uo_value' => array(23,1),
            'field_org_count_house_sobstv_uo_value' => array(24,1),
            'field_org_count_house_konkurs_value' => array(25,1),
        'field_org_count_house_end_value' => array(2,2),
            'field_org_count_house_end_tszh_value' => array(22,2),
            'field_org_count_house_end_tszh_u_value' => array(23,2),
            'field_org_count_house_end_sobstv_value' => array(24,2),
            'field_org_count_house_end_konkur_value' => array(25,2),
        'field_org_s_house_value' => array(3, 3),
            'field_org_s_house_25_value' => array(26,3),
            'field_org_s_house_50_value' => array(27,3),
            'field_org_s_house_75_value' => array(28,3),
            'field_org_s_house_over_75_value' => array(29,3),
            'field_org_s_house_emerg_value' => array(30,3),
        'field_org_s_house_start_value' => array(4,4),
            'field_org_s_house_start_zak_dog_value' => array(37,4),
            'field_org_s_house_start_ras_dog_value' => array(38,4),
        'field_org_srok_mkd_value' => array(5,5),
            'field_org_srok_mkd_25_value' => array(26,5),
            'field_org_srok_mkd_50_value' => array(27,5),
            'field_org_srok_mkd_75_value' => array(28,5),
            'field_org_srok_mkd_over_75_value' => array(29,5),
            'field_org_srok_mkd_crashed_value' => array(30,5),
        'field_org_dohod_upr_value' => array(6,6),
            'field_org_dohod_upr_25_value' => array(26,6),
            'field_org_dohod_upr_50_value' => array(27,6),
            'field_org_dohod_upr_75_value' => array(28,6),
            'field_org_dohod_upr_over_75_value' => array(29,6),
            'field_org_dohod_upr_emerg_value' => array(30,6),
        'field_org_dohod_period_value' => array(7,7),
            'field_org_dohod_period_25_value' => array(26,7),
            'field_org_dohod_period_50_value' => array(27,7),
            'field_org_dohod_period_75_value' => array(28,7),
            'field_org_dohod_period_over_75_value' => array(29,7),
            'field_org_dohod_period_crashed_value' => array(30,7),
        'field_org_ku_value' => array(8,8),
            'field_org_ku_otoplenie_value' => array(31,8),
            'field_org_ku_elektichestvo_value' => array(32,8),
            'field_org_ku_gaz_value' => array(33,8),
            'field_org_ku_hot_voda_value' => array(34,8),
            'field_org_ku_cold_voda_value' => array(35,8),
            'field_org_ku_wastewater_value' => array(36,8),
        'field_org_rash_upr_value' => array(9,9),
            'field_org_rash_upr_25_value' => array(26,9),
            'field_org_rash_upr_50_value' => array(27,9),
            'field_org_rash_upr_75_value' => array(28,9),
            'field_org_rash_upr_over_75_value' => array(29,9),
            'field_org_rash_upr_emerg_value' => array(30,9),
        'field_org_viplata_otchet_value' => array(10,10),
            'field_org_viplata_otchet_komp_value' => array(39,10),
            'field_org_viplata_otchet_snizh_value' => array(40, 10),
            'field_org_viplata_otchet_nedopos_value' => array(41,10),
        'field_org_viplata_rso_value' => array(11,11),
            'field_org_viplata_rso_otop_value' => array(31,11),
            'field_org_viplata_rso_elekt_value' => array(32,11),
            'field_org_viplata_rso_gaz_value' => array(33,11),
            'field_org_viplata_rso_gor_vod_value' => array(34,11),
            'field_org_viplata_rso_hol_vod_value' => array(35,11),
            'field_org_viplata_rso_vodotv_value' => array(36,11),
        'field_org_trust_value' => array(12,12),
            'field_org_trust_25_value' => array(26,12),
            'field_org_trust_50_value' => array(27,12),
            'field_org_trust_75_value' => array(28,12),
            'field_org_trust_over_75_value' => array(29,12),
            'field_org_trust_emerg_value' => array(30,12),
        'field_org_trust_ku_value' => array(13,13),
            'field_org_trust_ku_otoplenie_value' => array(31,13),
            'field_org_trust_ku_eletrichestvo_value' => array(32,13),
            'field_org_trust_ku_gaz_value' => array(33,13),
            'field_org_trust_ku_hot_voda_value' => array(34,13),
            'field_org_trust_ku_cold_voda_value' => array(35,13),
            'field_org_trust_ku_wastewater_value' => array(36,13),
        'field_org_trust_uo_of_date_value' => array(14,14),
            'field_org_trust_uo_of_date_otop_value' => array(31,14),
            'field_org_trust_uo_of_date_elekr_value' => array(32,14),
            'field_org_trust_uo_of_date_gaz_value' => array(33,14),
            'field_org_trust_uo_of_date_gor_value' => array(34,14),
            'field_org_trust_uo_of_date_holod_value' => array(35,14),
            'field_org_trust_uo_of_date_otvod_value' => array(36,14),
        'field_org_charged_upr_period_value' => array(15,15),
            'field_org_charged_upr_period_25_value' => array(26,15),
            'field_org_charged_upr_period_50_value' => array(27,15),
            'field_org_charged_upr_period_75_value' => array(28,15),
            'field_org_charged_upr_period_o75_value' => array(29,15),
            'field_org_charged_upr_period_cra_value' => array(30,15),
        'field_org_charged_period_value' => array(16,16),
            'field_org_charged_period_otop_value' => array(31,16),
            'field_org_charged_period_elektr_value' => array(32,16),
            'field_org_charged_period_gaz_value' => array(33,16),
            'field_org_charged_period_gor_value' => array(34,16),
            'field_org_charged_period_holod_value' => array(35,16),
            'field_org_charged_period_otvod_value' => array(36,16),
        'field_org_repair_period_value' => array(17,17),
            'field_org_repair_period_25_value' => array(26,17),
            'field_org_repair_period_50_value' => array(27,17),
            'field_org_repair_period_75_value' => array(28,17),
            'field_org_repair_period_over_75_value' => array(29,17),
            'field_org_repair_period_crached_value' => array(30,17),
        'field_org_blag_repair_period_value' => array(18,18),
            'field_org_blag_repair_period_25_value' => array(26,18),
            'field_org_blag_repair_period_50_value' => array(27,18),
            'field_org_blag_repair_period_75_value' => array(28,18),
            'field_org_blag_repair_period_o75_value' => array(29,18),
            'field_org_blag_repair_period_cr_value' => array(30,18),
        'field_org_money_period_value' => array(19,19),
            'field_org_money_period_subsidy_value' => array(42,19),
            'field_org_money_period_credits_value' => array(43,19),
            'field_org_money_period_lising_value' => array(44,19),
            'field_org_money_period_service_value' => array(45,19),
            'field_org_money_period_vznos_value' => array(46,19),
            'field_org_money_period_other_value' => array(47,19),
        'field_org_paid_ku_pu_period_value' => array(20,20),
            'field_org_paid_ku_pu_period_otop_value' => array(31,20),
            'field_org_paid_ku_pu_period_elek_value' => array(32,20),
            'field_org_paid_ku_pu_period_gaz_value' => array(33,20),
            'field_org_paid_ku_pu_period_gor_value' => array(34,20),
            'field_org_paid_ku_pu_period_hol_value' => array(35,20),
        'field_org_paid_nujd_period_value' => array(21,21),
            'field_org_paid_nujd_period_otop_value' => array(31,21),
            'field_org_paid_nujd_period_elekt_value' => array(32,21),
            'field_org_paid_nujd_period_gaz_value' => array(33,21),
            'field_org_paid_nujd_gor_value' => array(34,21),
            'field_org_paid_nujd_holod_value' => array(35,21),
        'field_org_count_residents_value' => array(48,22),
        'field_org_count_dissmiss_value' => array(49,23),
            'field_org_count_dissmiss_adm_value' => array(50,23),
            'field_org_count_dissmis_engineer_value' => array(51,23),
            'field_org_count_dissmiss_work_value' => array(52,23),
        );

    protected $_aliases = array(
        'count_houses_under_mng_report_date' => 'Домов под управлением на отчетную дату',
        'count_houses_under_mng_start_period' => 'Домов под управлением на начало периода',
        'sum_sq_houses_under_mng_report_date' => 'Площадь домов под управлением на отчетную дату, тыс.кв.м.',
        'sum_sq_houses_under_mng_start_period' => 'Площадь домов под управлением на начало периода, тыс.кв.м.',
        'avg_time_service_mkd' => 'Средний срок обслуживания МКД, лет.',
        'income_of_mng' => 'Доходы, полученные за оказание услуг по управлению многоквартирными домами, тыс.руб.',
        'income_of_usage' => 'Доход, полученный от использования общего имущества за отчетный период, тыс.руб.',
        'income_of_ku' => 'Доход от предоставления КУ за отчетный период, тыс.руб.',
        'spending_of_mng' => 'Расходы, полученные в связи с оказанием услуг по управлению многоквартирными домами, тыс.руб.',
        'claims_by_contracts_mng' => 'Выплата по искам по договорам управления за отчетный период, тыс.руб.',
        'claims_by_rso' => 'Выплата по искам РСО за отчетный период, тыс.руб.',
        'debt_for_mng' => 'Задолженность собственников за услуги управления на отчетную дату, тыс.руб.',
        'debt_owners_for_ku' => 'Задолженность собственников за КУ на отчетную дату, тыс.руб.',
        'debt_uo_for_ku' => 'Задолженность УО за КУ на отчетную дату, тыс.руб.',
        'charged_for_mng' => 'Взыскано с собственников за услуги управления за отчетный период, тыс.руб.',
        'charged_for_resources' => 'Взыскано с собственников за поставленные ресурсы за отчетный период, тыс.руб.',
        'spending_repair' => 'Объем средств, затраченных на работы по текущему ремонту за отчетный период, тыс.руб. ',
        'spending_beaty' => 'Объем средств, затраченных на работы по благоустройству за отчетный период, тыс.руб.',
        'spending_repair_invests' => 'Объем привлеченных средств на ремонт, модернизацию и благоустройство за отчетный период, тыс.руб.',
        'payed_ku_by_statements' => 'Оплачено КУ по показаниям общедомовых ПУ за отчетный период, тыс.руб.',
        'payed_ku_by_needs' => 'Оплачено КУ по счетам на общедомовые нужды за отчетный период, тыс.руб.',
        'serviced_by_tsg' => 'Обслуживаемых ТСЖ',
        'serviced_by_tsg_uo' => 'обслуживаемых по договору между ТСЖ и управляющей организацией',
        'serviced_by_owner_uo' => 'обслуживаемых по договору между собственниками и управляющей организацией',
        'serviced_by_competition' => 'обслуживаемых по результатам открытого конкурса органов местного самоуправления',
        'by_houses_25' => 'по домам до 25 лет',
        'by_houses_26_50' => 'по домам от 26 до 50 лет',
        'by_houses_51_75' => 'по домам от 51 до 75 лет',
        'by_houses_76' => 'по домам 76 лет и более',
        'by_houses_alarm' => 'по аварийным домам',
        'by_heating' => 'отопление',
        'by_electro' => 'электричество',
        'by_gaz' => 'газ',
        'by_hot_water' => 'горячее водоснабжение',
        'by_cold_water' => 'холодное водоснабжение',
        'by_sewerage' => 'водоотведение',
        'sum_sq_houses_concluded_contracts' => 'Изменение общей площади домов за отчетный период по заключенным договорам, тыс.кв.м.',
        'sum_sq_houses_terminated_contracts' => 'Изменение общей площади домов за отчетный период по расторгнутым договорам, тыс.кв.м.',
        'claims_for_damage' => 'иски по компенсации нанесенного ущерба',
        'claims_for_refusal' => 'иски по снижению платы в связи с неоказанием услуг',
        'claims_for_short_shipment' => 'иски по снижению платы в связи с недопоставкой ресурсов',
        'subsidy' => 'Субсидии',
        'credits' => 'Кредиты',
        'fin_lising' => 'Финансирование по договорам лизинга',
        'fin_service' => 'Целевые взносы жителей',
        'contributions_residents' => 'Целевые взносы жителей',
        'other_sources' => 'Другие источники'
    );
}
