<?php

namespace Migration\Mapper;

use Migration\Utils\SqlLogger;

class ImportFiasMapper extends AbstractMapper
{
    protected $_codes = array();
    public $updateRevisions = array();
   // protected $_logging = false;

    public function clearDataFromTblFias_rgkh2()
    {
        $this->togglePgTblAllTriggers('fias_addrobj', 'disable');
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "DELETE FROM fias_addrobj"
        );
        $this->togglePgTblAllTriggers('fias_addrobj', 'enable');
    }

    public function dropFiasIndexes()
    {
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            'DROP INDEX "fias_addrobj_AISID_INDEX"'
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            'DROP INDEX "fias_addrobj_AOGUID_INDEX"'
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            'DROP INDEX "fias_addrobj_AOLEVEL_INDEX"'
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            'DROP INDEX "fias_addrobj_CODE_INDEX"'
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            'DROP INDEX "fias_addrobj_PLAINCODE_INDEX"'
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            'DROP INDEX "fias_addrobj_PARENTGUID_INDEX"'
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            'DROP INDEX "fias_addrobj_OFFNAME_INDEX"'
        );
    }

    public function addFiasIndexes()
    {
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            'CREATE INDEX "fias_addrobj_AISID_INDEX" ON fias_addrobj USING btree ("aisid");'
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            'CREATE INDEX "fias_addrobj_AOGUID_INDEX" ON fias_addrobj USING btree ("aoguid");'
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            'CREATE INDEX "fias_addrobj_AOLEVEL_INDEX" ON fias_addrobj USING btree ("aolevel");'
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            'CREATE INDEX "fias_addrobj_CODE_INDEX" ON fias_addrobj USING btree ("code");'
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            'CREATE INDEX "fias_addrobj_PLAINCODE_INDEX" ON fias_addrobj USING btree ("plaincode");'
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            'CREATE INDEX "fias_addrobj_PARENTGUID_INDEX" ON fias_addrobj USING btree ("parentguid");'
        );
        $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            'CREATE INDEX "fias_addrobj_OFFNAME_INDEX" ON fias_addrobj USING btree ("offname");'
        );
    }

    public function selectCodesFromKladr_rgkh1()
    {
        echo "Regions...";
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT ct.field_kladr_region_kladr_value as code, ct.field_kladr_region_ais_id_value as ais_id FROM node n INNER JOIN content_type_kladr_region ct ON ct.vid = n.vid WHERE n.type ='kladr_region' AND ct.field_kladr_region_archaic_value is null AND ct.field_kladr_region_ais_id_value is not null"
        );
        foreach($resultData as $item){
            $this->_codes['region'][substr($item['code'], 0, strlen($item['code'])-2)] = $item['ais_id'];
        }
        unset($resultData);
        echo "Areas...";
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT ct.field_kladr_raion_kladr_value as code, ct.field_kladr_raion_ais_id_value as ais_id FROM node n INNER JOIN content_type_kladr_raion ct ON ct.vid = n.vid WHERE n.type ='kladr_raion' AND ct.field_kladr_raion_archaic_value is null AND ct.field_kladr_raion_ais_id_value is not null"
        );
        foreach($resultData as $item){
            $this->_codes['area'][substr($item['code'], 0, strlen($item['code'])-2)] = $item['ais_id'];
        }
        unset($resultData);
        echo "Cities...";
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT ct.field_kladr_naspunkt_kladr_value as code, ct.field_kladr_naspunkt_ais_id_value as ais_id FROM node n INNER JOIN content_type_kladr_naspunkt ct ON ct.vid = n.vid WHERE n.type ='kladr_naspunkt' AND ct.field_kladr_naspunkt_archaic_value is null AND ct.field_kladr_naspunkt_ais_id_value is not null"
        );
        foreach($resultData as $item){
            $this->_codes['city'][substr($item['code'], 0, strlen($item['code'])-2)] = $item['ais_id'];
        }
        unset($resultData);
        echo "Streets...";
        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(),
            "SELECT ct.field_kladr_street_kladr_value as code, ct.field_kladr_street_ais_id_value as ais_id FROM node n INNER JOIN content_type_kladr_street ct ON ct.vid = n.vid WHERE n.type ='kladr_street' AND ct.field_kladr_street_archaic_value is null AND ct.field_kladr_street_ais_id_value is not null"
        );
        foreach($resultData as $item){
            $this->_codes['street'][substr($item['code'], 0, strlen($item['code'])-2)] = $item['ais_id'];
        }
        unset($resultData);
        echo "\n";
    }

    public function insertDataIntoFiasTbl($data, $force = false)
    {
        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('fias_addrobj', 'disable');

        $sql = 'INSERT INTO fias_addrobj ( "aoid", "aoguid", "parentguid", "aolevel", "formalname", "shortname", "offname", "regioncode", "areacode", "autocode", "citycode", "ctarcode", "placecode", "streetcode", "extrcode", "sextcode",
            "postalcode", "ifnsfl", "terrifnsfl", "ifnsul", "terrifnsul", "okato", "oktmo", "previd", "nextid", "code", "plaincode", "actstatus", "livestatus", "centstatus",
            "operstatus", "currstatus", "startdate", "enddate", "updatedate", "aisid"
        ) VALUES';
        $sql_values = array();


        foreach ($data as &$row) {
            $row_l = array();
            foreach($row as $row_key => $row_value) {
                $row_l[strtoupper($row_key)] = $row_value;
            }

            // записи, которые мы пропускаем
            if (!$force && empty($row_l['CODE'])) {
                continue;
            }

            $row_l["FORMALNAME"] = $this->escape_string($row_l["FORMALNAME"]);
            $row_l["SHORTNAME"] = $this->escape_string($row_l["SHORTNAME"]);
            $row_l["OFFNAME"] = $this->escape_string($row_l["OFFNAME"]);

            $pc = "".$row_l['PLAINCODE'];

            if (!$row_l['AISID_']){
                $level = 'region';
                if (in_array($row_l['AOLEVEL'],array(1,2)))
                {
                    $level = 'region';
                } else if (in_array($row_l['AOLEVEL'],array(3))) {
                    $level = 'area';
                } else if (in_array($row_l['AOLEVEL'],array(4,5,6))) {
                    $level = 'city';
                } else if (in_array($row_l['AOLEVEL'],array(7,90,91))) {
                    $level = 'street';
                }


                $aisid = !empty($this->_codes[$level][$pc]) ? $this->_codes[$level][$pc] : "null";
            } else {
                $aisid = $row_l['AISID_'];
            }

            //@TODO:
            //$row_l['NORMDOC'] = 'NULL';


            $sql_values[] = "('".(!isset($row_l['AOID_NEW'])?$row_l['AOID']:$row_l['AOID_NEW'])."','".$row_l['AOGUID']."', '".$row_l['PARENTGUID']."', '".$row_l['AOLEVEL']."', '".$row_l['FORMALNAME']."', '".$row_l['SHORTNAME']."', '".$row_l['OFFNAME']."'
                            , '".$row_l['REGIONCODE']."', '".$row_l['AREACODE']."', '".$row_l['AUTOCODE']."', '".$row_l['CITYCODE']."', '".$row_l['CTARCODE']."', '".$row_l['PLACECODE']."'
                            , '".$row_l['STREETCODE']."', '".$row_l['EXTRCODE']."', '".$row_l['SEXTCODE']."', '".$row_l['POSTALCODE']."', '".$row_l['IFNSFL']."', '".$row_l['TERRIFNSFL']."'
                            , '".$row_l['IFNSUL']."', '".$row_l['TERRIFNSUL']."', '".$row_l['OKATO']."', '".$row_l['OKTMO']."', '".$row_l['PREVID']."', '".$row_l['NEXTID']."', '".$row_l['CODE']."'
                            , '".$row_l['PLAINCODE']."', '".$row_l['ACTSTATUS']."', '".$row_l['LIVESTATUS']."', '".$row_l['CENTSTATUS']."', '".$row_l['OPERSTATUS']."', '".$row_l['CURRSTATUS']."', '".$row_l['STARTDATE']."'
                            , '".$row_l['ENDDATE']."', '".$row_l['UPDATEDATE']."', ".$aisid.")";
            unset($row);
            unset($row_l);
        }

        $result = $this->insertByParts($this->getPgSqlEntityManager(), $sql, $sql_values, count($data), false);

        unset($data);
        unset($sql_values);

        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('fias_addrobj', 'enable');

        return $result;
    }

    public function getShortName($off_field, $short_field)
    {
        $short_field = str_replace($off_field, "EKWORD",$short_field);

        try {
            preg_match('/([^\s]+)\s(\bEKWORD\b)/', $short_field, $matches);
        } catch (\Exception $e) {
            $logger = new SqlLogger();
            $logger->loggingQueryError($e->getMessage());
        }
        return str_replace('.','',$matches[1]);
    }

    private function GUID()
    {
        if (function_exists('com_create_guid') === true)
        {
            return trim(com_create_guid(), '{}');
        }

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    private function generateaoguid( $valLength )
    {
     /*   $result = '';
        $moduleLength = 40;   // we use sha1, so module is 40 chars
        $steps = round($valLength/$moduleLength + 0.5);

        for( $i=0; $i<$steps; $i++ ) {
          $result .= sha1( uniqid() . md5( rand() . uniqid() ) );
        }

        return substr( $result, 0, $valLength );*/
        return $this->GUID();
    }

    private function getFiasAOLevelsByPart($part)
    {
        switch ($part)
        {
            case "Regions":
                $levels = array("'1'","'2'");
                break;
            case "Areas":
                $levels = array("'3'");
                break;
            case "Cities":
                $levels = array("'4'","'5'","'6'");
                break;
            case "Streets":
                $levels = array("'7'","'90'","'91'");
                break;
        }
        return $levels;
    }

    private function selectGuidByCode_rgkh2($code, $level = null){
        if ($code=='') return null;

        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "SELECT \"aoguid\" FROM fias_addrobj WHERE ".(!empty($level)?"\"aolevel\" IN (".implode(',',$level).") AND ":"")."\"code\" = '{$code}'"
        );

        if (empty($resultData))
        {
            return null;
        }
        return $resultData[0]['aoguid'];
    }

    /** ------------------------------------------------------ sync -------------------------------------------------------- */
    /**
     * select data from rgkh1.0 kladr tables
     * @param $part - Regions, Areas, Cities, Steets
     * @return array|int|null
     */
    public function selectKladrData_rgkh1($part)
    {
        switch ($part){
            case "Regions":
                $fields = array('code' => 'ct.field_kladr_region_kladr_value',
                                'aisid' => 'ct.field_kladr_region_ais_id_value',
                                'formalname' => 'ct.field_kladr_region_name_value',
                                'shortname' => 'ct.field_kladr_region_short_name_value',
                                'offname' => 'ct.field_kladr_region_name_value',
                          );
                $join = ' INNER JOIN content_type_kladr_region ct ON ct.vid = n.vid ';

                $where = "n.type = 'kladr_region' AND  ct.field_kladr_region_archaic_value is null AND ct.field_kladr_region_ais_id_value is not null";
                break;
            case "Areas":
                $fields = array('code' => 'ct.field_kladr_raion_kladr_value',
                                'aisid' => 'ct.field_kladr_raion_ais_id_value',
                                'formalname' => 'ct.field_kladr_raion_name_value',
                                'shortname' => 'ct.field_kladr_raion_short_name_value',
                                'offname' => 'ct.field_kladr_raion_name_value',
                );
                $join = ' INNER JOIN content_type_kladr_raion ct ON ct.vid = n.vid ';

                $where = "n.type = 'kladr_raion' AND ct.field_kladr_raion_archaic_value is null AND ct.field_kladr_raion_ais_id_value is not null";
                break;
            case "Cities":
                $fields = array('code' => 'ct.field_kladr_naspunkt_kladr_value',
                                'aisid' => 'ct.field_kladr_naspunkt_ais_id_value',
                                'formalname' => 'ct.field_kladr_naspunkt_name_value',
                                'shortname' => 'ct.field_kladr_naspunkt_short_name_value',
                                'offname' => 'ct.field_kladr_naspunkt_name_value',
                );
                $join = ' INNER JOIN content_type_kladr_naspunkt ct ON ct.vid = n.vid ';

                $where = "n.type = 'kladr_naspunkt' AND ct.field_kladr_naspunkt_archaic_value is null AND ct.field_kladr_naspunkt_ais_id_value is not null";
                break;
            case "Streets":
                $fields = array('code' => 'ct.field_kladr_street_kladr_value',
                                'aisid' => 'ct.field_kladr_street_ais_id_value',
                                'formalname' => 'ct.field_kladr_street_name_value',
                                'shortname' => 'ct.field_kladr_street_short_name_value',
                                'offname' => 'ct.field_kladr_street_name_value'
                );
                $join = ' INNER JOIN content_type_kladr_street ct ON ct.vid = n.vid ';

                $where = "n.type = 'kladr_street' AND ct.field_kladr_street_archaic_value is null AND ct.field_kladr_street_ais_id_value is not null";
                break;
        }

        $query = "SELECT ";

        $i = 0;
        foreach($fields as $alias => $field) {
            $query .= "{$field} as {$alias}";
            $i++;
            if ($i<count($fields)) $query .=  ',';
        }

        $query .= " FROM node n ".$join." WHERE ".$where;

        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(), $query);

        return $resultData;
    }

    /**
     * select 1 row data by ais_id from rgkh1.0 kladr tables
     * @param $part - Regions, Areas, Cities, Steets
     * @param $ais_id
     * @return array|int|null
     */
    public function selectKladrRow_rgkh1($part, $ais_id)
    {
        switch ($part){
            case "Regions":
                $fields = array('code' => 'ct.field_kladr_region_kladr_value',
                                'aisid' => 'ct.field_kladr_region_ais_id_value',
                                'formalname' => 'ct.field_kladr_region_name_value',
                                'shortname' => 'ct.field_kladr_region_short_name_value',
                                'offname' => 'ct.field_kladr_region_name_value',
                                'regioncode' => 'ct.field_kladr_region_code_value',
                                'postalcode' => 'ct.field_kladr_region_zip_value',
                                'okato' => 'ct.field_kladr_region_okato_value',
                                'actstatus' => 'ct.field_kladr_region_archaic_value'
                );
                $join = ' INNER JOIN content_type_kladr_region ct ON ct.vid = n.vid ';

                $where = "n.type = 'kladr_region' AND  ct.field_kladr_region_archaic_value is null AND ct.field_kladr_region_ais_id_value = $ais_id";
                break;
            case "Areas":
                $fields = array('code' => 'ct.field_kladr_raion_kladr_value',
                                'aisid' => 'ct.field_kladr_raion_ais_id_value',
                                'formalname' => 'ct.field_kladr_raion_name_value',
                                'shortname' => 'ct.field_kladr_raion_short_name_value',
                                'offname' => 'ct.field_kladr_raion_name_value',
                                'postalcode' => 'ct.field_kladr_raion_zip_value',
                                'okato' => 'ct.field_kladr_raion_okato_value',
                                'actstatus' => 'ct.field_kladr_raion_archaic_value',
                                'PARENTcode' => 'ctp.field_kladr_region_kladr_value'
                );
                $join = ' INNER JOIN content_type_kladr_raion ct ON ct.vid = n.vid LEFT JOIN content_type_kladr_region ctp ON ctp.field_kladr_region_ais_id_value = ct.field_kladr_raion_region_value ';

                $where = "n.type = 'kladr_raion' AND ct.field_kladr_raion_archaic_value is null AND ct.field_kladr_raion_ais_id_value = $ais_id";
                break;
            case "Cities":
                $fields = array('code' => 'ct.field_kladr_naspunkt_kladr_value',
                                'aisid' => 'ct.field_kladr_naspunkt_ais_id_value',
                                'formalname' => 'ct.field_kladr_naspunkt_name_value',
                                'shortname' => 'ct.field_kladr_naspunkt_short_name_value',
                                'offname' => 'ct.field_kladr_naspunkt_name_value',
                                'postalcode' => 'ct.field_kladr_naspunkt_zip_value',
                                'okato' => 'ct.field_kladr_naspunkt_region_value',
                                'actstatus' => 'ct.field_kladr_naspunkt_archaic_value',
                                'MAINCITY' => 'ct.field_kladr_naspunkt_main_city_value',
                                'PARENTcode_A' => 'ctp_a.field_kladr_raion_kladr_value',
                                'PARENTcode_R' => 'ctp_r.field_kladr_region_kladr_value',
                );
                $join = ' INNER JOIN content_type_kladr_naspunkt ct ON ct.vid = n.vid LEFT JOIN content_type_kladr_raion ctp_a ON ctp_a.field_kladr_raion_ais_id_value = ct.field_kladr_naspunkt_region_area_value LEFT JOIN content_type_kladr_region ctp_r ON ctp_r.field_kladr_region_ais_id_value = ct.field_kladr_naspunkt_region_gni_value ';

                $where = "n.type = 'kladr_naspunkt' AND ct.field_kladr_naspunkt_archaic_value is null AND ct.field_kladr_naspunkt_ais_id_value = $ais_id";
                break;
            case "Streets":
                $fields = array('code' => 'ct.field_kladr_street_kladr_value',
                                'aisid' => 'ct.field_kladr_street_ais_id_value',
                                'formalname' => 'ct.field_kladr_street_name_value',
                                'shortname' => 'ct.field_kladr_street_short_name_value',
                                'offname' => 'ct.field_kladr_street_name_value',
                                'postalcode' => 'ct.field_kladr_street_zip_value',
                                'okato' => 'ct.field_kladr_street_okato_value',
                                'actstatus' => 'ct.field_kladr_street_archaic_value',
                                'PARENTcode' => 'ctp.field_kladr_naspunkt_kladr_value',
                );
                $join = ' INNER JOIN content_type_kladr_street ct ON ct.vid = n.vid LEFT JOIN content_type_kladr_naspunkt ctp ON ctp.field_kladr_naspunkt_ais_id_value = ct.field_kladr_street_city_value ';

                $where = "n.type = 'kladr_street' AND ct.field_kladr_street_archaic_value is null AND ct.field_kladr_street_ais_id_value = $ais_id";
                break;
        }

        $query = "SELECT ";

        $i = 0;
        foreach($fields as $alias => $field) {
            $query .= "{$field} as {$alias}";
            $i++;
            if ($i<count($fields)) $query .=  ',';
        }

        $query .= " FROM node n ".$join." WHERE ".$where . " LIMIT 1";

        $resultData = $this->executeQueryWithLog($this->getMySqlEntityManager(), $query);

        if (empty($resultData)) return null;
        return $resultData[0];
    }

    /**
     * Select actuals from rgkh2.0 fias tbl
     * @param $part - Regions, Areas, Cities, Streets
     * @return array fias rows
     */
    public function selectRowsFromTblFias_rgkh2($part)
    {
        $levels = $this->getFiasAOLevelsByPart($part);

        $data = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "SELECT \"aoid\",\"shortname\",\"offname\",\"aisid\",\"code\",\"plaincode\" FROM fias_addrobj WHERE  \"aolevel\" in (".implode(',',$levels).") AND \"actstatus\" = '1' AND \"livestatus\" = '1'"   // select actual revision by plaincode
        );
        foreach ($data as $i=>$item) {
            $resultData[$item['plaincode']] = $item;
            unset($item);
            unset($data[$i]);
        }
        unset($data);

        return $resultData;
    }

    /**
     * Select 1 row from rgkh2.0 fias tbl compared kladr row
     * @param $aoid
     * @return fias row
     */
    public function selectRowFromTblFias_rgkh2($aoid)
    {
        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "SELECT * FROM fias_addrobj WHERE  \"aoid\" = '{$aoid}'"   // select actual revision by plaincode
        );

        if (empty($resultData)) return null;
        return $resultData[0];
    }

    /**
     * update ais_id in fias row
     * @param $part - Regions, Areas, Cities, Streets
     * @param $row fias row
     * @return array|int|null   count affected rows
     */
    public function updateAisIdInTblFias_rgkh2($part, $kladrRow, $fiasRow)
    {
        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('fias_addrobj', 'disable');

        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            "UPDATE fias_addrobj SET \"aisid\" = ".$kladrRow['aisid']." WHERE \"aoid\" = '".$fiasRow['aoid']."'",  // update ais_id
            true
        );

        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('fias_addrobj', 'enable');

        return $resultData;
    }

    /**
     * insert a new fias row revision based on kladr row
     * @param $part - Regions, Areas, Cities, Streets
     * @param $fiasRow fias row
     * @param $kladrRow kladr row
     * @return int inserted rows
     */
    public function insertRevisionIntoTblFias_rgkh2($part, $fiasRow, $kladrRow, $changed)
    {


        // add new revision
        // fields from old revision
        $fiasRow = $this->selectRowFromTblFias_rgkh2($fiasRow['aoid']);
        $fiasRow['code'] = $kladrRow['code'];
        $fiasRow['plaincode'] = substr($kladrRow['code'],0,strlen($kladrRow['code'])-2);
        $fiasRow['currstatus'] = substr($kladrRow['code'],strlen($kladrRow['code'])-2,2);
        $fiasRow["formalname"] = iconv('windows-1251','utf-8',$this->escape_string($kladrRow["formalname"]));
        $fiasRow["shortname"] = iconv('windows-1251','utf-8',$this->escape_string($kladrRow["shortname"]));
        $fiasRow["offname"] = iconv('windows-1251','utf-8',$this->escape_string($kladrRow["offname"]));
        $fiasRow['updatedate'] = date('Y-m-d');

        switch ($changed){
            case "name":
                $fiasRow['operstatus'] = self::OPERSTATUS_NEWREVAO_NAME;
                break;
            case "currcod":
                $fiasRow['operstatus'] = self::OPERSTATUS_NEWREVAO_CURRCODE;
                break;
        }

        $fiasRow['previd'] = $fiasRow['aoid'];

        $fiasRow['aoid_new'] = $this->generateaoguid(16);
        $fiasRow['aisid_'] = $fiasRow['aisid'];


        // disable current revision
        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('fias_addrobj', 'disable');

        $this->updateRevisions[] = "('".$fiasRow['aoid_new']."','".self::OPERSTATUS_OLDREVAO."', '".$fiasRow['aoid']."')";

        $this->insertDataIntoFiasTbl(array($fiasRow), true);

        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('fias_addrobj', 'enable');

        return $fiasRow;   // rows insert affected
    }

    /**
     * insert a new fias row
     * @param $part - Regions, Areas, Cities, Streets
     * @param $row fias row
     * @return array (inserted rows, parents count)
     */
    public function insertNewRowIntoTblFias_rgkh2($part, $row)
    {
        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('fias_addrobj', 'disable');

        // not mapped fields
        $row['aoid'] = $this->generateaoguid(16);
        if (empty($row['aoguid']))
            $row['aoguid'] = $this->generateaoguid(16); // autogenerate aoguid 16 bytes
        $row['plaincode'] = substr($row['code'],0,strlen($row['code'])-2);
        $row['operstatus'] = self::OPERSTATUS_NEWAO;
        $row['currstatus'] = substr($row['code'],strlen($row['code'])-2,2);
        $row['updatedate'] = $row['startdate'] = date('Y-m-d');
        $row["formalname"] = $this->escape_string($row["formalname"]);
        $row["shortname"] = $this->escape_string($row["shortname"]);
        $row["offname"] = $this->escape_string($row["offname"]);
        $row['livestatus'] = $row['actstatus'] = !empty($row['actstatus']) ? '0' : '1';

        switch ($part){
            case "Regions":
                $row['aolevel'] = '1';
                $parentFound = 0;
                $values = array('aoid' => "'{$row['aoid']}'", 'aoguid' => "'{$row['aoguid']}'", 'aisid' => "{$row['aisid']}", 'aolevel' => "'{$row['aolevel']}'", 'formalname' => "'{$row['formalname']}'", 'shortname' => "'{$row['shortname']}'", 'offname' => "'{$row['offname']}'",
                    'regioncode' => "'{$row['regioncode']}'", 'postalcode' => "'{$row['postalcode']}'", 'okato' => "'{$row['okato']}'", 'code' => "'{$row['code']}'", 'plaincode' => "'{$row['plaincode']}'", 'actstatus' => "'{$row['actstatus']}'", 'livestatus' => "'{$row['livestatus']}'", 'operstatus' => "'{$row['operstatus']}'",
                    'currstatus' => "'{$row['currstatus']}'", 'startdate' => "'{$row['startdate']}'", 'updatedate' => "'{$row['updatedate']}'"
                );
                break;
            case "Areas":
                $row['aolevel'] = '3';
                $row['parentguid'] = $this->selectGuidByCode_rgkh2($row['PARENTcode'], $this->getFiasAOLevelsByPart('Regions'));
                $parentFound = !empty($row['parentguid']) ? 1 : 0;
                $row['regioncode'] = substr($row['code'],0,2);
                $row['areacode'] = substr($row['code'],2,3);
                $values = array('aoid' => "'{$row['aoid']}'", 'aoguid' => "'{$row['aoguid']}'", 'aisid' => "{$row['aisid']}", 'parentguid' => "'{$row['parentguid']}'", 'aolevel' => "'{$row['aolevel']}'", 'formalname' => "'{$row['formalname']}'", 'shortname' => "'{$row['shortname']}'", 'offname' => "'{$row['offname']}'",
                    'regioncode' => "'{$row['regioncode']}'", 'areacode' => "'{$row['areacode']}'", 'postalcode' => "'{$row['postalcode']}'", 'okato' => "'{$row['okato']}'", 'code' => "'{$row['code']}'", 'plaincode' => "'{$row['plaincode']}'", 'actstatus' => "'{$row['actstatus']}'", 'livestatus' => "'{$row['livestatus']}'", 'operstatus' => "'{$row['operstatus']}'",
                    'currstatus' => "'{$row['currstatus']}'", 'startdate' => "'{$row['startdate']}'", 'updatedate' => "'{$row['updatedate']}'"
                );
                break;
            case "Cities":
                if (!empty($row['PARENTcode_A']))   // привязано к району
                {
                    $row['parentguid'] = $this->selectGuidByCode_rgkh2($row['PARENTcode_A'], $this->getFiasAOLevelsByPart('Areas'));
                }
                else if (!empty($row['PARENTcode_R']))   // привязано к региону
                {
                    $row['parentguid'] = $this->selectGuidByCode_rgkh2($row['PARENTcode_R'], $this->getFiasAOLevelsByPart('Regions'));
                }
                $parentFound = !empty($row['parentguid']) ? 1 : 0;

                $row['regioncode'] = substr($row['code'],0,2);
                $row['areacode'] = substr($row['code'],2,3);
                $row['citycode'] = substr($row['code'],5,3);
                $row['placecode'] = substr($row['placecode'],8,3);

                $row['aolevel'] = $row['citycode'] != '000' ? '4' : '6';

                $values = array('aoid' => "'{$row['aoid']}'", 'aoguid' => "'{$row['aoguid']}'", 'aisid' => "{$row['aisid']}", 'parentguid' => "'{$row['parentguid']}'", 'aolevel' => "'{$row['aolevel']}'", 'formalname' => "'{$row['formalname']}'", 'shortname' => "'{$row['shortname']}'", 'offname' => "'{$row['offname']}'",
                    'regioncode' => "'{$row['regioncode']}'", 'areacode' => "'{$row['areacode']}'", 'citycode' => "'{$row['citycode']}'", 'placecode' => "'{$row['placecode']}'",'postalcode' => "'{$row['postalcode']}'", 'okato' => "'{$row['okato']}'", 'code' => "'{$row['code']}'", 'plaincode' => "'{$row['plaincode']}'", 'actstatus' => "'{$row['actstatus']}'", 'livestatus' => "'{$row['livestatus']}'", 'operstatus' => "'{$row['operstatus']}'",
                    'currstatus' => "'{$row['currstatus']}'", 'startdate' => "'{$row['startdate']}'", 'updatedate' => "'{$row['updatedate']}'"
                );
                break;
            case "Streets":
                $row['aolevel'] = '7';  // @TODO: ???? как определить 7,90,91?
                $row['parentguid'] = $this->selectGuidByCode_rgkh2($row['PARENTcode'], $this->getFiasAOLevelsByPart('Cities'));
                $parentFound = !empty($row['parentguid']) ? 1 : 0;
                $row['regioncode'] = substr($row['code'],0,2);
                $row['areacode'] = substr($row['code'],2,3);
                $row['citycode'] = substr($row['code'],5,3);
                $row['placecode'] = substr($row['placecode'],8,3);
                $row['streetcode'] = substr($row['code'],11,4);
                $values = array('aoid' => "'{$row['aoid']}'", 'aoguid' => "'{$row['aoguid']}'", 'aisid' => "{$row['aisid']}", 'parentguid' => "'{$row['parentguid']}'", 'aolevel' => "'{$row['aolevel']}'", 'formalname' => "'{$row['formalname']}'", 'shortname' => "'{$row['shortname']}'", 'offname' => "'{$row['offname']}'",
                    'regioncode' => "'{$row['regioncode']}'", 'areacode' => "'{$row['areacode']}'", 'citycode' => "'{$row['citycode']}'", 'placecode' => "'{$row['placecode']}'", 'streetcode' => "'{$row['streetcode']}'", 'postalcode' => "'{$row['postalcode']}'", 'okato' => "'{$row['okato']}'", 'code' => "'{$row['code']}'", 'plaincode' => "'{$row['plaincode']}'", 'actstatus' => "'{$row['actstatus']}'", 'livestatus' => "'{$row['livestatus']}'", 'operstatus' => "'{$row['operstatus']}'",
                    'currstatus' => "'{$row['currstatus']}'", 'startdate' => "'{$row['startdate']}'", 'updatedate' => "'{$row['updatedate']}'"
                );
                break;
        }

        $query = 'INSERT INTO fias_addrobj (';
        foreach (array_keys($values) as $i => $column)
        {
            $query .= '"'.strtolower($column).'"';
            if ($i < count($values)-1) $query .= ',';
        }
        $query .= ') VALUES ('.implode(',', $values).')';

        unset($values);

        $resultData = $this->executeQueryWithLog($this->getPgSqlEntityManager(),
            $query,
            true
        );

        if ($this->_forceMode)
            $this->togglePgTblAllTriggers('fias_addrobj', 'enable');

        return array($resultData, $parentFound);
    }

    public function prepareArchiveAOs_rgkh1()
    {
        $ao_vids = $this->executeQueryWithLog($this->getMysqlEntityManager(),
        "select s.vid from content_type_tp tp join node n on n.vid = tp.vid join content_type_kladr_street s on s.nid = tp.field_tp_kladr_street_nid where s.field_kladr_street_archaic_value = 1 and (field_kladr_street_new_table_value is null or field_kladr_street_new_table_value = '')");

        $logger = new SqlLogger();
        $vids_s = array();
        foreach ($ao_vids as $ao){
            $vids_s[] = $ao['vid'];
        }
        $logger->loggingActions('Archive streets ('.count($vids_s).'): '.implode(',',$vids_s));

        $ao_vids = $this->executeQueryWithLog($this->getMysqlEntityManager(),
            "select s.vid from content_type_tp tp join node n on n.vid = tp.vid join content_type_kladr_naspunkt s on s.nid = tp.field_tp_kladr_naspunkt_nid where s.field_kladr_naspunkt_archaic_value = 1");

        $logger = new SqlLogger();
        $vids_c = array();
        foreach ($ao_vids as $ao){
            $vids_c[] = $ao['vid'];
        }
        $logger->loggingActions('Archive cities ('.count($vids_c).'): '.implode(',',$vids_c));
/*
        $ao_vids = $this->executeQueryWithLog($this->getMysqlEntityManager(),
            "select s.vid from content_type_tp tp join node n on n.vid = tp.vid join content_type_kladr_raion s on s.nid = tp.field_tp_kladr_raion_nid where s.field_kladr_raion_archaic_value = 1 and (field_kladr_raion_new_table_value is null or field_kladr_raion_new_table_value = '')");

        $logger = new SqlLogger();
        $vids_a = array();
        foreach ($ao_vids as $ao){
            $vids_a[] = $ao['vid'];
        }
        $logger->loggingActions('Archive areas ('.count($vids_a).'): '.implode(',',$vids_a));
*/
        if (count($vids_s)>0)
            $this->executeQueryWithLog($this->getMysqlEntityManager(), "UPDATE content_type_kladr_street SET field_kladr_street_archaic_value = null WHERE vid in (".implode(',',$vids_s).")");
        if (count($vids_c)>0)
            $this->executeQueryWithLog($this->getMysqlEntityManager(), "UPDATE content_type_kladr_naspunkt SET field_kladr_naspunkt_archaic_value = null WHERE vid in (".implode(',',$vids_c).")");
    //    $this->executeQueryWithLog($this->getMysqlEntityManager(), "UPDATE content_type_kladr_raion SET field_kladr_raion_archaic_value = null WHERE vid in (".implode(',',$vids_a).")");
    }
}
