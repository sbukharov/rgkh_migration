<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Migration;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\ConsoleBannerProviderInterface;
use Zend\ModuleManager\Feature\ConsoleUsageProviderInterface;
use Zend\Console\Adapter\AdapterInterface as Console;
use DoctrineORMModule\Service\EntityManagerFactory;
use DoctrineORMModule\Service\DBALConnectionFactory;
use DoctrineORMModule\Service\ConfigurationFactory;

class Module implements ConsoleBannerProviderInterface, ConsoleUsageProviderInterface
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConsoleBanner(Console $console)
    {
        return "Application \"Reforma GKH: Migration DB\". Copyright (c) 2013 F-technology Ltd. (www.f-technology.ru)";
    }

    public function getConsoleUsage(Console $console){
        return array();
    }

    public function getServiceConfig()
    {
        return array(
            'aliases' => array(
                'mysqlDoctrineEm' => 'doctrine.entitymanager.orm_default',
                'pgsqlDoctrineEm' => 'doctrine.entitymanager.orm_pgsql',
                'mysqlDoctrineEmSecond' => 'doctrine.entitymanager.orm_mysql_second'
            ),
            'factories' => array(
                'doctrine.entitymanager.orm_pgsql'        => new EntityManagerFactory('orm_pgsql'),
                'doctrine.connection.orm_pgsql'           => new DBALConnectionFactory('orm_pgsql'),
                'doctrine.configuration.orm_pgsql'        => new ConfigurationFactory('orm_pgsql'),

        //      'doctrine.configuration.orm_mysql_second'        => new ConfigurationFactory('orm_mysql_second'),
                'doctrine.connection.orm_mysql_second'           => new DBALConnectionFactory('orm_mysql_second'),
                'doctrine.entitymanager.orm_mysql_second'        => new EntityManagerFactory('orm_mysql_second'),


                'importFiasModel' => function ($serviceManager) {
                        return new Model\ImportFiasModel(
                            $serviceManager->get('mysqlDoctrineEm'),
                            $serviceManager->get('pgsqlDoctrineEm')
                        );
                    },
                'usersModel' => function ($serviceManager) {
                        return new Model\UsersModel(
                            $serviceManager->get('mysqlDoctrineEm'),
                            $serviceManager->get('pgsqlDoctrineEm')
                        );
                    },
                'geotagsModel' => function ($serviceManager) {
                        return new Model\GeotagsModel(
                            $serviceManager->get('mysqlDoctrineEm'),
                            $serviceManager->get('pgsqlDoctrineEm')
                        );
                    },
                'managementOrgsModel' => function ($serviceManager) {
                        return new Model\ManagementOrgsModel(
                            $serviceManager->get('mysqlDoctrineEm'),
                            $serviceManager->get('pgsqlDoctrineEm')
                        );
                    },
                'mkdModel' => function ($serviceManager) {
                        return new Model\MkdModel(
                            $serviceManager->get('mysqlDoctrineEm'),
                            $serviceManager->get('pgsqlDoctrineEm'),
                            $serviceManager->get('mysqlDoctrineEmSecond')
                        );
                    },
                'ratingUOModel' => function ($serviceManager) {
                        return new Model\RatingUOModel(
                            $serviceManager->get('mysqlDoctrineEm'),
                            $serviceManager->get('pgsqlDoctrineEm')
                        );
                    },
                'filesModel' => function ($serviceManager) {
                        return new Model\FilesModel(
                            $serviceManager->get('mysqlDoctrineEm'),
                            $serviceManager->get('pgsqlDoctrineEm')
                        );
                    },
            )
        );
    }
}
